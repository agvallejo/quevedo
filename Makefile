include mk/init.mk

$(eval $(call include,test/module.mk))
$(eval $(call include,libc/module.mk))
$(eval $(call include,initrd/module.mk))
$(eval $(call include,kernel/module.mk))

.DEFAULT_GOAL:=qemu
.PHONY: test
test: $(TESTS)
	@$(^:= &&) echo "---Tests completed---"

.PHONY: qemu-multiboot
qemu-multiboot: $(ISO<kernel-multiboot>)
	# q35 is required for PCIe support (MCFG not present otherwise)
	# Should work with -cpu IvyBridge, but TCG complains
	#
	# Open alongside "gdb quevedo.elf" and type "target remote localhost:1234"
	$(VM<amd64>) -enable-kvm -cpu host -machine q35 -cdrom $< -serial stdio -s

.PHONY: vbox
vbox: $(IMG<efi_loader>)
	rm -f $(<:.img=.vdi)
	VBoxManage convertfromraw $< $(<:.img=.vdi) --format vdi
	VBoxManage storageattach tmp --storagectl SATA \
	                             --port 0 \
	                             --medium $(<:.img=.vdi)
	VBoxManage startvm tmp

.PHONY: qemu
NCORES?=`nproc`
qemu: $(IMG<efi_loader>) $(wildcard $(OVMF<efi_loader>)*.fd)
	qemu-system-x86_64 -cpu host,migratable=no -s -enable-kvm \
	-smp $(NCORES) \
	-drive if=pflash,format=raw,unit=0,file=$(OVMF<efi_loader>)OVMF_CODE.fd,readonly=on \
	-drive if=pflash,format=raw,unit=1,file=$(OVMF<efi_loader>)OVMF_VARS.fd \
	-net none \
	-drive file=$(IMG<efi_loader>),if=ide

.PHONY: bochs
bochs: $(ISO<kernel-multiboot>) $(SYM<kernel-multiboot>)
	bochs -q

$(ISO<kernel-multiboot>): $(BIN<kernel-multiboot>) $(BIN<init>) $(CFG<kernel-multiboot>)
	mkdir -p $(SYSROOT)boot/grub
	cp $(BIN<kernel-multiboot>) $(SYSROOT)boot/
	cp $(CFG<kernel-multiboot>) $(SYSROOT)boot/grub/
	mkdir -p $(SYSROOT)bin
	cp $(BIN<init>)   $(SYSROOT)bin/
	grub-mkrescue -o $@.tmp $(SYSROOT)
	mv $@.tmp $@


install: $(BIN<efi_loader>) $(BIN<kernel-efi>) $(BIN<initrd>)
	cp $^ /efi/EFI/quevedo

.PHONY: efi
efi: $(IMG<efi_loader>)

.PHONY: iso
iso: $(ISO<kernel>)

.PHONY: docs
docs:
	mkdir -p $(BUILDDIR)docs/
	pdflatex -output-directory $(BUILDDIR)docs/ docs/quevedo.tex
	pdflatex -output-directory $(BUILDDIR)docs/ docs/quevedo.tex
	evince $(BUILDDIR)docs/quevedo.pdf

.PHONY: clean
clean:
	rm -rf $(BUILDDIR) $(SYSROOT)
