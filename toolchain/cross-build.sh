#!/bin/sh -exu

GCC_VERSION=7.3.0
BINUTILS_VERSION=2.30
AUTOCONF_VERSION=2.64
AUTOMAKE_VERSION=1.11.6

# Requirements
REPO_DIR=$REPO_DIR
TMP_DIR=$TMP_DIR
PREFIX=$PREFIX
TARGET=$TARGET
SYSROOT=$SYSROOT

GCC_DIR="$REPO_DIR/gcc"
BINUTILS_DIR="$REPO_DIR/binutils-gdb"
PATCHES_DIR="$PWD/patches"
QUEVEDO_DIR="$PWD/.."

GCC_URL=https://github.com/gcc-mirror/gcc.git #Mirror
BINUTILS_URL=https://github.com/bminor/binutils-gdb #Mirror
AUTOMAKE_URL="http://ftp.gnu.org/gnu/automake/automake-$AUTOMAKE_VERSION.tar.gz"
AUTOCONF_URL="http://ftp.gnu.org/gnu/autoconf/autoconf-$AUTOCONF_VERSION.tar.gz"

get_automake(){
    cd "$TMP_DIR"
    wget -qO- "$AUTOMAKE_URL" | tar xz
    DIR="$TMP_DIR/automake-$AUTOMAKE_VERSION/build"
    mkdir -p "$DIR"
    cd "$DIR"
    ../configure --prefix="$PREFIX"
    make && make install
    cd "$PREFIX/bin"
    patch -p0 -s < "$PATCHES_DIR/automake-$AUTOMAKE_VERSION.patch"
}

get_autoconf(){
    cd "$TMP_DIR"
    wget -qO- "$AUTOCONF_URL" | tar xz
    DIR="$TMP_DIR/autoconf-$AUTOCONF_VERSION/build"
    mkdir -p "$DIR"
    cd "$DIR"
    ../configure --prefix="$PREFIX"
    make && make install
}

get_gcc(){
    if [ ! -d "$GCC_DIR" ]
    then
        cd "$REPO_DIR"
        git clone "$GCC_URL"
        cd "$GCC_DIR"
    else
        cd "$GCC_DIR"
        git fetch
        git reset --hard
        git clean -df
    fi
    git checkout tags/gcc-`echo $GCC_VERSION | sed 's/\./_/g'`-release
    cd "$GCC_DIR"
    git apply "$PATCHES_DIR/gcc-$GCC_VERSION.patch"
    ./contrib/download_prerequisites
    cd "$GCC_DIR"/libstdc++-v3
    autoconf
    rm -rf "$GCC_DIR"/build/*
    mkdir -p "$GCC_DIR"/build
    cd "$GCC_DIR"/build
    make -C $QUEVEDO_DIR sysheaders
    ../configure --prefix="$PREFIX" --target="$TARGET" --with-sysroot="$SYSROOT"\
                 --disable-nls --enable-languages=c,c++ >configure.gcc
    make -s all-gcc all-target-libgcc>make.gcc
    make -s install-gcc install-target-libgcc
}

get_binutils(){
    if [ ! -d "$BINUTILS_DIR" ]
    then
        cd "$REPO_DIR"
        git clone "$BINUTILS_URL"
        cd "$BINUTILS_DIR"
    else
        cd "$BINUTILS_DIR"
        git fetch
        git reset --hard
        git clean -df
    fi
    git checkout tags/binutils-`echo $BINUTILS_VERSION | sed 's/\./_/g'`
    cd "$BINUTILS_DIR"
    git apply "$PATCHES_DIR/binutils-$BINUTILS_VERSION.patch"
    cd "$BINUTILS_DIR"/ld
    automake
    rm -rf "$BINUTILS_DIR"/build/*
    mkdir -p "$BINUTILS_DIR"/build
    cd "$BINUTILS_DIR"/build
    ../configure --prefix="$PREFIX" --target="$TARGET" --with-sysroot="$SYSROOT"\
                 --disable-nls --disable-gdb --disable-libdecnumber\
                 --disable-readline --disable-sim > configure.binutils
    make -s> make.binutils && make -s install
}

mkdir -p "$TMP_DIR"
mkdir -p "$REPO_DIR"
mkdir -p "$PREFIX"
make -C../ sysheaders

echo "$PATCHES_DIR/automake-$AUTOMAKE_VERSION.patch"
if [ ! -f "$PREFIX/bin/automake" ]
then
    get_automake
fi

if [ ! -f "$PREFIX/bin/autoconf" ]
then
    get_autoconf
fi

export PATH="$PREFIX/bin:$PATH"
get_binutils
get_gcc
