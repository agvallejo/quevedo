define push =
    $(eval _SP:=S$(_SP))
    $(eval _STACK_$(_SP):=$($1))
endef
define pop =
    $(eval $1:=$(_STACK_$(_SP)))
    $(eval _SP:=$(_SP:%S=%))
endef
define include =
    $(eval $(call push,DIR))
    $(eval DIR:=$(dir $1))
    $(eval include $1)
    $(eval $(call pop,DIR))
endef
define include_mkfiles =
    $(eval $(call push,MKFILE))
    $(eval $(foreach MKFILE, $(wildcard $(DIR)$1/*.mk), $(call include,$(MKFILE))))
    $(eval $(call pop,MKFILE))
endef
define envcpy =
    $(eval CC<$1>:=$(CC<$2>))
    $(eval CFLAGS<$1>=$(CFLAGS<$2>))
    $(eval CPPFLAGS<$1>=$(CPPFLAGS<$2>))
    $(eval LDFLAGS<$1>=$(LDFLAGS<$2>))
    $(eval LDLIBS<$1>=$(LDLIBS<$2>))
endef
define getobjs =
    $(eval OBJS<$1>:=$(SRCS:%.c=$(dir $1)%.c.o) $(ASMS:%.asm=$(dir $1)%.asm.o))
    $(eval SRCS:=)
    $(eval ASMS:=)
endef

define spawn_c_rule =
$(dir $1)%.c.o: %.c
	mkdir -p $$(@D)
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $$< -o $$@
endef
define spawn_asm_rule =
$(dir $1)%.asm.o: %.asm
	mkdir -p $$(@D)
	$(AS) $$< $(ASFLAGS) -o $$@
endef
define spawn_link_rule =
$1: $(OBJS<$1>) $(LDSCRIPT<$1>)
	mkdir -p $$(@D)
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJS<$1>) -o $$@ $(LDLIBS)
-include $(OBJS<$1>:.o=.d)
endef
define spawn_lib_rule =
$1: $(OBJS<$1>)
	mkdir -p $$(@D)
	$(AR) rcs $$@ $$^
-include $(OBJS<$1>:.o=.d)
endef
