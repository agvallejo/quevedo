DIR:=mk/
# Useful macros to be used with $(call ...)
#
# All inclludes should go through the 'include' macro, but we don't have it yet.
# This should be the only exception of an include that uses an absolute path
include mk/macros.mk

$(eval $(call include,$(DIR)config.mk))

# Load the environments used for compilation.
# Careful with the dependencies...
$(eval $(call include,$(DIR)env.test.mk))
$(eval $(call include,$(DIR)env.test32.mk))

# Verbose, but effective. These are the basic includes
INCLUDE<sys>:=\
q_libc/_va_list.h \
q_libc/init.h \
q_libc/intptr_t.h \
q_libc/null.h \
q_libc/size_t.h \
\
sys/types.h \
\
errno.h \
stdarg.h \
stddef.h \
stdlib.h \
string.h \
time.h \
iso646.h \
stdalign.h \
stdbool.h \
stdio.h \
stdnoreturn.h \

INCLUDE<sys>:=$(INCLUDE<sys>:%=$(SYSROOT<include>)%)

# Init the tests to run
TESTS:=
