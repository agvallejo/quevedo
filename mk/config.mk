SYSROOT:=sysroot/
SYSROOT<include>:=$(SYSROOT)usr/include/
SYSROOT<lib>:=$(SYSROOT)usr/lib/

BUILDDIR:=build/
TARGETDIR:=$(BUILDDIR)target/
ISODIR:=$(TARGETDIR)iso/
TESTSDIR:=$(BUILDDIR)tests/

TESTDIR:=test/

WARNINGS:=-std=c11 -pedantic \
          -Wall -Wextra -Werror \
          -Wstrict-prototypes -Wmissing-prototypes \
          -Wundef
CFLAGS<default>:=$(WARNINGS) -g -MD -MP -O3

CC<amd64>:=x86_64-quevedo-gcc --sysroot=$(SYSROOT)
AS<amd64>:=nasm
ASFLAGS<amd64>:=-felf64 -Werror
ASFLAGS<efi_loader>:=-fwin64 -Werror
VM<amd64>:=qemu-system-x86_64
