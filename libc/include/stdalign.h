#ifndef _QUEVEDO_STDALIGN_H
#define _QUEVEDO_STDALIGN_H

#define alignas _Alignas
#define alignof _Alignof

#define __alignas_is_defined 1
#define __alignof_is_defined 1

#endif // _QUEVEDO_STDALIGN_H
