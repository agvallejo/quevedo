#ifndef _QUEVEDO_Q_LIBC_INIT_H
#define _QUEVEDO_Q_LIBC_INIT_H

void _quevedo_putchar_init(void (*fun)(int));

#endif // _QUEVEDO_Q_LIBC_INIT_H
