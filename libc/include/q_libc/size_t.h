#ifndef _QUEVEDO_SIZE_T
#define _QUEVEDO_SIZE_T

/*
 * There is no particular reason for making this unsigned long instead
 * of just unsigned except for the fact that it's generally expected
 * to be so. Both Clang's and GCC's built-ins have that size and being
 * out-of-sync is a PITA because we wouldn't be able to use them when
 * the target architecture provides good HW aids. Oh, well...
 *
 * UPDATE: After more experience under my belt I guess I was naïve when
 * I thought I understood the subtleties here. There is a very good reason
 * why you can't define it to whatever you want. sizeof is built into
 * the compiler, so your libc should agree with whatever the compiler
 * desires for it to be portable. On Windows amd64 this is actually
 * unsigned long long. Problematic because unsigned long is only
 * 32 bits. I was just lucky before. This should actually be a built-in
 * type.
 */
typedef __SIZE_TYPE__ size_t;

#endif
