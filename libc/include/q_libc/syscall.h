#ifndef _QUEVEDO_Q_LIBC_SYSCALL_H
#define _QUEVEDO_Q_LIBC_SYSCALL_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <sys/types.h>

/**
 * Function codes for the syscall code
 */
enum isr_syscall_code {
	ISR_SYSCALL_PUTCHAR = 0,
	ISR_SYSCALL_SLEEP   = 1,
	ISR_SYSCALL_GETPID  = 2,
	ISR_SYSCALL_EXEC    = 3,
	ISR_SYSCALL_NUM_TOTAL
};

static inline uintptr_t syscall_0(enum isr_syscall_code code)
{
	uintptr_t ret;
	__asm__ volatile("syscall" : "=a"(ret) : "D"(code):);
	return ret;
}

static inline uintptr_t syscall_1(enum isr_syscall_code code, uintptr_t arg1)
{
	uintptr_t ret;
	__asm__ volatile("syscall" : "=a"(ret) : "D"(code), "S"(arg1) :);
	return ret;
}

static inline uintptr_t syscall_2(enum isr_syscall_code code, uintptr_t arg1, uintptr_t arg2)
{
	uintptr_t ret;
	__asm__ volatile("syscall" : "=a"(ret) : "D"(code), "S"(arg1), "d"(arg2):);
	return ret;
}

static inline void sleep(size_t ns) {syscall_1(ISR_SYSCALL_SLEEP, ns);}
static inline pid_t getpid(void) {return syscall_0(ISR_SYSCALL_GETPID);}
static inline bool exec(void) {return syscall_0(ISR_SYSCALL_EXEC);}

#endif // _QUEVEDO_Q_LIBC_SYSCALL_H
