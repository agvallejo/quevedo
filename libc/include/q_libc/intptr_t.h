#ifndef _QUEVEDO_INTPTR_T
#define _QUEVEDO_INTPTR_T

/*
There is no particular reason for making this unsigned long instead
of just unsigned except for the fact that it's generally expected
to be so. Both Clang's and GCC's built-ins have that size and being
out-of-sync is a PITA because we wouldn't be able to use them when
the target architecture provides good HW aids. Oh, well...
*/
typedef long intptr_t;

#endif
