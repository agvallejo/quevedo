#ifndef _QUEVEDO_STDDEF_H
#define _QUEVEDO_STDDEF_H

#include <q_libc/size_t.h>
#include <q_libc/null.h>

/*
TODO:
Not a fan of this definition, I should probably come back to better define it
as intptr_t once stdint.h is part of this libc and not borrowed from GCC's
sources.
*/
typedef long ptrdiff_t;

typedef __WCHAR_TYPE__ wchar_t;

#define offsetof(type, x) ((size_t)((char*)&((type *)(0))->x - (char*)0))

#endif // _QUEVEDO_STDDEF_H
