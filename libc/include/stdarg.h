#ifndef _QUEVEDO_STDARG_H
#define _QUEVEDO_STDARG_H

#include <q_libc/_va_list.h>

typedef _va_list va_list;
#define va_start(a,p)		__builtin_va_start(a, p)
#define va_end(a)		__builtin_va_end(a)
#define va_arg(a,t)		__builtin_va_arg(a,t)
#define va_copy(dest, src)	__builtin_va_copy(dest,src)

#endif // _QUEVEDO_STDARG_H
