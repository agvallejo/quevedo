#ifndef _QUEVEDO_STDBOOL_H
#define _QUEVEDO_STDBOOL_H

typedef _Bool bool;

#undef  false
#define false (0)

#undef  true
#define true (1)

#endif // _QUEVEDO_STDBOOL_H
