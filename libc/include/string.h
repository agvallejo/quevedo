#ifndef _QUEVEDO_STRING_H
#define _QUEVEDO_STRING_H

#include <q_libc/size_t.h>

int memcmp(const void *s1, const void *s2, size_t n);
void *memcpy(void* restrict, const void* restrict, size_t);
void *memmove(void*, const void*, size_t);
void *memset(void*, int, size_t);
char *strcpy(char * restrict s1, const char * restrict s2);
int strcmp(const char *s1, const char *s2);
size_t strlen(const char *s);
int strncmp(const char *s1, const char *s2, size_t n);

#endif // _QUEVEDO_STRING_H
