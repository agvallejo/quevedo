#ifndef _QUEVEDO_UNISTD_H
#define _QUEVEDO_UNISTD_H

#include <sys/types.h> //include pid_t
#include <stdbool.h>

pid_t fork(void);
int execv(const char *path, char *const argv[]);
int execve(const char *path, char *const argv[], char *const envp[]);
int execvp(const char *file, char *const argv[]);

#endif // _QUEVEDO_UNISTD_H
