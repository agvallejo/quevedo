#include <string.h>
#include <stddef.h>

int strncmp(const char *s1, const char *s2, size_t n)
{
	const unsigned char *us1 = (const unsigned char *)s1;
	const unsigned char *us2 = (const unsigned char *)s2;

	for (size_t i=0; i<n; i++, us1++, us2++)
		if (!*us1 || (*us1 != *us2))
			break;

	return *us1-*us2;
}
