#-------------------------------------------------------------------------------
# Artifacts
#-------------------------------------------------------------------------------
INCLUDE<libc>:=$(DIR)include/
LIB<libk>:=$(TARGETDIR)libc/kernelmode/libk.a
LIB<libc>:=$(TARGETDIR)libc/usermode/libc.a

#-------------------------------------------------------------------------------
# Source discovery
#-------------------------------------------------------------------------------
SRCS<libc>:=$(wildcard $(DIR)*.c)
ASMS<libc>:=$(wildcard $(DIR)*.asm)
$(eval $(call include,$(DIR)string/module.mk))
$(eval $(call include,$(DIR)stdio/module.mk))
$(eval $(call include,$(DIR)crt/module.mk))

#-------------------------------------------------------------------------------
# Object file generation
#-------------------------------------------------------------------------------
SRCS:=$(SRCS<libc>)
ASMS:=$(ASMS<libc>)
$(eval $(call getobjs,$(LIB<libk>)))

SRCS:=$(SRCS<libc>)
ASMS:=$(ASMS<libc>)
$(eval $(call getobjs,$(LIB<libc>)))

#-------------------------------------------------------------------------------
# Rules
#-------------------------------------------------------------------------------
CC:=$(CC<amd64>)
AS:=$(AS<amd64>)
ASFLAGS:=$(ASFLAGS<amd64>)
CFLAGS:=$(CFLAGS<default>) \
    -ffreestanding \
    -mno-red-zone \
    -mno-sse \
    -mcmodel=kernel \
    -zmax-page-size=0x1000
CPPFLAGS:=\
    -I$(INCLUDE<libc>) \
    -DIS_KERNEL=1 \
    -DNORETURN=_Noreturn \
    -DSTATIC=static
$(eval $(call spawn_c_rule,$(LIB<libk>)))
$(eval $(call spawn_asm_rule,$(LIB<libk>)))
$(eval $(call spawn_lib_rule,$(LIB<libk>)))
$(LIB<libk>): $(INCLUDE<sys>)

CC:=$(CC<amd64>)
AS:=$(AS<amd64>)
ASFLAGS:=$(ASFLAGS<amd64>)
CFLAGS:=$(CFLAGS<default>)
CPPFLAGS:=\
    -I$(INCLUDE<libc>) \
    -DIS_KERNEL=0 \
    -DNORETURN=_Noreturn \
    -DSTATIC=static
$(eval $(call spawn_c_rule,$(LIB<libc>)))
$(eval $(call spawn_asm_rule,$(LIB<libc>)))
$(eval $(call spawn_lib_rule,$(LIB<libc>)))
$(LIB<libc>): $(INCLUDE<sys>)

$(SYSROOT<lib>)libk.a: $(LIB<libk>)
	mkdir -p $(@D)
	cp $< $@
$(SYSROOT<lib>)libc.a: $(LIB<libc>)
	mkdir -p $(@D)
	cp $< $@
$(SYSROOT<include>)%.h: $(INCLUDE<libc>)%.h
	mkdir -p $(@D)
	cp $< $@

.PHONY: sysheaders
sysheaders:
	mkdir -p $(SYSROOT<include>)
	cp -ru $(INCLUDE<libc>)* $(SYSROOT<include>)
