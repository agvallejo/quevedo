BIN<initrd>:=$(TARGETDIR)initrd/initrd.bin

SRCS:=$(DIR)init.c
ASMS:=
$(eval $(call getobjs,$(BIN<initrd>)))

CC:=$(CC<amd64>)
AS:=$(AS<amd64>)
ASFLAGS:=$(ASFLAGS<amd64>)
CFLAGS:=$(CFLAGS<default>)
CPPFLAGS:=\
    -I$(INCLUDE<kernel>) \
    -I$(INCLUDE<libc>) \
    -DSTATIC=static \
    -DNORETURN=noreturn
LDFLAGS:=
LDLIBS:=
$(eval $(call spawn_c_rule,$(BIN<initrd>)))
$(eval $(call spawn_asm_rule,$(BIN<initrd>)))
$(eval $(call spawn_link_rule,$(BIN<initrd>)))

# In time initrd.bin will hold a full filesystem,
# but for the time being it's just a single ELF

$(BIN<initrd>): $(SYSROOT<lib>)libc.a $(OBJS<crt>)
