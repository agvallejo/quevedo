################################################################################
# Setup
################################################################################
# Global defs
INCLUDE<kernel>:=$(DIR)include/
DIR<kernel>:=$(DIR)
# Multiboot defs
LDSCRIPT<kernel-multiboot>:=$(DIR)linker-multiboot.ld
BIN<kernel-multiboot>:=$(TARGETDIR)kernel-multiboot/quevedo.elf
ISO<kernel-multiboot>:=$(TARGETDIR)kernel-multiboot/quevedo.iso
SYM<kernel-multiboot>:=$(TARGETDIR)kernel-multiboot/quevedo.sym
# EFI defs
LDSCRIPT<kernel-efi>:=$(DIR)linker-efi.ld
BIN<kernel-efi>:=$(TARGETDIR)kernel-efi/quevedo.elf
ISO<kernel-efi>:=$(TARGETDIR)kernel-efi/quevedo.iso
SYM<kernel-efi>:=$(TARGETDIR)kernel-efi/quevedo.sym

# Initialise the sources
SRCS<kernel>:=$(wildcard $(DIR)*.c)
ASMS<kernel>:=$(wildcard $(DIR)*.asm)
SRCS<kernel-efi>:=
ASMS<kernel-efi>:=
SRCS<kernel-multiboot>:=
ASMS<kernel-multiboot>:=

################################################################################
# Path traversal
################################################################################
# Load the bootloaders. Bootloaders are cool
$(eval $(call include,$(DIR)boot/module.mk))

$(eval $(call include,$(DIR)elf/module.mk))
$(eval $(call include,$(DIR)fb/module.mk))
$(eval $(call include,$(DIR)fs/module.mk))
$(eval $(call include,$(DIR)hal/module.mk))
$(eval $(call include,$(DIR)mm/module.mk))
$(eval $(call include,$(DIR)mp/module.mk))
#$(eval $(call include,$(DIR)serial/module.mk))
$(eval $(call include,$(DIR)time/module.mk))
$(eval $(call include,$(DIR)tsch/module.mk))

################################################################################
# Multiboot+EFI rule setup
################################################################################
SRCS:=$(SRCS<kernel>) $(SRCS<kernel-multiboot>)
ASMS:=$(ASMS<kernel>) $(ASMS<kernel-multiboot>)
$(eval $(call getobjs,$(BIN<kernel-multiboot>)))
SRCS:=$(SRCS<kernel>) $(SRCS<kernel-efi>)
ASMS:=$(ASMS<kernel>) $(ASMS<kernel-efi>)
$(eval $(call getobjs,$(BIN<kernel-efi>)))

# Rules for compiling object files and linking binaries
# on amd64. So far, this is simple enough. Later on, when
# I decide to include ARM support I'll see how to deal with
# this mess
CC:=$(CC<amd64>)
AS:=$(AS<amd64>)
ASFLAGS:=$(ASFLAGS<amd64>)
CFLAGS:=$(CFLAGS<default>) \
    -ffreestanding \
    -mno-red-zone \
    -mno-sse \
    -mcmodel=kernel \
    -zmax-page-size=0x1000
CPPFLAGS:=\
    -I$(DIR<kernel>) \
    -I$(INCLUDE<kernel>) \
    -I$(INCLUDE<libc>) \
    -DNORETURN=_Noreturn \
    -DSTATIC=static
LDSCRIPT:=$(LDSCRIPT<kernel-multiboot>)
LDFLAGS:=-Wl,-T$(LDSCRIPT)
LDLIBS:=-nostdlib -lk -lgcc
$(eval $(call spawn_c_rule,$(BIN<kernel-multiboot>)))
$(eval $(call spawn_asm_rule,$(BIN<kernel-multiboot>)))
$(eval $(call spawn_link_rule,$(BIN<kernel-multiboot>)))
$(BIN<kernel-multiboot>): $(INCLUDE<sys>) $(SYSROOT<lib>)libk.a
$(SYM<kernel-multiboot>): $(BIN<kernel-multiboot>)
	nm $< | awk '{print $$1" "$$3}' > $@.tmp
	mv $@.tmp $@

# Inherit everything from the multiboot build
LDSCRIPT:=$(LDSCRIPT<kernel-efi>)
LDFLAGS:=-Wl,-T$(LDSCRIPT)
$(eval $(call spawn_c_rule,$(BIN<kernel-efi>)))
$(eval $(call spawn_asm_rule,$(BIN<kernel-efi>)))
$(eval $(call spawn_link_rule,$(BIN<kernel-efi>)))
$(BIN<kernel-efi>): $(LDSCRIPT<kernel-efi>) $(INCLUDE<sys>) $(SYSROOT<lib>)libk.a
$(SYM<kernel-efi>): $(BIN<kernel-efi>)
	nm $< | awk '{print $$1" "$$3}' > $@.tmp
	mv $@.tmp $@

$(SYSROOT<include>)%.h: $(INCLUDE<kernel>)%.h
	mkdir -p $(@D)
	cp $< $@

################################################################################
# ISO/IMG setup
################################################################################
$(IMG<efi_loader>): $(CREATE_IMG<efi_loader>) $(BIN<efi_loader>) $(BIN<kernel-efi>) $(BIN<initrd>)
	KERNEL_BIN=$(BIN<kernel-efi>) INITRD_BIN=$(BIN<initrd>) EFI_IMG=$(IMG<efi_loader>) EFI_APP=$(BIN<efi_loader>) $(CREATE_IMG<efi_loader>)
