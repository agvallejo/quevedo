#ifndef QUEVEDO_FB_H
#define QUEVEDO_FB_H

#include <stdint.h>
#include <stddef.h>

/**
 * Initialise the framebuffer with the bootstrap map already provided
 *
 * Can only be used while the HBT is available
 *
 * @param hbt Handover Boot Table
 */
struct hbt;
void fb_pre_init(struct hbt *hbt);

/**
 * Initialise the framebuffer properly mapping it into kernel space
 *
 * Needs to be done before starting the AP cores or getting rid of the HBT
 *
 * @param hbt Handover Boot Table
 */
struct hbt;
void fb_init(struct hbt *hbt);

/** Clear the framebuffer */
void fb_clear(void);

/**
 * Print a character to the output device
 *
 * @param c Character to print
 */
void fb_putchar(int c);

#endif /* QUEVEDO_IO_H */
