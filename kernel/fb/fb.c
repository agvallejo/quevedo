#include <quevedo/boot.h>
#include <quevedo/panic.h>

#include <mm/vmm.h>
#include "fb_private.h"

#include <q_libc/init.h>
#include <string.h>
#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

STATIC struct fb_data fb_private_data;

/* There's an array inside the header. Ugly as it is, this makes it static */
static unsigned
#include "font/font8x8_basic.h"

#define BACKGROUND  (0x000000u)
#define FOREGROUND  (0xFFFFFFu)
#define CHAR_WIDTH  (8u)
#define CHAR_HEIGHT (8u)

static void fb_putchar_at(unsigned char c, size_t posx, size_t posy)
{
	uint32_t *buffer = fb_getbuffer();
	size_t offset = posx + posy*fb_getsize_x();

	for (size_t y = 0; y<CHAR_HEIGHT; y++)
	{
		uint8_t line = font8x8_basic[c][y];
		for (size_t x = 0; x<CHAR_WIDTH; x++)
		{
			buffer[offset+x+y*fb_getsize_x()] = ((line>>x)&1)*FOREGROUND;
		}
	}
}

static void fb_shiftline(void)
{
	uint32_t *buffer = fb_getbuffer();
	memmove(buffer, buffer+CHAR_HEIGHT*fb_getsize_x(), ((fb_getsize_y()-CHAR_HEIGHT)*fb_getsize_x())*sizeof *buffer);
	memset(buffer + ((fb_getsize_y()-CHAR_HEIGHT)*fb_getsize_x()), BACKGROUND, CHAR_HEIGHT*fb_getsize_x()*sizeof *buffer);
}

void fb_pre_init(struct hbt *hbt)
{
	_quevedo_putchar_init(fb_putchar);
	fb_setbuffer((void*)hbt->gfx.virt_buf);
	fb_setsize_x(hbt->gfx.resx);
	fb_setsize_y(hbt->gfx.resy);
}

void fb_init(struct hbt *hbt)
{
	size_t buffer_size = hbt->gfx.resx*hbt->gfx.resy*4;
	uintptr_t virt_buf;
	printf("[fb] phy_buf=0x%llX\n", hbt->gfx.phy_buf);
	kassert_bool(vmm_map(VMM_REGION_PRIV_GLOBAL, hbt->gfx.phy_buf, buffer_size, &virt_buf), buffer_size);

	fb_setbuffer((void*)virt_buf);
	fb_setsize_x(hbt->gfx.resx);
	fb_setsize_y(hbt->gfx.resy);
}

void fb_clear(void)
{
	uint32_t *buffer = fb_getbuffer();

	size_t size_x = fb_getsize_x();
	size_t size_y = fb_getsize_y();
	for (size_t i=0; i<size_x*size_y; i++)
		*buffer = BACKGROUND;

	fb_setpos_x(0);
	fb_setpos_y(0);
}

void fb_putchar(int c)
{
	unsigned char uc = c;
	if (uc != '\n')
	{
		fb_putchar_at(c, fb_getpos_x(), fb_getpos_y());
		fb_setpos_x(fb_getpos_x()+CHAR_WIDTH);
		if (fb_getpos_x() != fb_getsize_x())
			return;
	}

	fb_setpos_x(0);
	if (fb_getpos_y() == (fb_getsize_y()-CHAR_HEIGHT))
		fb_shiftline();
	else
		fb_setpos_y(fb_getpos_y()+CHAR_HEIGHT);
}
