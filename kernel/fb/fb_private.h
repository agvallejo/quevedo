#ifndef FB_PRIVATE_H
#define FB_PRIVATE_H

#include "fb.h"

typedef struct {
	size_t x;
	size_t y;
} fbcoord_t;

struct fb_data{
	uint32_t  *buffer;
	fbcoord_t size;
	fbcoord_t pos;
};

#define fb_getbuffer() (fb_private_data.buffer)
#define fb_getpos_x()  (fb_private_data.pos.x)
#define fb_getpos_y()  (fb_private_data.pos.y)
#define fb_getsize_x() (fb_private_data.size.x)
#define fb_getsize_y() (fb_private_data.size.y)

#define fb_setbuffer(val)  do{fb_private_data.buffer = (val);}while(0)
#define fb_setpos_x(val)   do{fb_private_data.pos.x  = (val);}while(0)
#define fb_setpos_y(val)   do{fb_private_data.pos.y  = (val);}while(0)
#define fb_setsize_x(val)  do{fb_private_data.size.x = (val);}while(0)
#define fb_setsize_y(val)  do{fb_private_data.size.y = (val);}while(0)

#endif // FB_PRIVATE_H
