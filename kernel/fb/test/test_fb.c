#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include <munit.h>

#include <fb/fb.h>

#include "../fb_private.h"

extern struct fb_data fb_private_data;
#define TEST_WIDTH  80
#define TEST_HEIGHT 25

static void *setup(const MunitParameter params[], void* data)
{
	(void) params; (void) data;
	memset(&fb_private_data, 0, sizeof fb_private_data);
	void *buffer = calloc(1,TEST_WIDTH*TEST_HEIGHT*sizeof(fbchar_t));
	fb_init(buffer, TEST_WIDTH, TEST_HEIGHT);
	return buffer;
}

static void teardown(void* data)
{
	free(data);
}


//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------
static MunitResult test_fb_init(const MunitParameter params[], void* data)
{
	(void)params; (void)data;

	munit_assert_int(fb_getsize_x(), ==, TEST_WIDTH);
	munit_assert_int(fb_getsize_y(), ==, TEST_HEIGHT);
	munit_assert_ptr(fb_getbuffer(), ==, data);

	return MUNIT_OK;
}

static MunitResult test_fb_putchar_basic(const MunitParameter params[], void* data)
{
	(void)params; (void)data;

	fbchar_t *buffer = fb_getbuffer();
	char *str = "abcdfg";
	for (size_t i=0; i<sizeof str; i++) {
		fb_putchar(str[i]);
		munit_assert_int(buffer[i] & 0xFF, ==, str[i]);
	}

	return MUNIT_OK;
}

static MunitResult test_fb_putchar_linewrap(const MunitParameter params[], void* data)
{
	(void)params; (void)data;

	for (size_t i=0; i<TEST_WIDTH; i++)
		fb_putchar('a');
	munit_assert_int(fb_getpos_y(), ==, 1);
	munit_assert_int(fb_getpos_x(), ==, 0);
	return MUNIT_OK;
}

static MunitResult test_fb_putchar_wrap_on_lastline(const MunitParameter params[], void* data)
{
	(void)params; (void)data;

	for (size_t i=0; i<TEST_WIDTH*TEST_HEIGHT; i++)
		fb_putchar('a');
	munit_assert_int(fb_getpos_y(), ==, TEST_HEIGHT-1);
	munit_assert_int(fb_getpos_x(), ==, 0);
	return MUNIT_OK;
}

static MunitResult test_fb_putchar_lines_shift_on_wrap(const MunitParameter params[], void* data)
{
	(void)params; (void)data;

	fbchar_t *buffer = fb_getbuffer();

	for (size_t i=0; i<TEST_WIDTH; i++)
		fb_putchar('a');
	for (size_t i=0; i<TEST_WIDTH; i++)
		fb_putchar('b');
	for (size_t i=0; i<(TEST_HEIGHT-2)*TEST_WIDTH; i++)
		fb_putchar('a');

	// Only the second line has b's, so the first
	// line should have them after the line shift
	munit_assert_int(buffer[0] & 0xFF, ==, 'b');
	munit_assert_int(buffer[TEST_WIDTH-1] & 0xFF, ==, 'b');
	munit_assert_int(buffer[TEST_WIDTH] & 0xFF, ==, 'a');

	// The last line is spaces because of the shift
	munit_assert_int(buffer[(TEST_WIDTH-1)*TEST_HEIGHT] & 0xFF, ==, ' ');
	munit_assert_int(buffer[TEST_WIDTH*TEST_HEIGHT-1] & 0xFF, ==, ' ');
	return MUNIT_OK;
}

static MunitResult test_fb_putchar_newline(const MunitParameter params[], void* data)
{
	(void)params; (void)data;

	fb_putchar('a');
	fb_putchar('\n');

	munit_assert_int(fb_getpos_y(), ==, 1);
	munit_assert_int(fb_getpos_x(), ==, 0);
	return MUNIT_OK;
}

static MunitResult test_fb_clear(const MunitParameter params[], void* data)
{
	(void)params;
	
	while(!fb_getpos_y())
		fb_putchar('a');
	fb_clear();
	munit_assert_int(fb_getpos_y(), ==, 0);
	munit_assert_int(fb_getpos_x(), ==, 0);

	// Check that the first character is the space on the framebuffer
	munit_assert_uint((*(fbchar_t*)data), ==, fb_makechar(' '));
	return MUNIT_OK;
}

//------------------------------------------------------------------------------
// Test runner
//------------------------------------------------------------------------------
static MunitTest test_suite_tests[] = {
	// Name, function, setup, teardown, options, parameters
	{"/fb/init", test_fb_init, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/fb/putchar/basic", test_fb_putchar_basic, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/fb/putchar/linewrap/generic", test_fb_putchar_linewrap, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/fb/putchar/linewrap/last", test_fb_putchar_wrap_on_lastline, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/fb/putchar/linewrap/shift", test_fb_putchar_lines_shift_on_wrap, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/fb/putchar/newline", test_fb_putchar_newline, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/fb/clear", test_fb_clear, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	// NULL ENTRY @ END
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
	"", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
