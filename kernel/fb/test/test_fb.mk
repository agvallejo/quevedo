TESTNAME:=test_fb
ENV:=test
TEST:=$(TESTSDIR)$(ENV)/$(TESTNAME)
TESTS+=$(TEST)

ASMS:=
SRCS:=\
    $(DIR)../fb.c \
    $(DIR)test_fb.c \
    $(SRCS<test>)
$(eval $(call getobjs,$(TEST)))

CC:=$(CC<$(ENV)>)
CFLAGS:=$(CFLAGS<default>)
CPPFLAGS:=-I$(INCLUDE<test>) -I$(INCLUDE<kernel>) -DSTATIC= -DUNIT_TESTING
LDFLAGS:=
LDLIBS:=
$(eval $(call spawn_c_rule,$(TEST)))
$(eval $(call spawn_link_rule,$(TEST)))
