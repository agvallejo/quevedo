CFG<kernel-multiboot>:=$(DIR)grub.cfg

SRCS<kernel-multiboot>+=$(wildcard $(DIR)*.c)
ASMS<kernel-multiboot>+=$(wildcard $(DIR)*.asm)

$(eval $(call include_mkfiles,test))
