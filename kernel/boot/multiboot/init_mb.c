#include <quevedo/mm.h>
#include <quevedo/ld.h>
#include <quevedo/panic.h>

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "init_private.h"

#define NEXT_MMAP_ENTRY(x) do{ x = (void*)(sizeof((x)->size) + (x)->size + (uintptr_t)(x));}while(0)

STATIC void init_mb_mmap_print(struct init_mb_info *mb_info)
{
	printf("Found %u boot module%s\n", mb_info->mods_count, mb_info->mods_count==1?"":"s");
	struct init_mb_mmap *mmap = (void*)(uintptr_t)mb_info->mmap_addr;
	while ((mb_info->mmap_addr+mb_info->mmap_len) > (uintptr_t)mmap) {
		printf(" type=%u addr=0x%lX len=0x%lX\n", mmap->type, mmap->addr, mmap->len);
		NEXT_MMAP_ENTRY(mmap);
	}
}

STATIC bool init_mb_mmap_overlaps(struct init_mb_mmap *prev_mmap,
                           struct init_mb_mmap *mmap)
{
	if ((prev_mmap->addr <= mmap->addr) &&
	    ((prev_mmap->addr + prev_mmap->len)>mmap->addr))
		return true;
	if ((prev_mmap->addr > mmap->addr) &&
	    ((mmap->addr + mmap->len)>prev_mmap->addr))
		return true;
	return false;
}

STATIC void init_mb_mmap_merge(struct init_mb_mmap *prev_mmap,
                        struct init_mb_mmap *mmap)
{
	uint64_t prev_mmap_end = prev_mmap->addr + prev_mmap->len;
	uint64_t mmap_end = mmap->addr + mmap->len;
	if (prev_mmap->addr > mmap->addr)
		prev_mmap->addr = mmap->addr;
	if (prev_mmap_end >= mmap_end)
		prev_mmap->len = prev_mmap_end-prev_mmap->addr;
	else
		prev_mmap->len = mmap_end-prev_mmap->addr;

	mmap->type = INIT_MB_MMAP_ENTRY_RSVD;
}

STATIC void init_mb_mmap_sanitise(struct init_mb_info *mb_info)
{
	struct init_mb_mmap *first_mmap = (void*)(uintptr_t)mb_info->mmap_addr;

	uintptr_t buffer_end = mb_info->mmap_addr+mb_info->mmap_len;
	struct init_mb_mmap *mmap = first_mmap;
	while (buffer_end > (uintptr_t)mmap) {
		if (mmap->type != INIT_MB_MMAP_ENTRY_AVAIL){
			NEXT_MMAP_ENTRY(mmap);
			continue;
		}
		if ((mmap->len>=0x1000) && !mmap->addr){
			mmap->len -= 0x1000;
			mmap->addr = 0x1000;
		}
		if ((mmap->len>0x1000) && (mmap->addr & 0xFFF)) {
			mmap->len  -= mmap->addr & 0xFFF;
			mmap->addr  = (mmap->addr&~0xFFFLU) + 0x1000;
		}
		mmap->len  &= ~0xFFFLU;
		if (!mmap->len) {
			mmap->type = INIT_MB_MMAP_ENTRY_RSVD;
			NEXT_MMAP_ENTRY(mmap);
			continue;
		}
		struct init_mb_mmap *prev_mmap = first_mmap;
		while (prev_mmap < mmap) {
			if (prev_mmap->type != INIT_MB_MMAP_ENTRY_AVAIL) {
				NEXT_MMAP_ENTRY(prev_mmap);
				continue;
			}
			else if (init_mb_mmap_overlaps(prev_mmap, mmap)) {
				MB_WARNING("Merged mmap regions");
				init_mb_mmap_merge(prev_mmap, mmap);
				// Start over with 'mmap' invalidated. Deals
				// with the corner case of a chunk in a position
				// that makes two previous chunks mergeable.
				// This function is called ONCE and this
				// structure is small. I don't care about
				// efficiency or time complexity now.
				mmap = first_mmap;
				prev_mmap = first_mmap;
				break;
			}
			NEXT_MMAP_ENTRY(prev_mmap);
		}
		NEXT_MMAP_ENTRY(mmap);
	}
}

/**
 * Ensure that the physical address 'addr' is usable
 *
 * It could be checked in a single go from text_start to
 * bss_end, but this way the linker script can be reordered
 * or gaps created.
 *
 * @param addr Adress to check
 * @return True if 'addr' is not part of the kernel, else false
 */
static bool init_mb_phyaddr_is_usable(uintptr_t addr)
{
	if ((addr >= ld_get_lma(kernel_text_start)) &&
	    (addr <  ld_get_lma(kernel_text_end)))
		return false;
	if ((addr >= ld_get_lma(kernel_rodata_start)) &&
	    (addr <  ld_get_lma(kernel_rodata_end)))
		return false;
	if ((addr >= ld_get_lma(kernel_data_start)) &&
	    (addr <  ld_get_lma(kernel_data_end)))
		return false;
	if ((addr >= ld_get_lma(kernel_bss_start)) &&
	    (addr <  ld_get_lma(kernel_bss_end)))
		return false;
	if ((addr >= ld_get(bootstrap_tables_start)) &&
	    (addr <  ld_get(bootstrap_tables_end)))
		return false;
	return true;
}

STATIC void init_mb_mmap_to_mmphy(struct init_mb_info *mb_info)
{
	struct init_mb_mmap *mmap = (void*)(uintptr_t)mb_info->mmap_addr;
	while ((mb_info->mmap_addr+mb_info->mmap_len) > (uintptr_t)mmap) {
		if (mmap->type != INIT_MB_MMAP_ENTRY_AVAIL) {
			NEXT_MMAP_ENTRY(mmap);
			continue;
		}
		while(mmap->len) {
			if (init_mb_phyaddr_is_usable(mmap->addr))
				mm_phy_free(mmap->addr);
			mmap->len  -= 0x1000;
			mmap->addr += 0x1000;
		}

		NEXT_MMAP_ENTRY(mmap);
	}
}

void init_arg_parse(void *arg)
{
	struct init_mb_info *mb_info = arg;

	/* Memory map not present*/
	kassert(mb_info->flags, &, INIT_MB_MMAP_AVAIL);
	/* Corrupted mmap data */
	kassert(mb_info->mmap_addr+mb_info->mmap_len, >, mb_info->mmap_addr);

	init_mb_mmap_print(mb_info);
	init_mb_mmap_sanitise(mb_info);
	init_mb_mmap_to_mmphy(mb_info);
}

static struct init_mb_mod *init_mb_findmod(void *arg, char *modname)
{
	struct init_mb_info *mb_info = arg;
	struct init_mb_mod *mod = (void*)(uintptr_t)mb_info->mods_addr;
	size_t mods_count = mb_info->mods_count;
	while (mods_count--){
		if (!strcmp((char*)(uintptr_t)mod->cmdline,modname))
			return mod;
		mod++;
	}
	printf("Module '%s' not found\n", modname);
	panic("Module not present", mb_info->mods_count);
	return NULL;
}

uintptr_t init_mb_getmod_addr(void *arg, char *modname)
{
	struct init_mb_mod *mod = init_mb_findmod(arg, modname);
	return mod->mod_start;
}

size_t init_mb_getmod_size(void *arg, char *modname)
{
	struct init_mb_mod *mod = init_mb_findmod(arg, modname);
	return mod->mod_end-mod->mod_start;
}
