#include <cfg/cfg.h>
#include <quevedo/panic.h>
#include <quevedo/ld.h>

#include <mm/vmm.h>

#define LOG_MODULE init
#include <log/log.h>

#include <string.h>
#include <stdbool.h>

#include <isr/apic.h>

#include "init_acpi.h"
#include "init_acpi_private.h"

#define ACPI_TABLE(x) "[ACPI|"x"]"

STATIC bool acpi_checksum_isvalid(const void *start, size_t octets)
{
	const uint8_t *p = start;
	uint8_t checksum = 0;
	for (size_t i=0; i<octets; i++)
		checksum += *p++;

	// The checksum fields ensure the table itself sums zero
	return !checksum;
}

STATIC const struct acpi_rsdp *acpi_find_rsdp_in_range(uintptr_t start, uintptr_t end)
{
	// The RSDP is 16-byte aligned
	for (const struct acpi_rsdp *rsdp = (void*)start;
	     start < end; rsdp=(void*)(start+=16)) {
		// The signature is not null-terminated
		if (memcmp(rsdp->signature, "RSD PTR ", 8))
			continue;

		// '0' is ACPI 1.0 and '2' the rest. Other values are invalid
		size_t size;
		switch(rsdp->revision) {
			case 0: size=20; break;
			case 2: size=rsdp->length; break;
			default: continue;
		}

		if (!acpi_checksum_isvalid((void*)start, size))
			continue;

		// TODO: Check the checksums of XSDT or RSDT
		if (rsdp->revision == 2) {
			/* Ensure the XSDT is not over our identity mapped region */
			kassert(rsdp->xsdt_addr, <, (1ULL<<32));
		}

#ifndef UNIT_TESTING
		log_info("Found ACPI tables. RSDP->revision=%u (ACPI %s)", rsdp->revision, rsdp->revision?">=2.0":"1.0");
#endif /* UNIT_TESTING */
		return rsdp;
	}
	return NULL;
}

static const struct acpi_rsdp *acpi_find_rsdp(void)
{
	const struct acpi_rsdp *rsdp;

	// Try to find it on RO BIOS memory
	rsdp = acpi_find_rsdp_in_range(0xE0000,0x100000);
	if (rsdp) return rsdp;

	// As a last resort, look into EBDA. I wouldn't hold my breath on PC
	rsdp = acpi_find_rsdp_in_range(0x9FC00,0xA0000);

	kassert(rsdp, !=, NULL);

	return rsdp;
}

static bool acpi_sdt_matchessignature(char *signature, const struct acpi_sdt_hdr *sdt)
{
	/* Table must be within the identity map */
	kassert(sdt,<=, 1ULL<<32);
	kassert_true(acpi_checksum_isvalid(sdt, sdt->length), sdt);
	return !memcmp(sdt->signature, signature, 4);
}

static const struct acpi_sdt_hdr *acpi_find_table(
		const struct acpi_rsdp *rsdp,
		char *signature)
{
	const struct acpi_sdt_hdr *sdt;
	if (rsdp->revision == 0) {
		const struct acpi_rsdt *rsdt = (void*)(uintptr_t)rsdp->rsdt_addr;
		size_t max_idx = (rsdt->hdr.length-sizeof *rsdt)/sizeof(uint32_t);
		for (size_t i=0; i<max_idx; i++) {
			sdt = (void*)(uintptr_t)rsdt->entry[i];
			if (acpi_sdt_matchessignature(signature, sdt))
				return sdt;
		}
	}
	if (rsdp->revision == 2) {
		const struct acpi_xsdt *xsdt = (void*)rsdp->xsdt_addr;
		size_t max_idx = (xsdt->hdr.length-sizeof *xsdt)/sizeof(uint64_t);
		for (size_t i=0; i<max_idx; i++) {
			sdt = (void*)(uint64_t)xsdt->entry[i];
			if (acpi_sdt_matchessignature(signature, sdt))
				return sdt;
		}
	}
	return NULL;
}

static void acpi_madt_process_ioapic_redir(const struct acpi_madt *madt)
{
	const struct acpi_madt_entry *entry = (void*)((uintptr_t)madt->entry);
	size_t len = madt->hdr.length - sizeof *madt;
	while(len) {
		if (entry->type == ACPI_MADT_ENTRY_IOAPIC_REDIR) {
			log_info(ACPI_TABLE("MADT-IOAPICredir")" dst=%u, src=%u, flags=%u",
			       entry->int_override.glb_sys_int,
			       entry->int_override.irq_src,
			       entry->int_override.flags);
		}
		/* Sizes are expected to match */
		kassert(len, >=, entry->length);
		len -= entry->length;
		entry = (void*)(entry->length + (uintptr_t)entry);;
	}
}

	
static bool acpi_madt_process_ioapic_base(const struct acpi_madt *madt)
{
	const struct acpi_madt_entry *entry = (void*)((uintptr_t)madt->entry);
	size_t len = madt->hdr.length - sizeof *madt;
	bool found = false;
	while(len) {
		if (entry->type == ACPI_MADT_ENTRY_IOAPIC_BASE) {
			log_info("ioapic_addr=%X, offset=%u", entry->ioapic.ioapic_addr, entry->ioapic.glb_sys_int_base);
			apic_ioapic_add(entry->ioapic.ioapic_addr, entry->ioapic.glb_sys_int_base);
			kassert_true(!found, 0); /* Assert only 1 IOAPIC definition is found */
			found = true;
		}
		/* Sizes are expected to match */
		kassert(len, >=, entry->length);
		len -= entry->length;
		entry = (void*)(entry->length + (uintptr_t)entry);;
	}

	kassert_true(found, 0);
	return found;
}

static void acpi_process_madt(const struct acpi_rsdp *rsdp)
{
	const struct acpi_madt *madt = (void*)acpi_find_table(rsdp, "APIC");
	kassert_true(madt, 0);

	/* Map the APIC registers */
	log_info(ACPI_TABLE("MADT")"Local APIC Address=0x%X", madt->lapic_addr);
	vmm_map((uintptr_t)ld_get(lapic_start), madt->lapic_addr);
	vmm_cfg((uintptr_t)ld_get(lapic_start), 0x1000, VMM_CFG_RW_CD);

	if (acpi_madt_process_ioapic_base(madt)) {
		apic_init();
		acpi_madt_process_ioapic_redir(madt);
	}
}

static void acpi_process_hpet(const struct acpi_rsdp *rsdp)
{
	const struct acpi_hpet *hpet = (void*)acpi_find_table(rsdp, "HPET");
	if (!hpet) {
		return;
	}
	log_info(ACPI_TABLE("HPET")"Base address=0x%lX", hpet->base_addr);
	vmm_map((uintptr_t)ld_get(hpet_start), hpet->base_addr);
	vmm_cfg((uintptr_t)ld_get(hpet_start), 0x1000, VMM_CFG_RW_CD);
}

static void acpi_process_mcfg(const struct acpi_rsdp *rsdp)
{
	const struct acpi_mcfg *mcfg = (void*)acpi_find_table(rsdp, "MCFG");
	kassert_true(mcfg, 0);

	size_t len = mcfg->hdr.length - sizeof *mcfg;
	kassert(len, ==, 0x10); /* More than one segment */
	for (size_t i=0; i*sizeof *mcfg->entry < len; i++) {
		log_info(ACPI_TABLE("MCFG")"Base address=0x%lu", mcfg->entry[i].base);
		log_info(ACPI_TABLE("MCFG")"Segment group no.=0x%lu", mcfg->entry[i].group_no);
		log_info(ACPI_TABLE("MCFG")"Start PCI bus no.=0x%lu", mcfg->entry[i].startbus_no);
		log_info(ACPI_TABLE("MCFG")"End PCI bus no.=0x%lu", mcfg->entry[i].endbus_no);
		vmm_map((uintptr_t)ld_get(pci_start), mcfg->entry[i].base);
		vmm_cfg((uintptr_t)ld_get(pci_start), 0x1000, VMM_CFG_RW_CD);
	}
}

void acpi_init(void)
{
	const struct acpi_rsdp *rsdp = acpi_find_rsdp();
	acpi_process_madt(rsdp);
	acpi_process_hpet(rsdp);
	acpi_process_mcfg(rsdp);
}
