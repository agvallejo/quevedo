;-------------------------------------------------------------------------------
; Error codes
;-------------------------------------------------------------------------------
ERR_DEADEND            equ "0"
ERR_MULTIBOOT_MISMATCH equ "1"
ERR_NO_CPUID           equ "2"
ERR_NO_LONGMODE        equ "3"

;-------------------------------------------------------------------------------
; Multiboot header
;-------------------------------------------------------------------------------
MBALIGN  equ  1<<0                   ; align loaded modules on page boundaries
MEMINFO  equ  1<<1                   ; provide memory map
FLAGS    equ  MBALIGN | MEMINFO      ; this is the Multiboot 'flag' field
MAGIC    equ  0x1BADB002             ; 'magic number' lets bootloader find the header
CHECKSUM equ -(MAGIC + FLAGS)        ; checksum of above, to prove we are multiboot

section .header.bootstrap
align 4
	dd MAGIC
	dd FLAGS
	dd CHECKSUM

;-------------------------------------------------------------------------------
; Data structures
;-------------------------------------------------------------------------------
section .bootstrap
;-----------------
; GDT
;-----------------
GDT_EXECUTABLE_BIT equ 1<<43
GDT_DESCTYPE_BIT   equ 1<<44
GDT_PRESENT_BIT    equ 1<<47
GDT_LONGMODE_BIT   equ 1<<53
align 16
gdt32:
	dq 0 ; Null entry
	dq 0x00209A0000000000 ; Kernel code
	dq 0x0020920000000000 ; Kernel data
	dq 0x0020FA0000000000 ; User code
	dq 0x0020F20000000000 ; User data
.ptr:
	dw $ - gdt32 - 1
	dq gdt32

section .bss.bootstrap nobits
;-----------------
; Paging tables
; -We want to identity map the lower 32-bit
;  address space to guarantee that we have
;  access to Multiboot's junk
; -'huge' pages are used so the PT is not needed
; -1GiB pages are not that common, so weĺl use
;  just 2MiB pages and 4 PDs.
;-----------------
align 4096
pdpt32: ; Will map the first 4 GiB
	resb 4096
pd32:
.1:
	resb 4096
.2:
	resb 4096
.3:
	resb 4096
.4:
	resb 4096

;-----------------
; Stack
;-----------------
align 16
stack_bottom:
	resb 4096
stack_top:

section .bss.bootstrap.tables nobits
align 4096
pml4:
	resb 4096
pdpt64:
	resb 4096

;-------------------------------------------
; Will map the kernel
;-------------------------------------------
pd64:
	resb 4096

;-------------------------------------------
; Will map the physical memory manager
;-------------------------------------------
pd64mm:
	resb 4096
pt64mm:
	resb 4096
mm_phy: ;First memory mapped page of the mm_phy
	resb 4096

;-------------------------------------------------------------------------------
; BOOTSTRAP CODE
;-------------------------------------------------------------------------------
section .bootstrap
bits 32

;-------------------------
; Utility functions
;-------------------------
; Identity map the first 4GiB
PRESENT_FLAG   equ 1<<0
WRITABLE_FLAG  equ 1<<1
HUGESIZE_FLAG  equ 1<<7
NX_FLAG        equ 1<<63
PAGE_FLAGS     equ PRESENT_FLAG | WRITABLE_FLAG
HUGEPAGE_FLAGS equ HUGESIZE_FLAG | PAGE_FLAGS
HUGEPAGE_SIZE  equ 0x200000 ; 2MiB
identity_map:
	lea eax, [pdpt32+PAGE_FLAGS]
	mov [pml4], eax
	lea eax, [pd32.1+PAGE_FLAGS]
	mov [pdpt32], eax
	lea eax, [pd32.2+PAGE_FLAGS]
	mov [pdpt32+0x08], eax
	lea eax, [pd32.3+PAGE_FLAGS]
	mov [pdpt32+0x10], eax
	lea eax, [pd32.4+PAGE_FLAGS]
	mov [pdpt32+0x18], eax

	; Walk through 2048 entries. At 512 entries
	; per PD that means we walk through the 4 PDs
	; starting at pd32.1 with 0 and finishing at
	; the end of pd32.4 with the 4GiB mark
	; (2048 entries x 2MiB/entry = 4GiB)
	xor ecx, ecx
.keep_mapping:
	mov eax, HUGEPAGE_SIZE
	mul ecx
	or eax, HUGEPAGE_FLAGS
	mov [pd32+ecx*8], eax
	inc ecx
	cmp ecx, 2048
	jne .keep_mapping

	ret

; Map the kernel by the -2GiB mark. The flags are unimportant
; as they will be fixed when we jump into C land.
PAGE_SIZE equ 0x1000 ; 4KiB
map_kernel:
	lea eax, [pd64+PAGE_FLAGS]
	mov [pdpt64+510*8], eax

	extern __ld_kernel_lma_start
	extern __ld_kernel_lma_end
	xor ecx, ecx
	mov eax, __ld_kernel_lma_start
	or eax, HUGEPAGE_FLAGS
	mov ebx, __ld_kernel_lma_end
.keep_mapping:
	mov [pd64+ecx*8], eax
	inc ecx
	mov eax, [pd32+ecx*8] ; Use pd32 as a LUT for multiples of 2MiB with appropriate flags
	add eax, __ld_kernel_lma_start ; Add the offset into the kernel to the LUT entry
	
	cmp eax, ebx
	jl .keep_mapping
	ret

fill_paging_tables:
	; Recursive map starting at -1TiB
	lea eax, [pml4+PAGE_FLAGS]
	mov [pml4+510*8], eax

	; Kernel+mm_phy maps
	lea eax, [pdpt64+PAGE_FLAGS]
	mov [pml4+511*8], eax

	; Map the bare minimum for a working mm_phy
	; It's sandwiched between the kernel and the
	; recursive map. We have 510 GiB-8GiB (the guard
	; pages)
	lea eax, [pd64mm+PAGE_FLAGS]
	mov [pdpt64+509*8], eax
	lea eax, [pt64mm+PAGE_FLAGS]
	mov [pd64mm+511*8], eax
	lea eax, [mm_phy+PAGE_FLAGS]
	mov [pt64mm+510*8], eax

	call identity_map
	call map_kernel

	ret

PAE_BIT          equ 1<<5       ; Enable bit for PAE on cr4
LONGMODE_MSR     equ 0xC0000080 ; MSR with the bit to enable Long Mode
LONGMODE_MSR_BIT equ 1<<8       ; Enable bit for Long Mode on the MSR
PAGING_BIT       equ 1<<31      ; Enable bit for paging on cr0
enable_paging:
	; Load PML4
	mov eax, pml4
	mov cr3, eax

	; Activate PAE
	mov eax, cr4
	or eax, PAE_BIT
	mov cr4, eax

	; Activate Long-Mode
	mov ecx, LONGMODE_MSR
	rdmsr
	or eax, LONGMODE_MSR_BIT
	wrmsr

	; Enable paging
	mov eax, cr0
	or eax, PAGING_BIT
	mov cr0, eax

	; Reconfigure segmentation
	lgdt [gdt32.ptr]

	ret

TTY_BUFFER equ 0xB8000
TTY_SIZE   equ 80*25
clean_screen:
	mov edi, TTY_BUFFER
	xor eax, eax
	mov ecx, TTY_SIZE
	rep stosw
	ret

error:
	mov dword [TTY_BUFFER   ], 0x4f524f45
	mov dword [TTY_BUFFER+4 ], 0x4f3a4f52
	mov dword [TTY_BUFFER+8 ], 0x4f204f20
	mov byte  [TTY_BUFFER+10], al
.hang:
	cli
	hlt
	jmp .hang

;-------------------------
; Sanity checks
;-------------------------
MAGIC_EAX equ 0x2BADB002 ; 'magic number' returned by the bootloader
check_multiboot:
	cmp eax, MAGIC_EAX
	jne .multiboot_mismatch
	ret
.multiboot_mismatch:
	mov al, ERR_MULTIBOOT_MISMATCH
	jmp error

ID_BIT equ 1<<21
check_cpuid:
	; Flip ID bit on FLAGS and back it up on ecx
	pushfd
	pop eax
	mov ecx, eax
	xor eax, ID_BIT
	push eax
	popfd

	; Restore original FLAGS
	pushfd
	pop eax
	push ecx
	popfd

	; If the bit can be flipped, CPUID is available
	cmp eax, ecx
	je .no_cpuid
	ret
.no_cpuid:
	mov al, ERR_NO_CPUID
	jmp error

CPUID_MAX_EXTENDED  equ 0x80000000
CPUID_EXTENDED_INFO equ 0x80000001
CPUID_LONGMODE      equ 0x80000000
LONGMODE_CPUID_BIT  equ 1<<29
check_longmode:
	; Check the presence of extended CPU info.
	mov eax, CPUID_MAX_EXTENDED
	cpuid
	cmp eax, CPUID_EXTENDED_INFO
	jb .no_longmode

	; Check the presence of longmode
	mov eax, CPUID_EXTENDED_INFO
	cpuid
	test edx, LONGMODE_CPUID_BIT
	jz .no_longmode
	ret
.no_longmode:
	mov al, ERR_NO_LONGMODE
	jmp error

PICMASTER_COMMAND equ 0x20
PICMASTER_DATA    equ 0x21
PICSLAVE_COMMAND  equ 0xA0
PICSLAVE_DATA     equ 0xA1
remap_pic:
	; ICW1: Initialization
	mov al, 0x11
	out PICMASTER_COMMAND, al
	out PICSLAVE_COMMAND,  al

	; ICW2: Offsets
	mov al, 0x20;
	out PICMASTER_DATA, al
	mov al, 0x28;
	out PICSLAVE_DATA,  al

	; ICW3: Cascading defs.
	mov al, 4 ; Bit form. i.e: 4=2th IR
	out PICMASTER_DATA, al
	mov al, 2 ; Binary form. i.e: 2=2th IR
	out PICSLAVE_DATA,  al

	; ICW4: 8086 Mode
	mov al, 1
	out PICMASTER_DATA, al
	out PICSLAVE_DATA,  al

	; Mask everything
	mov al, 0xFF
	out PICMASTER_DATA, al
	out PICSLAVE_DATA,  al

	ret

;-------------------------
; Entry point
;-------------------------
global _start
_start:
	cli
	mov esp, stack_top

	push dword 0
	push ebx ; Multiboot structures (64 bits)
	push eax ; Magic word

	call clean_screen
	pop eax
	call check_multiboot
	call check_cpuid
	call check_longmode
	call fill_paging_tables
	call enable_paging

	call remap_pic
	sti ;All the interrupts are masked, this is safe

	pop edi ;Get the Multiboot structure before changing stacks
	jmp 0x8:longmode

	mov al, ERR_DEADEND
	jmp error

;-------------------------------------------------------------------------------
; 64 bits code, but still on the .bootstrap section
bits 64
longmode:
	; The higher 32 bits are undefined, as stated on the Intel Manual Vol 1.
	; Ch. 3.4.1.1 - "General-Purpose Registers in 64Bit Mode"
	; Note the leading zero to prevent sign extension
	and rdi, 0x0FFFFFFF

	; Move the stack to the higher half
	mov rsp, stack64_top

	; No need to reload the segments because they are identical, but we need
	; the higher half GDT for when we jump to usermode
	lgdt [gdt64.ptr]

	; This is needed due to a nasty bug processing interrupts before jumping
	; to usermode. null descriptors are fine in long mode, but invalid ones
	; are NOT. If we don't jump to userspace and process an interrupt the SS
	; will be evaluated on iretq, which will raise a #GPF. Now, coming from
	; usermode it's not problematic because SS is automatically set to zero
	; which renders the iretq test useless. God I hate this architecture...
	xor ax, ax
	mov ss, ax
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	call gen_tss_desc

	; Jump to C
	extern init
	mov rax, init
	call rax

	cli
.hang:
	hlt
	jmp .hang

gen_tss_desc:
	; Set RSP0
	;--------------------------------
	mov qword [tss64 + 4], stack64_top

	; Set the base
	;--------------------------------
	lea rax, [tss64]
	and rax, 0xFFFF
	sal rax, 16

	lea rbx, [tss64]
	sar rbx, 16
	and rbx, 0xFF
	sal rbx, 32
	or rax, rbx

	lea rbx, [tss64]
	sar rbx, 24
	and rbx, 0xFF
	sal rbx, 24+32
	or rax, rbx

	; Set the limit
	;--------------------------------
	add rax, tss64.end - tss64

	; Load GDT descriptor
	;--------------------------------
	or [gdt64.tss_lo], rax
	lea rax, [tss64]
	sar rax, 32
	mov dword [gdt64.tss_hi], eax

	; Load the TR
	;--------------------------------
	mov ax, 0x2B
	ltr ax

	ret

;-------------------------------------------------------------------------------
; GDT
;-------------------------------------------------------------------------------
;                           DATA SEGMENT DESCRIPTOR
;
;  31                23                15                7               0
; +-----------------+-+-+-+-+---------+-+-----+---------+-----------------+
; |#################|#|#|#|A| LIMIT   |#|     |  TYPE   |#################|
; |###BASE 31..24###|G|B|L|V| 19..16  |P| DPL |         |###BASE 23..16###| 4
; |#################|#|#|#|L|         |#|     |1|0|E|W|A|#################|
; |-----------------+-+-+-+-+---------+-+-----+-+-+-+-+-+-----------------|
; |###################################|                                   |
; |########SEGMENT BASE 15..0#########|        SEGMENT LIMIT 15..0        | 0
; |###################################|                                   |
; +-----------------+-----------------+-----------------+-----------------+
;
;                        EXECUTABLE SEGMENT DESCRIPTOR
;
;  31                23                15                7               0
; +-----------------+-+-+-+-+---------+-+-----+---------+-----------------+
; |#################|#|#|#|A| LIMIT   |#|     |  TYPE   |#################|
; |###BASE 31..24###|G|D|L|V| 19..16  |P| DPL |         |###BASE 23..16###| 4
; |#################|#|#|#|L|         |#|     |1|0|C|R|A|#################|
; |-----------------+-+-+-+-+---------+-+-----+-+-+-+-+-+-----------------|
; |###################################|                                   |
; |########SEGMENT BASE 15..0#########|        SEGMENT LIMIT 15..0        | 0
; |###################################|                                   |
; +-----------------+-----------------+-----------------+-----------------+
;
;                         SYSTEM SEGMENT DESCRIPTOR
;
;  31                23                15                7               0
; +-----------------+-+-+-+-+---------+-+-----+-+-------+-----------------+
; |#################|#|#|#|A| LIMIT   |#|     | |       |#################|
; |###BASE 31..24###|G|X|L|V| 19..16  |P| DPL |0| TYPE  |###BASE 23..16###| 4
; |#################|#|#|#|L|         |#|     | |       |#################|
; |-----------------+-+-+-+-+---------+-+-----+-+-------+-----------------|
; |###################################|                                   |
; |########SEGMENT BASE 15..0#########|       SEGMENT LIMIT 15..0         | 0
; |###################################|                                   |
; +-----------------+-----------------+-----------------+-----------------+
;        A   - ACCESSED                              E   - EXPAND-DOWN
;        AVL - AVAILABLE FOR PROGRAMMERS USE         G   - GRANULARITY
;        B   - BIG                                   P   - SEGMENT PRESENT
;        C   - CONFORMING                            R   - READABLE
;        D   - DEFAULT                               W   - WRITABLE
;        DPL - DESCRIPTOR PRIVILEGE LEVEL            L   - LONG MODE

section .rodata
align 8
gdt64:
.null:        dq 0
.kernel_code: dq 0x002F9A000000FFFF ; Data is set to null
.kernel_data: dq 0x002F92000000FFFF ; Shouldn't be used
.user_code:   dq 0x002FFA000000FFFF
.user_data:   dq 0x002FF2000000FFFF
.tss_lo:      dq 0x0000E90000000000 ; Populated at runtime
.tss_hi:      dq 0x0000000000000000 ; Populated at runtime
.ptr:
	dw $ - gdt64 - 1
	dq gdt64

section .bss nobits
align 16
stack64_bottom:
	resb 4096
stack64_top:

align 8
global tss64
tss64:
	resb 0x68
.end:
