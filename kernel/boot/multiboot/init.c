#include <quevedo/hal.h>
#include <quevedo/ld.h>
#include <quevedo/mm.h>
#include <quevedo/panic.h>

#include <cpuinfo/cpuinfo.h>
#include <fb/fb.h>
#include <mm/mm_heap.h>
#include <isr/isr.h>
#include <time/time.h>
#include <proc/proc.h>
#include <vfs/vfs.h>

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <q_libc/init.h>

#include "init_acpi.h"
#include "init_private.h"

#include "mm/vmm.h"

#define LOG_MODULE init
#include <log/log.h>

/** Run some dummy allocations and frees and check if everything works ok */
static void init_sanity(void)
{
	kassert_true(mm_virt_ismapped(0), 0);
	kassert_true(!mm_virt_ismapped(4ull<<30), 0);

	size_t old_usage = mm_phy_getusage();
	uintptr_t addr = 4ULL<<30;
	size_t    size = 1ULL<<20;
	vmm_alloc(addr, size);
	kassert(mm_phy_getusage(), <, old_usage);
	kassert_true(mm_virt_ismapped(addr), addr);
	kassert(mm_virt_getbsize(addr), ==, MM_VIRT_BSIZE_4KiB);

	mm_virt_free(addr, size, MM_VIRT_BSIZE_4KiB);

	/* Check for memory leaks */
	kassert(mm_phy_getusage(), ==, old_usage);
	kassert_true(mm_virt_ismapped(ld_get(kernel_text_start)), ld_get(kernel_text_start));
	kassert(mm_virt_getbsize(ld_get(kernel_text_start)), ==, MM_VIRT_BSIZE_2MiB);
}

static void init_relocate_initrd(void *arg)
{
	// Relocate initrd to kernel space
	char *modname = "initrd";
	uintptr_t initrd_addr = init_mb_getmod_addr(arg, modname);
	size_t    initrd_size = init_mb_getmod_size(arg, modname);
	kassert(initrd_size, <=, ld_get(initrd_end)-ld_get(initrd_start));
	memcpy((void*)ld_get(initrd_start), (void*)initrd_addr, initrd_size);
}

void init(void *arg)
{
	/* Enable framebuffer on 0xB8000 virtual.
	 * It's not yet relocated into kernel space
	 * but we want text ASAP
	 */
	fb_init((void*)0xB8000, 80, 25);
	fb_clear();

	/* Start logging ASAP */
	log_init();

	/* Recover data from the CPU */
	cpuinfo_init();

	/* Enable interrupts */
	isr_init();

	// Enable memory manager
	mm_virt_init();
	mm_phy_init();
	mm_heap_init((void*)ld_get(heap_start),
		    ld_get(heap_end)-ld_get(heap_start));

	// Sanity check on the argument
	init_arg_parse(arg);

	/********************* Memory managers are usable *********************/

	// Parse ACPI tables
	acpi_init();

	// Now we can relocate the framebuffer. We have to, in fact, or we
	// won't be able to print anything after we unmap the lower 4GiB
	vmm_map(ld_get(fb_start), 0xB8000);
	fb_init((void*)ld_get(fb_start), 80, 25);

	init_relocate_initrd(arg);

	// Interesting info. for the user
	log_info("Available pages for use: 0x%llX", mm_phy_getusage());

	// Sanity check on mm_virt. I trust my code, BUT...
	init_sanity();

	// Initialise timers
	time_init();

	// Initialise the virtual FS
	vfs_init();

	// Initialise scheduler
	proc_init();

	panic("Deadend while initialising", 0);
}
