#ifndef INIT_PRIVATE_H
#define INIT_PRIVATE_H

#include <stdint.h>

/* Ease unit testing on 64-bit architectures */
#ifdef UNIT_TESTING
#define MB_WARNING(x) ((void)0)
typedef uintptr_t addr_t;
#else
#define MB_WARNING(x) puts("[WARNING] " x)
typedef uint32_t addr_t;
#endif


struct init_mb_info {
	uint32_t flags;
#define INIT_MB_MMAP_AVAIL (1U<<6)
	uint32_t mem_lower;
	uint32_t mem_upper;
	uint32_t boot_device;
	uint32_t cmdline;
	uint32_t mods_count;
	addr_t   mods_addr;
	uint32_t elf_sections[4];
	uint32_t mmap_len;
	addr_t   mmap_addr;
	// The struct actually holds more info, but I dont need it yet
};

struct init_mb_mmap {
	uint32_t size;
	uint64_t addr;
	uint64_t len;
#define INIT_MB_MMAP_ENTRY_AVAIL (1U)
#define INIT_MB_MMAP_ENTRY_RSVD  (6U)
	uint32_t type;
} __attribute__((packed));

struct init_mb_mod {
	uint32_t mod_start;
	uint32_t mod_end;
	uint32_t cmdline;
	uint32_t pad;
} __attribute__((packed));

void init_arg_parse(void *arg);

uintptr_t init_mb_getmod_addr(void *arg, char *modname);
size_t    init_mb_getmod_size(void *arg, char *modname);

#endif // INIT_PRIVATE_H
