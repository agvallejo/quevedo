#include <stdbool.h>
#include <stdio.h>

#include <munit.h>
#include <isr/isr.h>
#include <quevedo/ld.h>
#include <stdint.h>
#include <stddef.h>

#include "../init_private.h"

bool init_mb_mmap_overlaps(void*, void*);
void init_mb_mmap_merge(void*, void*);
void init_mb_mmap_sanitise(void*);
void init_mb_mmap_print(void*);
void mm_phy_free(uintptr_t page)
{
	(void)page;
}

struct fixture_t{
	struct init_mb_info mb_info;
	struct init_mb_mmap mmap[10];
} fixture;
struct ld_symbols ld_fake_symbols;

static void *setup(const MunitParameter params[], void* data)
{
	(void) params; (void) data;
	memset(&fixture, 0, sizeof fixture);

	fixture.mb_info.flags     = INIT_MB_MMAP_AVAIL;
	fixture.mb_info.mmap_addr = (uintptr_t)fixture.mmap;
	fixture.mb_info.mmap_len  = sizeof fixture.mmap;

	ld_fake_symbols=(struct ld_symbols){0};

	for (size_t i=0; i<(sizeof fixture.mmap/sizeof *fixture.mmap); i++)
		fixture.mmap[i].size = sizeof *fixture.mmap - sizeof fixture.mmap->size;

	return &fixture;
}

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------
static MunitResult test_init_mb_overlaps_lut(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	struct {
		bool ret;
		uint64_t addr0;
		uint64_t len0;
		uint64_t addr1;
		uint64_t len1;
	} test_lut[] = {
		{.ret = false, .addr0=0, .len0=1, .addr1=1, .len1=1}, //Disjoint
		{.ret = false, .addr0=1, .len0=1, .addr1=0, .len1=1}, //Disjoint
		{.ret = true,  .addr0=0, .len0=2, .addr1=1, .len1=2}, //Overlapped
		{.ret = true,  .addr0=1, .len0=2, .addr1=0, .len1=2}, //Overlapped
		{.ret = true,  .addr0=0, .len0=4, .addr1=1, .len1=2}, //Embedded
		{.ret = true,  .addr0=1, .len0=2, .addr1=0, .len1=4}  //Embedded
	};
	for (size_t i=0; i<sizeof test_lut/sizeof *test_lut; i++) {
		fixture.mmap[0].addr = test_lut[i].addr0;
		fixture.mmap[0].len  = test_lut[i].len0;
		fixture.mmap[1].addr = test_lut[i].addr1;
		fixture.mmap[1].len  = test_lut[i].len1;
		munit_assert_true(test_lut[i].ret == init_mb_mmap_overlaps(fixture.mmap, fixture.mmap+1));
	}

	return MUNIT_OK;
}

static MunitResult test_init_mb_merge_lut(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	struct {
		uint64_t addr0;
		uint64_t len0;
		uint64_t addr1;
		uint64_t len1;
		uint64_t ret_addr;
		uint64_t ret_len;
	} test_lut[] = {
		{.addr0=0, .len0=2, .addr1=1, .len1=2, .ret_addr=0, .ret_len=3}, //Overlapped
		{.addr0=1, .len0=2, .addr1=0, .len1=2, .ret_addr=0, .ret_len=3}, //Overlapped
		{.addr0=0, .len0=4, .addr1=1, .len1=2, .ret_addr=0, .ret_len=4}, //Embedded
		{.addr0=1, .len0=2, .addr1=0, .len1=4, .ret_addr=0, .ret_len=4}  //Embedded
	};
	for (size_t i=0; i<sizeof test_lut/sizeof *test_lut; i++) {
		fixture.mmap[0].addr = test_lut[i].addr0;
		fixture.mmap[0].len  = test_lut[i].len0;
		fixture.mmap[0].type = INIT_MB_MMAP_ENTRY_AVAIL;
		fixture.mmap[1].addr = test_lut[i].addr1;
		fixture.mmap[1].len  = test_lut[i].len1;
		fixture.mmap[1].type = INIT_MB_MMAP_ENTRY_AVAIL;
		init_mb_mmap_merge(fixture.mmap, fixture.mmap+1);
		munit_assert_uint(fixture.mmap[0].addr, ==, test_lut[i].ret_addr);
		munit_assert_uint(fixture.mmap[0].len, ==, test_lut[i].ret_len);
		munit_assert_int(fixture.mmap[1].type, ==, INIT_MB_MMAP_ENTRY_RSVD);
	}

	return MUNIT_OK;
}

static MunitResult test_init_mb_sanitise_merges_overlap(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	fixture.mmap[0].addr = 0x100000;
	fixture.mmap[0].len  = 0x2000;
	fixture.mmap[0].type = INIT_MB_MMAP_ENTRY_AVAIL;
	fixture.mmap[1].type = 666; // Non-valid type
	fixture.mmap[2].addr = 0x101000;
	fixture.mmap[2].len  = 0x3000;
	fixture.mmap[2].type = INIT_MB_MMAP_ENTRY_AVAIL;

	init_mb_mmap_sanitise(&fixture.mb_info);

	munit_assert_uint(fixture.mmap[0].addr, ==, 0x100000);
	munit_assert_uint(fixture.mmap[0].len,  ==, 0x4000);
	munit_assert_int(fixture.mmap[2].type, ==, INIT_MB_MMAP_ENTRY_RSVD);

	return MUNIT_OK;
}

static MunitResult test_init_mb_sanitise_nonaligned_addr(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	fixture.mmap[0].addr = 0x100010;
	fixture.mmap[0].len  = 0x2000;
	fixture.mmap[0].type = INIT_MB_MMAP_ENTRY_AVAIL;
	init_mb_mmap_sanitise(&fixture.mb_info);
	munit_assert_uint(fixture.mmap[0].addr, ==, 0x101000);
	munit_assert_uint(fixture.mmap[0].len,  ==, 0x1000);
	munit_assert_int(fixture.mmap[0].type, ==, INIT_MB_MMAP_ENTRY_AVAIL);

	return MUNIT_OK;
}

static MunitResult test_init_mb_sanitise_merges_complex_overlap(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	//both [0] and [1] are disjoint (among themselves) and mergeable with [2]
	//Result is just 1 element on [0]
	fixture.mmap[0].addr = 0x100000;
	fixture.mmap[0].len  = 0x2000;
	fixture.mmap[0].type = INIT_MB_MMAP_ENTRY_AVAIL;
	fixture.mmap[1].addr = 0x200000;
	fixture.mmap[1].len  = 0x2000;
	fixture.mmap[1].type = INIT_MB_MMAP_ENTRY_AVAIL;
	fixture.mmap[2].addr = 0x101000;
	fixture.mmap[2].len  = 0x100000;
	fixture.mmap[2].type = INIT_MB_MMAP_ENTRY_AVAIL;

	init_mb_mmap_sanitise(&fixture.mb_info);

	munit_assert_uint(fixture.mmap[0].addr, ==, 0x100000);
	munit_assert_uint(fixture.mmap[0].len,  ==, 0x102000);
	munit_assert_int(fixture.mmap[1].type, ==, INIT_MB_MMAP_ENTRY_RSVD);
	munit_assert_int(fixture.mmap[2].type, ==, INIT_MB_MMAP_ENTRY_RSVD);

	return MUNIT_OK;
}

static MunitResult test_init_mb_sanitise_merges_real_qemu(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	// QEMU default memory map
	fixture.mmap[0] = (struct init_mb_mmap){.size=0x14, .addr=0x00000000, .len=0x009FC00, .type=1}; // Conv. mem.
	fixture.mmap[1] = (struct init_mb_mmap){.size=0x14, .addr=0x0009FC00, .len=0x0000400, .type=2}; // EBDA
	fixture.mmap[2] = (struct init_mb_mmap){.size=0x14, .addr=0x000F0000, .len=0x0010000, .type=2}; // BIOS
	fixture.mmap[3] = (struct init_mb_mmap){.size=0x14, .addr=0x00100000, .len=0x7EE0000, .type=1}; // Ext. mem.
	fixture.mmap[4] = (struct init_mb_mmap){.size=0x14, .addr=0x07EE0000, .len=0x0020000, .type=2}; // BIOS
	fixture.mmap[5] = (struct init_mb_mmap){.size=0x14, .addr=0xFFFC0000, .len=0x0040000, .type=2}; // BIOS
	struct fixture_t fixture_backup = fixture;
	fixture_backup.mb_info.mmap_addr = (uintptr_t)fixture_backup.mmap;

	init_mb_mmap_sanitise(&fixture.mb_info);

	fixture_backup.mmap[0] = (struct init_mb_mmap){.size=0x14, .addr=0x00001000, .len=0x009E000, .type=1}; // Conv. mem.
	fixture_backup.mmap[1] = (struct init_mb_mmap){.size=0x14, .addr=0x0009FC00, .len=0x0000400, .type=2}; // EBDA
	fixture_backup.mmap[2] = (struct init_mb_mmap){.size=0x14, .addr=0x000F0000, .len=0x0010000, .type=2}; // BIOS
	fixture_backup.mmap[3] = (struct init_mb_mmap){.size=0x14, .addr=0x00100000, .len=0x7EE0000, .type=1}; // Ext. mem.
	fixture_backup.mmap[4] = (struct init_mb_mmap){.size=0x14, .addr=0x07EE0000, .len=0x0020000, .type=2}; // BIOS
	fixture_backup.mmap[5] = (struct init_mb_mmap){.size=0x14, .addr=0xFFFC0000, .len=0x0040000, .type=2}; // BIOS

	munit_assert_false(memcmp(fixture.mmap,fixture_backup.mmap,sizeof fixture.mmap));

	return MUNIT_OK;
}

static MunitResult test_init_mb_sanitise_merges_unrealistic_data(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	// memory map with overlaps + out of order + zero size. The true unrealistic stress test
	fixture.mmap[0] = (struct init_mb_mmap){.size=0x14, .addr=0x000000000, .len=0x009FC00, .type=1}; // Conv. mem.
	fixture.mmap[1] = (struct init_mb_mmap){.size=0x14, .addr=0x00009FC00, .len=0x0000400, .type=2}; // EBDA
	fixture.mmap[2] = (struct init_mb_mmap){.size=0x14, .addr=0x0000F0000, .len=0x0010000, .type=2}; // BIOS
	fixture.mmap[3] = (struct init_mb_mmap){.size=0x14, .addr=0x000200000, .len=0x7EE0000, .type=1}; // Ext. mem.
	fixture.mmap[4] = (struct init_mb_mmap){.size=0x14, .addr=0x000100000, .len=0x7EE0000, .type=1}; // Ext. mem.
	fixture.mmap[5] = (struct init_mb_mmap){.size=0x14, .addr=0x100002000, .len=0x0003000, .type=1}; // (>32bits)
	fixture.mmap[6] = (struct init_mb_mmap){.size=0x14, .addr=0x100002000, .len=0x0003000, .type=1}; // (>32bits)
	fixture.mmap[7] = (struct init_mb_mmap){.size=0x14, .addr=0x100000000, .len=0x0001000, .type=3}; // (>32bits)
	fixture.mmap[8] = (struct init_mb_mmap){.size=0x14, .addr=0x007EE0000, .len=0x0020000, .type=2}; // BIOS
	fixture.mmap[9] = (struct init_mb_mmap){.size=0x14, .addr=0x200000000, .len=0x0000000, .type=1}; // nonsense
	struct fixture_t fixture_backup = fixture;
	fixture_backup.mb_info.mmap_addr = (uintptr_t)fixture_backup.mmap;

	init_mb_mmap_sanitise(&fixture.mb_info);

	// Expected changes
	fixture_backup.mmap[0] = (struct init_mb_mmap){.size=0x14, .addr=0x000001000, .len=0x009E000, .type=1}; // Conv. mem.
	fixture_backup.mmap[1] = (struct init_mb_mmap){.size=0x14, .addr=0x00009FC00, .len=0x0000400, .type=2}; // EBDA
	fixture_backup.mmap[2] = (struct init_mb_mmap){.size=0x14, .addr=0x0000F0000, .len=0x0010000, .type=2}; // BIOS
	fixture_backup.mmap[3] = (struct init_mb_mmap){.size=0x14, .addr=0x000100000, .len=0x7FE0000, .type=1}; // Ext. mem.
	fixture_backup.mmap[4] = (struct init_mb_mmap){.size=0x14, .addr=0x000100000, .len=0x7EE0000, .type=6}; // Ext. mem.
	fixture_backup.mmap[5] = (struct init_mb_mmap){.size=0x14, .addr=0x100002000, .len=0x0003000, .type=1}; // (>32bits)
	fixture_backup.mmap[6] = (struct init_mb_mmap){.size=0x14, .addr=0x100002000, .len=0x0003000, .type=6}; // (>32bits)
	fixture_backup.mmap[7] = (struct init_mb_mmap){.size=0x14, .addr=0x100000000, .len=0x0001000, .type=3}; // (>32bits)
	fixture_backup.mmap[8] = (struct init_mb_mmap){.size=0x14, .addr=0x007EE0000, .len=0x0020000, .type=2}; // BIOS
	fixture_backup.mmap[9] = (struct init_mb_mmap){.size=0x14, .addr=0x200000000, .len=0x0000000, .type=6}; // nonsense

	munit_assert_false(memcmp(fixture.mmap,fixture_backup.mmap,sizeof fixture.mmap));

	return MUNIT_OK;
}

//------------------------------------------------------------------------------
// Test runner
//------------------------------------------------------------------------------
static MunitTest test_suite_tests[] = {
	// Name, function, setup, teardown, options, parameters
	{"/init/init_mb/overlaps", test_init_mb_overlaps_lut, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/init/init_mb/merge", test_init_mb_merge_lut, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/init/init_mb/sanitise/simple", test_init_mb_sanitise_merges_overlap, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/init/init_mb/sanitise/complex", test_init_mb_sanitise_merges_complex_overlap, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/init/init_mb/sanitise/nonaligned_addr", test_init_mb_sanitise_nonaligned_addr, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/init/init_mb/sanitise/qemu_default", test_init_mb_sanitise_merges_real_qemu, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/init/init_mb/sanitise/unrealistic", test_init_mb_sanitise_merges_unrealistic_data, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	// NULL ENTRY @ END
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};


static const MunitSuite test_suite = {
	"", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
