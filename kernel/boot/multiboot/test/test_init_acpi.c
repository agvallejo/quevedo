#include <munit.h>
#include <fff.h>

#include <stddef.h>

#include "../init_acpi_private.h"

DEFINE_FFF_GLOBALS
#include <quevedo/mm.h>
FAKE_VOID_FUNC(mm_virt_map, mm_phy_addr_t, uintptr_t, size_t, enum mm_virt_bsize, enum mm_virt_prot)
#include <isr/apic.h>
FAKE_VOID_FUNC0(apic_init)
FAKE_VOID_FUNC(apic_ioapic_set_ioredtbl, size_t)

#define RESET_ALL_FAKES() \
	do { \
		FFF_RESET_HISTORY(); \
		RESET_FAKE(mm_virt_map); \
	} while(0)

#include <quevedo/ld.h>
struct ld_symbols ld_fake_symbols;

struct {
	uint16_t rsvd1[3*16];
	struct acpi_rsdp rsdp;
} fixture;

static uint8_t get_checksum(void *rsdp)
{
	uint8_t *p= rsdp;
	uint8_t sum = 0;
	for (size_t i=0; i<sizeof(struct acpi_rsdp); i++)
		sum += p[i];

	return -sum;
}

static void *setup(const MunitParameter params[], void* data)
{
	(void) params;

	RESET_ALL_FAKES();

	memset(&fixture, 0, sizeof fixture);
	memcpy(fixture.rsdp.signature, "RSD PTR ", 8);
	fixture.rsdp.revision = 2;
	fixture.rsdp.length = sizeof fixture.rsdp;
	fixture.rsdp.checksum = get_checksum(&fixture.rsdp);
	munit_assert_true(acpi_checksum_isvalid(&fixture.rsdp, fixture.rsdp.length));
	return data;
}


//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------
static MunitResult test_checksum_valid(const MunitParameter params[], void* data)
{
	(void) params; (void) data;
	uint8_t chunk[]= {0x80, 0x80, 0x80, 0x80};
	munit_assert_true(acpi_checksum_isvalid(chunk, sizeof chunk));
	return MUNIT_OK;
}

static MunitResult test_checksum_invalid(const MunitParameter params[], void* data)
{
	(void) params; (void) data;
	uint8_t chunk[]= {0x80, 0x80, 0x80};
	munit_assert_false(acpi_checksum_isvalid(chunk, sizeof chunk));
	return MUNIT_OK;
}

static MunitResult test_find_rsdp_found(const MunitParameter params[], void* data)
{
	(void) params; (void) data;
	munit_assert_ptr(&fixture.rsdp, ==, acpi_find_rsdp_in_range((uintptr_t)&fixture,(uintptr_t)&fixture+sizeof fixture));
	return MUNIT_OK;
}

static MunitResult test_find_rsdp_notfound_signature(const MunitParameter params[], void* data)
{
	(void) params; (void) data;
	fixture.rsdp.signature[0] = 0;
	munit_assert_null(acpi_find_rsdp_in_range((uintptr_t)&fixture,(uintptr_t)&fixture+sizeof fixture));
	return MUNIT_OK;
}

static MunitResult test_find_rsdp_notfound_revision(const MunitParameter params[], void* data)
{
	(void) params; (void) data;
	fixture.rsdp.revision = 3;
	fixture.rsdp.checksum = get_checksum(&fixture.rsdp);
	munit_assert_null(acpi_find_rsdp_in_range((uintptr_t)&fixture,(uintptr_t)&fixture+sizeof fixture));
	return MUNIT_OK;
}

static MunitResult test_find_rsdp_notfound_badsum(const MunitParameter params[], void* data)
{
	(void) params; (void) data;
	fixture.rsdp.checksum++;
	munit_assert_null(acpi_find_rsdp_in_range((uintptr_t)&fixture,(uintptr_t)&fixture+sizeof fixture));
	return MUNIT_OK;
}

//------------------------------------------------------------------------------
// Test runner
//------------------------------------------------------------------------------
static MunitTest test_suite_tests[] = {
	// Name, function, setup, teardown, options, parameters
	{"/init/acpi/checksum_valid", test_checksum_valid, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/init/acpi/checksum_invalid", test_checksum_invalid, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/init/acpi/find_rsdp/found", test_find_rsdp_found, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/init/acpi/find_rsdp/not_found/signature", test_find_rsdp_notfound_signature, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/init/acpi/find_rsdp/not_found/revision", test_find_rsdp_notfound_revision, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/init/acpi/find_rsdp/not_found/badsum", test_find_rsdp_notfound_badsum, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	// NULL ENTRY @ END
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};


static const MunitSuite test_suite = {
	"", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
