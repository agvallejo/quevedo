TESTNAME:=test_init_mb
ENV:=test
TEST:=$(TESTSDIR)$(ENV)/$(TESTNAME)
TESTS+=$(TEST)

ASMS:=
SRCS:=\
    $(DIR)../init_mb.c \
    $(DIR)test_init_mb.c \
    $(SRCS<test>)
$(eval $(call getobjs,$(TEST)))

CC:=$(CC<$(ENV)>)
CFLAGS:=$(CFLAGS<default>)
CPPFLAGS:=-I$(INCLUDE<test>) -I$(INCLUDE<kernel>) -DSTATIC= -DUNIT_TESTING
LDFLAGS:=
LDLIBS:=
$(eval $(call spawn_c_rule,$(TEST)))
$(eval $(call spawn_link_rule,$(TEST)))
