#ifndef QUEVEDO_INIT_ACPI_H
#define QUEVEDO_INIT_ACPI_H

/** Parse ACPI tables */
void acpi_init(void);

#endif // QUEVEDO_INIT_ACPI_H
