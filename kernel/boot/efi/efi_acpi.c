#include <efi.h>

#include <quevedo/boot.h>
#include <quevedo/panic.h>

#include "efi_acpi_private.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

extern EFI_SYSTEM_TABLE *gST;

STATIC bool acpi_checksum_isvalid(const void *start, size_t octets)
{
	const uint8_t *p = start;
	uint8_t checksum = 0;
	for (size_t i=0; i<octets; i++)
		checksum += *p++;

	// The checksum fields ensure the table itself sums zero
	return !checksum;
}

static void acpi_validate_rsdp(void *rsdp_addr)
{
	/* The RSDP is 16-byte aligned */
	const struct acpi_rsdp *rsdp = (void*)rsdp_addr;
	kassert_bool(!memcmp(rsdp->signature, "RSD PTR ", 8), 0);

	/* '0' is ACPI 1.0 and '2' the rest. Other values are invalid */
	size_t size = 0;
	switch(rsdp->revision) {
		case 0: size=20; break;
		case 2: size=rsdp->length; break;
		default: panic("Invalid version", rsdp->revision);
	}

	kassert_bool(acpi_checksum_isvalid(rsdp_addr, size), 0);

	// TODO: Check the checksums of XSDT or RSDT

	printf("Found ACPI tables. RSDP->revision=%u (ACPI %s)", rsdp->revision, rsdp->revision?">=2.0":"1.0");
}

static bool acpi_sdt_matchessignature(char *signature, const struct acpi_sdt_hdr *sdt)
{
	kassert_bool(acpi_checksum_isvalid(sdt, sdt->length), 0);
	return !memcmp(sdt->signature, signature, strlen(signature));
}

static const void *acpi_find_table(const struct acpi_rsdp *rsdp, char *signature)
{
	const struct acpi_sdt_hdr *sdt;
	if (rsdp->revision == 0) {
		const struct acpi_rsdt *rsdt = (void*)(uintptr_t)rsdp->rsdt_addr;
		size_t max_idx = (rsdt->hdr.length-sizeof *rsdt)/sizeof(uint32_t);
		for (size_t i=0; i<max_idx; i++) {
			sdt = (void*)(uintptr_t)rsdt->entry[i];
			if (acpi_sdt_matchessignature(signature, sdt))
				return sdt;
		}
	}
	if (rsdp->revision == 2) {
		const struct acpi_xsdt *xsdt = (void*)rsdp->xsdt_addr;
		size_t max_idx = (xsdt->hdr.length-sizeof *xsdt)/sizeof(uint64_t);
		for (size_t i=0; i<max_idx; i++) {
			sdt = (void*)(uint64_t)xsdt->entry[i];
			if (acpi_sdt_matchessignature(signature, sdt))
				return sdt;
		}
	}
	return NULL;
}

static void acpi_process_hpet(const struct acpi_rsdp *rsdp, struct hbt* hbt)
{
	const struct acpi_hpet *hpet = acpi_find_table(rsdp, "HPET");
	hbt->hpet.present = 0;
	if (hpet)
	{
		hbt->hpet.present = 1;
		hbt->hpet.base = hpet->base_addr;
	}
}

static void acpi_process_madt(const struct acpi_rsdp *rsdp, struct hbt* hbt)
{
	const struct acpi_madt *madt = acpi_find_table(rsdp, "APIC");
	kassert_notnull(madt, 0);
	hbt->apic.present = 1;
	hbt->apic.lapic_base = madt->lapic_addr;

	hbt->mp.cpumap_size = 0;
        size_t len = madt->hdr.length - sizeof *madt;
	const struct acpi_madt_entry *entry = (void*)((uintptr_t)madt->entry);
        while(len) {
		switch(entry->type)
		{
                case ACPI_MADT_ENTRY_PROCESSOR_LAPIC:
			if (entry->lapic.flags != 1)
				break; /* Processor not enabled */
			hbt->mp.cpumap[hbt->mp.cpumap_size].apic_id = entry->lapic.apic_id;
			hbt->mp.cpumap_size++;
			break;
		default:
			break;
		}
                /* Sizes are expected to match */
                kassert(len, >=, entry->length);
                len -= entry->length;
                entry = (void*)(entry->length + (uintptr_t)entry);;
        }
}

void efi_acpi_init(struct hbt *hbt)
{
	EFI_CONFIGURATION_TABLE *tab = gST->ConfigurationTable;
	EFI_GUID guidAcpi = EFI_ACPI_20_TABLE_GUID;
	bool found = false;
	for (size_t i=0; i<gST->NumberOfTableEntries; i++)
	{
		if (! memcmp(&tab->VendorGuid, &guidAcpi, sizeof(EFI_GUID)))
		{
			found = true;
			break;
		}
		tab++;
	}
	kassert_bool(found, gST->NumberOfTableEntries);
	struct acpi_rsdp *rsdp = tab->VendorTable;
	acpi_validate_rsdp(rsdp);

	/* Cool, we found the ACPI tables */

	acpi_process_hpet(rsdp, hbt);
	acpi_process_madt(rsdp, hbt);
}
