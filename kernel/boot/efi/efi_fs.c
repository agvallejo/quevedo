#include <efi.h>
#include <efi-bs.h>
#include <protocol/efi-lip.h>
#include <protocol/efi-sfsp.h>

#include <quevedo/panic.h>

#include "efi_fs.h"

#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

extern EFI_SYSTEM_TABLE *gST;

#define PAGE_SIZE 4096u

void efi_fs_read(EFI_HANDLE img_handle, CHAR16 *fname, size_t *fsize, uintptr_t *fstart)
{
	/* For checks on return status of boot services */
	EFI_STATUS status;

	/* We want the filesystem over the same device we booted from
	 * That can be extracted from the loaded image protocol
	 * Because it's an EFI partition it will support Simple File System
	 *
	 * Thank god for that
	 */
	EFI_GUID guidLoadedImage = EFI_LOADED_IMAGE_PROTOCOL_GUID;
	EFI_LOADED_IMAGE_PROTOCOL *protoLoadedImage = NULL;
	kassert(EFI_SUCCESS, ==, gST->BootServices->HandleProtocol(img_handle,
	                                                           &guidLoadedImage,
	                                                           (VOID**)&protoLoadedImage));

	/* Use the device handle from the loaded image. That's the boot device */
	EFI_GUID guidSimpleFileSystem = EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID;
	EFI_SIMPLE_FILE_SYSTEM_PROTOCOL *protoSimpleFileSystem;
	kassert(EFI_SUCCESS, ==, gST->BootServices->HandleProtocol(protoLoadedImage->DeviceHandle,
	                                                           &guidSimpleFileSystem,
	                                                           (VOID **)&protoSimpleFileSystem));

	/* Get the device out of the way and get a reference in VFS terms to the Root folder */
	EFI_FILE_PROTOCOL *protoFileRoot;
	kassert(EFI_SUCCESS, ==, protoSimpleFileSystem->OpenVolume(protoSimpleFileSystem, &protoFileRoot));

	/* Open the requested file */
	EFI_FILE_PROTOCOL *protoFileKernel;
	status = protoFileRoot->Open(protoFileRoot, &protoFileKernel, fname, EFI_FILE_MODE_READ, 0);
	if (status == EFI_NOT_FOUND)
	{
		printf("File not found: ");
		gST->ConOut->OutputString(gST->ConOut, fname);
		printf("\n");
	}
	kassert(status, ==, EFI_SUCCESS);

	/* Find the file size */
	EFI_GUID guidFileInfo = EFI_FILE_INFO_ID;
	EFI_FILE_INFO *fileInfo = NULL;
	size_t fileInfoSize = 0;
	kassert(EFI_BUFFER_TOO_SMALL, ==, protoFileKernel->GetInfo(protoFileKernel, &guidFileInfo,
	                                                           &fileInfoSize, &fileInfo));
	kassert(EFI_SUCCESS, ==, gST->BootServices->AllocatePool(EfiLoaderData, fileInfoSize,
                                                                 (VOID**)&fileInfo));
	kassert(EFI_SUCCESS, ==, protoFileKernel->GetInfo(protoFileKernel, &guidFileInfo,
	                                                  &fileInfoSize, fileInfo));
	*fsize = fileInfo->FileSize;
	kassert(EFI_SUCCESS, ==, gST->BootServices->FreePool(fileInfo));

	/* Round up the number of required pages */
	size_t npages = (*fsize+PAGE_SIZE-1)/PAGE_SIZE;

	/* Retrieve the file */
	kassert(EFI_SUCCESS, ==, gST->BootServices->AllocatePages(AllocateAnyPages, EfiLoaderData, npages, fstart));
	kassert(EFI_SUCCESS, ==, protoFileKernel->Read(protoFileKernel, fsize, (void*)*fstart));
}
