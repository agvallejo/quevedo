section .rodata
bits 16
global efi_trampoline_ap_asm_start:
efi_trampoline_ap_asm_start:
start:
	cli

	mov edi, [cs:0xFF8] ; Find actual address of [cs:0]

	; Find the start of the GDT. The actual address
	; of "start" is in [cs:0], or the entry before
	; last on the HBT
	mov eax, edi
	add eax, gdt-start  ; Add the offset to the GDT
	mov dword [cs:gdt.addr-start], eax

	; Override the entry point
	mov eax, edi
	add eax, start.long-start
	mov dword [cs:start.entry_addr-start], eax

	; Insert the current cr3 (trampoline is identity mapped)
	mov eax, [cs:0xFFC]
	mov cr3, eax

	; Enable PAE and PGE
	mov eax, cr4
	or eax, 0xA0
	mov cr4, eax

	mov ecx, 0xC0000080
	rdmsr
	or eax, 1<<8 | 1<<11 ; Enable Longmode and NX
	wrmsr

	mov eax, cr0
	or eax, 0x80000001 ; PG and PM bits
	mov cr0, eax

	lgdt [cs:gdt.ptr-start] ; Install the GDT

	o32 jmp far dword [cs:start.entry-start]

.long:
bits 64
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov fs, ax
	mov gs, ax

	mov rsp, [edi+0xFF0]

	xor rbp, rbp
	push rbp ; RIP=0
	push rbp ; RBP=0
	mov rbp, rsp

	mov rax, [edi+0xFE8]
	jmp rax ; Jump into the kernel proper

align 4
.entry:
.entry_addr:
	dd 0 ; Address of the longmode entry point
.entry_cs:
	dw 8 ; CS

align 8
gdt:
    dq 0x0000000000000000 ; Null Descriptor - should be present.
    dq 0x00209A0000000000 ; 32-bit code descriptor (exec/read).
    dq 0x0000920000000000 ; 32-bit data descriptor (read/write).
    dd 0                  ; Padding to make the "address of the GDT" field aligned on a 4-byte boundary
.ptr:
    dw $ - gdt - 1 ; 16-bit Size (Limit) of GDT.
.addr:
    dd 0 ;To be overridden

global efi_trampoline_ap_asm_end:
efi_trampoline_ap_asm_end:

section .text
bits 64
global efi_trampoline_bsp_asm_start
efi_trampoline_bsp_asm_start:
	mov rsi, rdx ; The Handover Boot Table is the fourth parameter
	mov rdi, rcx ; Magic number is the third parameter
	mov r8,  rax ; Entry point is the first parameter
	mov cr3, rbx ; CR3 is the second parameter

	; Enable the NX bit
	mov rcx, 0xC0000080 ;EFER
	rdmsr
	; EDX is also thrashed
	or rax, 1<<11 ;Enable NX
	wrmsr

	;TODO: Load a preliminary GDT

	cli ; Ensure interrupts are off

	jmp r8
global efi_trampoline_bsp_asm_end
efi_trampoline_bsp_asm_end:
