#ifndef QUEVEDO_EFI_FS_H
#define QUEVEDO_EFI_FS_H

#include <efi.h>
#include <stdint.h>

/**
 * Read a file into a buffer
 *
 * @param[in]  handle Handle of the loaded image (used to determine the device)
 * @param[in]  fname  Name of the file to open
 * @param[out] fsize  Size of the file
 * @param[out] fstart Location of the file in memory. Automatically allocated
 */
void efi_fs_read(EFI_HANDLE handle, CHAR16 *fname, size_t *fsize, uintptr_t *fstart);

#endif /* QUEVEDO_EFI_FS_H */
