BIN<efi_loader>:=$(TARGETDIR)efi_loader/boot.efi
IMG<efi_loader>:=$(TARGETDIR)efi_loader/efi.img
DIR<efi_loader>:=$(DIR)

# Get all object files for compilation on amd64
SRCS<efi_loader>:=$(wildcard $(DIR)*.c)
SRCS<efi_loader>+=$(SRCS<libc>)
ASMS<efi_loader>:=$(wildcard $(DIR)*.asm)
SRCS:=$(SRCS<efi_loader>)
ASMS:=$(ASMS<efi_loader>)
$(eval $(call getobjs,$(BIN<efi_loader>)))

# UEFI uses PE files instead of ELFs
CC:=clang
CFLAGS:=$(CFLAGS<default>) \
         -ffreestanding \
         -target x86_64-pc-win32-coff \
         -fno-stack-protector \
         -mno-red-zone \
         -fshort-wchar \
         -mno-sse
CPPFLAGS:=\
    -I$(DIR)include \
    -I$(INCLUDE<libc>) \
    -I$(INCLUDE<kernel>) \
    -I$(DIR<kernel>) \
    -DIS_KERNEL \
    -DNORETURN=_Noreturn \
    -DSTATIC=static
AS:=$(AS<amd64>)
ASFLAGS:=$(ASFLAGS<efi_loader>)
LDFLAGS:=-Wl,-entry:efi_main \
         -Wl,-subsystem:efi_application \
         -fuse-ld=lld-link
LDLIBS:=-nostdlib
$(eval $(call spawn_c_rule,$(BIN<efi_loader>)))
$(eval $(call spawn_asm_rule,$(BIN<efi_loader>)))
$(eval $(call spawn_link_rule,$(BIN<efi_loader>)))

OVMF<efi_loader>:=$(DIR)ovmf/
CREATE_IMG<efi_loader>:=$(DIR)create_img.sh
