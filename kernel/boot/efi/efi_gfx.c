
#include <quevedo/boot.h>
#include <quevedo/panic.h>

#include <stddef.h>

#include <efi.h>
#include <efi-bs.h>
#include <protocol/efi-gop.h>

#include "efi_vmm.h"
#include "efi_gfx.h"

extern EFI_SYSTEM_TABLE *gST;

static const UINTN res_x = 1024;
static const UINTN res_y = 768;
static const UINTN pixel_fmt = PixelBlueGreenRedReserved8BitPerColor;
static EFI_GRAPHICS_OUTPUT_PROTOCOL *gProtoGop = NULL;

static void efi_gfx_put_triangle(EFI_PHYSICAL_ADDRESS lfb_base_addr, UINTN center_x, UINTN center_y, UINTN width, UINT32 color)
{
    UINT32* at = (UINT32*)lfb_base_addr;
    UINTN row, col;

    at += (res_x * (center_y - width / 2) + center_x - width / 2);

    for (row = 0; row < width / 2; row++) {
        for (col = 0; col < width - row * 2; col++)
            *at++ = color;
        at += (res_x - col);
        for (col = 0; col < width - row * 2; col++)
            *at++ = color;
        at += (res_x - col + 1);
    }
}

void efi_gfx_enable(struct hbt_gfx *gfx)
{
	/* Find the Graphics capable handles */
	UINTN handle_count = 0;
	EFI_HANDLE *handle_buffer;
	EFI_GUID guidGop = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;
	kassert(EFI_SUCCESS, ==, gST->BootServices->LocateHandleBuffer(ByProtocol,
	                                                               &guidGop,
	                                                               NULL,
	                                                               &handle_count,
	                                                               &handle_buffer));

	/* Make the GOP global, out of convenience for boot */
	kassert(EFI_SUCCESS, ==, gST->BootServices->HandleProtocol(handle_buffer[0],
	                                                           &guidGop,
	                                                           (VOID**)&gProtoGop));

	/* Find a video mode from the GOP that fits our needs */
	EFI_GRAPHICS_OUTPUT_MODE_INFORMATION* gopModeInfo;
	UINTN ModeInfoSize;
	UINTN mode_num = 0;
	do {
		kassert(EFI_SUCCESS, ==, gProtoGop->QueryMode(gProtoGop, mode_num, &ModeInfoSize, &gopModeInfo));
		if (gopModeInfo->HorizontalResolution == res_x &&
		    gopModeInfo->VerticalResolution == res_y &&
		    gopModeInfo->PixelFormat        == pixel_fmt)
			break;
		mode_num++;
	} while(1);

	kassert(EFI_SUCCESS, ==, gProtoGop->SetMode(gProtoGop, mode_num));

	/* Paint a triangle. For shits */
	efi_gfx_put_triangle(gProtoGop->Mode->FrameBufferBase, res_x / 2, res_y / 2 - 25, 100, 0x00ff99ff );

	/* TODO: Explain this. Essentially we uuse UEFI's manager to ensure the framebuffer won't collide
	 * with the trampoline
	 */
	void *vaddr = (void*)((256ull<<39)-VMM_PAGESIZE);
	size_t npages = (res_x*res_y*4+VMM_PAGESIZE-1)/VMM_PAGESIZE;
	kassert(EFI_SUCCESS, ==, gST->BootServices->AllocatePages(AllocateMaxAddress,
	                                                         EfiLoaderData,
	                                                         npages, (void*)&vaddr));

	gfx->resx = res_x;
	gfx->resy = res_y;
	gfx->fmt  = pixel_fmt;
	gfx->phy_buf  = gProtoGop->Mode->FrameBufferBase;
	gfx->virt_buf = (uint64_t)vaddr;

	efi_vmm_map(gfx->virt_buf, gfx->phy_buf, npages*VMM_PAGESIZE, EFI_PTYPE_MMIO);
}
