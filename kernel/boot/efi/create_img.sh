#!/usr/bin/env sh

set -u

dd if=/dev/zero of=$EFI_IMG.tmp bs=512 count=91669
mformat -i $EFI_IMG.tmp -h 32 -t 32 -n 64 -c 1
mcopy -i $EFI_IMG.tmp $EFI_APP ::
mmd -i $EFI_IMG.tmp ::/EFI
mmd -i $EFI_IMG.tmp ::/EFI/quevedo
mcopy -i $EFI_IMG.tmp $KERNEL_BIN ::/EFI/quevedo
mcopy -i $EFI_IMG.tmp $INITRD_BIN ::/EFI/quevedo

dd if=/dev/zero of=$EFI_IMG bs=512 count=93750
parted $EFI_IMG -s -a minimal mklabel gpt
parted $EFI_IMG -s -a minimal mkpart EFI FAT16 2048s 93716s
parted $EFI_IMG -s -a minimal toggle 1 boot

dd if=$EFI_IMG.tmp of=$EFI_IMG bs=512 bs=512 count=91669 seek=2048 conv=notrunc
