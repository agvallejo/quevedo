#include <efi.h>

#include "efi_pmm_private.h"

#include <quevedo/boot.h>
#include <quevedo/panic.h>

#include <string.h>
#include <stdio.h>

#define printf(...) /* Ignore prints unless debugging. Using them changes the memory map! */

static struct efi_pmm_data pmm_data;
extern EFI_SYSTEM_TABLE *gST;

static void efi_pmm_clean(struct hbt_pmm *pmm)
{
	*pmm = (struct hbt_pmm){0};
}

static void efi_pmm_add(struct hbt_pmm *pmm, uintptr_t start, size_t octets)
{
	kassert(pmm->map_size, <, sizeof pmm->map/sizeof *pmm->map);
	if (pmm->map_size)
	{
		kassert(pmm->map[pmm->map_size].start +
		        pmm->map[pmm->map_size].octets, <=,  start);
	}

	pmm->map[pmm->map_size].start  = start;
	pmm->map[pmm->map_size].octets = octets;
	pmm->map_size++;
}

void efi_pmm_reserve(uintptr_t paddr, size_t octets)
{
	kassert(pmm_data.rsvd_pages_size, <, (sizeof  pmm_data.rsvd_pages /
	                                      sizeof *pmm_data.rsvd_pages));
	printf("paddr=0x%lX\n", paddr);
	if (pmm_data.rsvd_pages_size)
	{
		for (size_t i=0; i<pmm_data.rsvd_pages_size; i++)
		{
			/* Don't reserve the same page several times */
			printf("    rsvd=0x%lX ", pmm_data.rsvd_pages[i]);
			printf("    paddr_start=0x%lX paddr_end=0x%lX\n", paddr, paddr+octets);
			kassert(pmm_data.rsvd_pages[i].start, !=, paddr);
		}
	}

	pmm_data.rsvd_pages[pmm_data.rsvd_pages_size].start = paddr;
	pmm_data.rsvd_pages[pmm_data.rsvd_pages_size].octets = octets;
	pmm_data.rsvd_pages_size++;
}

UINTN efi_pmm_init(struct hbt_pmm *pmm)
{
	efi_pmm_clean(pmm);

	/*
	 * In an act of brilliance on the UEFI spec you need to
	 * allocate memory for the memory map. But we don't know
	 * about it yet so we don't know how big it is, so we
	 * also don't know how much to allocate. As a side effect
	 * of failing to get the memory map you get the actual
	 * size of the memory map, so that should be enough, right?
	 *
	 * WRONG!
	 *
	 * Any call to boot services can potentially change the
	 * memory map, so even if you fail (and as a side effect
	 * you do get the right size) the new allocation might
	 * fuck up the memory map again.
	 *
	 * But wait! It gets better! The very descriptor size is
	 * fixed at present on 40 octets, but a descriptor size is
	 * given so they can increase its size on future specs if
	 * needed Not a bad move, really. But the side effects of
	 * that are painful at best.
	 * 
	 * Did you get it? You can allocate a buffer, fail at getting
	 * the memory map, realise you were N bytes off, then you free
	 * the old buffer to get a bigger one, but the memory map grew
	 * as well as a side effect of the allocation, and because the
	 * descriptor size is not fixed size we can't allocate just one
	 * more just in case.
	 *
	 * Oh, well. Stick a loop in here. Add 4 descriptors margin and
	 * pray for the best. This is stupidity at its finest...
	 */
	UINTN map_size = sizeof(EFI_MEMORY_DESCRIPTOR);
	EFI_MEMORY_DESCRIPTOR *desc;
	UINTN map_key;
	UINTN desc_size;
	UINT32 desc_version;
	EFI_STATUS status;
	do {
		status = gST->BootServices->AllocatePool(EfiLoaderData, map_size,
		                                         (VOID**)&desc);
		kassert(status, ==, EFI_SUCCESS);

		status = gST->BootServices->GetMemoryMap(&map_size, desc,
		                                         &map_key, &desc_size,
		                                         &desc_version);
		if (status == EFI_SUCCESS)
			break;

		map_size += sizeof *desc;
		status = gST->BootServices->FreePool(desc);
		kassert(status, ==, EFI_SUCCESS);
	} while(1);
 
	/* Debug. Calls to printf may fuck up the memory map */
	for (size_t i=0; i<map_size; i+=desc_size)
	{
		EFI_MEMORY_DESCRIPTOR *d = (void*)(i+(uintptr_t)desc);
		switch(d->Type)
		{
		case EfiLoaderCode:
		case EfiLoaderData:
		case EfiBootServicesCode:
		case EfiBootServicesData:
		case EfiConventionalMemory:
			break;
		default:
			continue;
		}

		printf("type=%d, ", d->Type);
		printf("phy=0x%llX, ", d->PhysicalStart);
		printf("virt=0x%llX, ", d->VirtualStart);
		printf("npages=%lld, ", d->NumberOfPages);
		printf("att=%llX\n", d->Attribute);

		/* Only these types are reclaimable */
		efi_pmm_add(pmm, d->PhysicalStart, 4096*d->NumberOfPages);
	}
	printf("map_key=%d, ", map_key);
	printf("desc_ver=%d, ", desc_version);

	for (size_t i=0; i<pmm_data.rsvd_pages_size; i++)
	{
		pmm->rsvd_pages[i].start = pmm_data.rsvd_pages[i].start;
		pmm->rsvd_pages[i].octets = pmm_data.rsvd_pages[i].octets;
		pmm->rsvd_pages_size = pmm_data.rsvd_pages_size;
	}

	return map_key;
}
