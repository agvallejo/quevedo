#include <quevedo/hal.h>

#include <stdint.h>
#include <stdio.h>

NORETURN void panic_full(char *filename, char *line, char *sarg1, char *op, char *sarg2, uint64_t arg1, uint64_t arg2)
{
	cli();
	printf("PANIC! %s-line=%s-(%s%s%s)(0x%lX, 0x%lX)\n", filename, line, sarg1, op, sarg2, arg1, arg2);
	halt_forever();
}

NORETURN void panic_simple(char *filename, char *line, char *cond, uint64_t arg)
{
	cli();
	printf("PANIC! %s-line=%s-%s-arg=0x%lX\n", filename, line, cond, arg);
	halt_forever();
}
