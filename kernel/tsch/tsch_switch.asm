global tsch_switch_asm
tsch_switch_asm:
	; Save the context that needs to
	; be preserved in the old stack
	pushfq
	push r15
	push r14
	push r13
	push r12
	push rbp
	push rbx

	; Save the old stack
	mov [rdi], rsp

	; Perform the context switch if we
	; aren't in the right address space
	mov rax, cr3
	cmp rax, rdx
	je .skip_cr3_reload
	mov cr3, rdx
.skip_cr3_reload:

	; Load the new stack
	mov rsp, rsi

	; Restore the new context
	pop rbx
	pop rbp
	pop r12
	pop r13
	pop r14
	pop r15
	popfq

	; Back to business
	ret
