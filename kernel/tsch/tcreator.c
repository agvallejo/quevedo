#include <mp/mp.h>
#include <mm/hmm.h>
#include <mm/mm_init.h>
#include <fs/fs.h>

#include "tsch_private.h"
#include "tcreator_private.h"

#include <stdio.h>

static struct tcreator_data tcreator_data;

static void tcreator_push(struct thread_t *t, uintptr_t val)
{
	t->regs.rsp -= sizeof t->regs.rsp;
	*(uintptr_t*)(t->regs.rsp) = val;
}

static void tcreator_load_elf(void)
{
	mm_local_init();
	fs_exec(mp_percore(tsch)->current_thread->file);
	kassert_unreachable();
}

struct thread_t *tcreator_alloc(char *filename)
{
	struct thread_t *t = hmm_alloc(VMM_REGION_PRIV_GLOBAL, sizeof *t);
	t->file = fs_find(filename);
	t->tid = atomic_fetch_add(&tcreator_data.next_tid, 1);
	kassert(t->tid, !=, -1);

	/* Setup the stack with the initial values to load an ELF file */
	t->stack_top = vmm_alloc(VMM_REGION_PRIV_GLOBAL, TSCH_STACK_SIZE);
	t->regs.rsp = t->stack_top+TSCH_STACK_SIZE;

	tcreator_push(t, (uintptr_t)tcreator_load_elf); /* Entry point */
	tcreator_push(t, hal_getflags()); /* Irrelevant */
	for (size_t i=0; i<6; i++)
		tcreator_push(t, 0x76543210); /* The preserved registers */

	t->regs.cr3 = vmm_create_address_space();

	return t;
}

void tcreator_free(struct thread_t *t)
{
	vmm_destroy_address_space(t->regs.cr3);
	vmm_free(t->stack_top, TSCH_STACK_SIZE);

	/* Free the actual thread LAST */
	hmm_free(t, sizeof *t);
}
