#ifndef QUEVEDO_TSCH_PRIVATE_H
#define QUEVEDO_TSCH_PRIVATE_H

#include <mp/mp.h>

#include "tsch_percore.h"

static inline struct thread_t *tsch_get_current(void)
{
	return mp_percore(tsch)->current_thread;
}

static inline void tsch_set_current(struct thread_t *t)
{
	mp_percore(tsch)->current_thread = t;
}

static inline struct thread_t *tsch_get_previous(void)
{
	return mp_percore(tsch)->previous_thread;
}

static inline void tsch_set_previous(struct thread_t *t)
{
	mp_percore(tsch)->previous_thread = t;
}

static inline void tsch_set_callback(tsch_cb cb)
{
	mp_percore(tsch)->afterswitch_callback = cb;
}

static inline void tsch_exec_callback(void)
{
	tsch_cb cb = (tsch_cb)(uintptr_t)mp_percore(tsch)->afterswitch_callback;
	cb(tsch_get_previous());
}

extern void tsch_switch_asm(uintptr_t *cur_rsp, uintptr_t next_rsp, uintptr_t next_cr3);
static inline void tsch_switch(struct thread_t *cur, struct thread_t *next)
{
	tsch_switch_asm(&cur->regs.rsp, next->regs.rsp, next->regs.cr3);
}

extern NORETURN void tsch_jump_asm(uintptr_t cr3, void (*entry)(void));

#endif /* QUEVEDO_TSCH_PRIVATE_H */
