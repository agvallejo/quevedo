#ifndef QUEVEDO_TIPC_PRIVATE_H
#define QUEVEDO_TIPC_PRIVATE_H

#include "tipc.h"

#include <quevedo/cfg.h>

#include <stddef.h>
#include <stdint.h>

struct tipc_msg
{
	size_t len;
	uint8_t buf[TIPC_MSG_SIZE];
};

struct tsch_q;
struct tipc_mbox
{
	size_t wrpos;
	size_t rdpos;
	struct tipc_msg msg[TIPC_MBOX_SIZE];
};

#endif /* QUEVEDO_TIPC_PRIVATE_H */
