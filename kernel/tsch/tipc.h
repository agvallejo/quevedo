#ifndef QUEVEDO_TIPC_H
#define QUEVEDO_TIPC_H

#include <stddef.h>
#include <stdbool.h>

struct tipc_mbox;
void tipc_init(struct tipc_mbox *mbox);

struct tipc_mbox;
bool tipc_send(struct tipc_mbox *mbox, void *buf, size_t len);

struct tipc_mbox;
bool tipc_recv(struct tipc_mbox *mbox, void *buf);

#endif /* QUEVEDO_TIPC_H */
