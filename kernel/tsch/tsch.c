#include "tsch_private.h"

#include <mp/mp.h>
#include <mm/hmm.h>

#include "tsch.h"
#include "tcreator.h"

#include <stdio.h>

extern uint8_t kmain_stack[TSCH_STACK_SIZE];

static struct thread_t *tsch_get_next(void)
{
	struct thread_t *cur = tsch_get_current();
	struct thread_t *next = NULL;
	struct tsch_q *q = cur->running_q;
	while(q->priority<TSCH_PRIORITY_RUNNABLE_NUM_TOTAL && !next)
	{
		mp_lock(&q->mutex);
		if (!STAILQ_EMPTY(&q->head))
		{
			next = STAILQ_FIRST(&q->head);
			STAILQ_REMOVE_HEAD(&q->head, link);
		}
		mp_unlock(&q->mutex);
		q++;
	}
	if (next)
		next->current_q = NULL;
	return next;
}

static void tsch_sendto_ready(struct thread_t *t)
{
	struct tsch_q *q = t->running_q;
	t->current_q = q;

	mp_lock(&q->mutex);
	STAILQ_INSERT_TAIL(&q->head, t, link);
	q->num_threads++;
	mp_unlock(&q->mutex);
}

static void tsch_sendto_sleep(struct thread_t *t)
{
	struct tsch_q *q = &mp_percore(tsch)->q[TSCH_PRIORITY_SLEEPING];
	t->current_q = q;

	mp_lock(&q->mutex);
	if (STAILQ_EMPTY(&q->head))
	{
		STAILQ_INSERT_HEAD(&q->head, t, link);
	}
	else
	{
		struct thread_t *entry;
		STAILQ_FOREACH(entry, &q->head, link)
		{
			struct thread_t *next = STAILQ_NEXT(entry, link);
			if (!next || time_op(next->sleep.target, >=, t->sleep.target))
			{
				STAILQ_INSERT_AFTER(&q->head, entry, t, link);
				break;
			}
		}
	}
	q->num_threads++;
	mp_unlock(&q->mutex);
}

static void tsch_schedule(struct thread_t *next, tsch_cb cb)
{
	struct thread_t *cur = tsch_get_current();

	tsch_set_current(next);
	tsch_set_previous(cur);
	tsch_set_callback(cb);
	tsch_switch(cur, next);
	tsch_exec_callback();
}

static NORETURN void tsch_idle(void)
{
	printf("[cpu%d] Idle task started\n", mp_percore(id));

	while(1) {
		sti();
		tsch_yield();
		halt();
	}

	kassert_unreachable();
}

NORETURN void tsch_init(void)
{
	struct tsch_q (*q)[TSCH_PRIORITY_NUM_TOTAL] = &mp_percore(tsch)->q;
	for (enum tsch_priority priority = 0;
	     priority < TSCH_PRIORITY_NUM_TOTAL;
	     priority++)
	{
		STAILQ_INIT(&(*q)[priority].head);
		(*q)[priority].priority = priority;
		(*q)[priority].num_threads = 0;
		mp_unlock(&(*q)[priority].mutex);
	}

	struct thread_t *t = hmm_alloc(VMM_REGION_PRIV_GLOBAL, sizeof *t);
	/* The stack is deliberately not configured because BSP and APs use
	 * different schemes and it doesn't really matter. The idle thread
	 * will live forever and its stack will never be freed.
	 * After Quevedo becomes a proper OS it won't even cost memory because
	 * it will be swapped out or not even allocated besides the initial
	 * 4KiB.
	 */
	t->regs.cr3 = hal_getcr3();
	t->running_q = &mp_percore(tsch)->q[TSCH_PRIORITY_IDLE];
	t->file = NULL;
	mp_percore(tsch)->current_thread = t;

	tsch_exec("init");

	tsch_idle();
}

void tsch_yield(void)
{
	struct thread_t *next = tsch_get_next();
	if (!next)
		return;

	tsch_schedule(next, tsch_sendto_ready);
}

void tsch_sleep_until(time_a target)
{
	struct thread_t *next = tsch_get_next();

	tsch_get_current()->sleep.target = target;
	tsch_schedule(next, tsch_sendto_sleep);
}

void tsch_exec(char *filename)
{
	struct thread_t *t = tcreator_alloc(filename);
	t->running_q = &mp_percore(tsch)->q[TSCH_PRIORITY_NORMAL];
	tsch_sendto_ready(t);
}

tid_t tsch_gettid(void)
{
	return tsch_get_current()->tid;
}

void tsch_block(struct tsch_q *q)
{
	(void)q;
	/* FIXME: Uninimplemented */
	kassert_unreachable();
}

void tsch_unblock(struct tsch_q *q)
{
	(void)q;
	/* FIXME: Uninimplemented */
	kassert_unreachable();
}
