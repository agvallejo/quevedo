#include "tipc_private.h"
#include "tsch.h"

#include <quevedo/panic.h>
#include <mp/mp.h>

#include <stdbool.h>
#include <string.h>

void tipc_init(struct tipc_mbox *mbox)
{
	memset(mbox, 0x66, sizeof *mbox);
	mbox->rdpos = 0;
	mbox->wrpos = 0;
}

bool tipc_send(struct tipc_mbox *mbox, void *buf, size_t len)
{
	kassert(len, <=, TIPC_MSG_SIZE);

	size_t rdpos = atomic_load(&mbox->rdpos);
	size_t free = TIPC_MBOX_SIZE - (mbox->wrpos-rdpos);

	if (!free)
		return false;

	/* Pointers are valid */
	kassert(free, <=, TIPC_MBOX_SIZE);

	struct tipc_msg *msg = &mbox->msg[mbox->wrpos];
	memcpy(msg->buf, buf, len);
	msg->len = len;

	/* Make absolutely sure the compiler doesn't reorder this one */
	atomic_fetch_add(&mbox->wrpos, 1);

	return true;
}

bool tipc_recv(struct tipc_mbox *mbox, void *buf)
{
	size_t wrpos = atomic_load(&mbox->wrpos);
	size_t free = TIPC_MBOX_SIZE-(wrpos-mbox->rdpos);

	if (free == TIPC_MBOX_SIZE)
		return false;

	/* Pointers are valid */
	kassert(free, !=, 0);
	kassert(free, <=, TIPC_MBOX_SIZE);

	struct tipc_msg *msg;
	msg = &mbox->msg[mbox->rdpos+1];
	memcpy(buf, msg->buf, msg->len);

	/* Make absolutely sure the compiler doesn't reorder this one */
	atomic_fetch_add(&mbox->rdpos, 1);

	return true;
}
