#ifndef QUEVEDO_TCREATOR_H
#define QUEVEDO_TCREATOR_H

#include <stdint.h>

struct thread_t;
struct thread_t *tcreator_alloc(char *filename);

void tcreator_free(struct thread_t *t);

#endif /* QUEVEDO_TCREATOR_H */
