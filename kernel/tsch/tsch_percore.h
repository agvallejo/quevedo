#ifndef QUEVEDO_TSCH_PERCORE_H
#define QUEVEDO_TSCH_PERCORE_H

#include <sys/queue.h>

#include <time/time.h>
#include <tsch/tcreator.h>
#include <tsch/tsch.h>

#include <stdatomic.h>
#include <stddef.h>
#include <stdint.h>

enum tsch_priority {
	TSCH_PRIORITY_IDLE,
	TSCH_PRIORITY_BACKGROUND,
	TSCH_PRIORITY_NORMAL,
	TSCH_PRIORITY_INTERACTIVE,
	TSCH_PRIORITY_RUNNABLE_NUM_TOTAL,

	TSCH_PRIORITY_SLEEPING = TSCH_PRIORITY_RUNNABLE_NUM_TOTAL,
	TSCH_PRIORITY_NUM_TOTAL
};

struct fs_file;
struct thread_t {
	STAILQ_ENTRY(thread_t) link;
	tid_t tid;
	struct tsch_q *running_q; /**< The running queue this thread belongs to */
	struct tsch_q *current_q; /**< The queue this thread currently lives in */
	uintptr_t stack_top;
	struct fs_file *file;
	struct {
		time_a target;
	} sleep;
	struct {
		uintptr_t cr3;
		uintptr_t rsp;
	} regs;
};

STAILQ_HEAD(tsch_head, thread_t);

struct tsch_q {
	enum tsch_priority priority; /**< Priority of this queue */
	atomic_flag mutex; /**< A mutex (PER QUEUE) for concurrent support */
	struct tsch_head head; /**< List with the threads */
	size_t num_threads; /**< Number of threads in the queue */
};

typedef void (*tsch_cb)(struct thread_t *t);

struct tsch_data_percore {
	struct tsch_q q[TSCH_PRIORITY_NUM_TOTAL]; /**< Thread queues. Running and blocked */
	struct thread_t *current_thread; /**< Currently running thread */
	struct thread_t *previous_thread; /**< Thread that was running before the current
	                                   *   one. Only valid immediately after a switch
	                                   */
	tsch_cb afterswitch_callback; /**< To be used on the previous thread after a context switch */
};

#endif /* QUEVEDO_TSCH_PERCORE_H */
