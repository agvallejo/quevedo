#ifndef QUEVEDO_TSCH_H 
#define QUEVEDO_TSCH_H 

#include <time/time.h>
#include <stdint.h>

/** Initialise the scheduler and start up the idle thread */
NORETURN void tsch_init(void);

/** Blast the schedulers of the APs */
NORETURN void tsch_ap_init(void);

/** Signal the scheduler that it may schedule another thread */
void tsch_yield(void);

/**
 * Block the current thread until 'target'
 *
 * @param target Wake time
 */
void tsch_sleep_until(time_a target);

/**
 * Create a new thread using the 'elf' image
 *
 * @param elf Image for the new thread
 */
void tsch_exec(char *filename);

typedef uintptr_t tid_t;

/**
 * Get the running tid 
 *
 * @return Thread-Id of the current thread
 */
tid_t tsch_gettid(void);

NORETURN void tsch_init(void);
NORETURN void tsch_ap_init(void);

struct tsch_q;
void tsch_block(struct tsch_q *q);

struct tsch_q;
void tsch_unblock(struct tsch_q *q);

#endif /* QUEVEDO_TSCH_H */
