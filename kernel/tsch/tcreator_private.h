#ifndef QUEVEDO_TCREATOR_PRIVATE_H
#define QUEVEDO_TCREATOR_PRIVATE_H

#include "tcreator.h"

struct tcreator_data
{
	tid_t next_tid;
};

#endif /* QUEVEDO_TCREATOR_PRIVATE_H */
