isr_syscall:
	swapgs
	extern isr_syscall
	call isr_syscall
	swapgs
	iretq

isr_wrapper:
	; Push cr2
	push rdi
	mov rdi, cr2
	xor rdi, [rsp]
	xor [rsp], rdi
	xor rdi, [rsp]

	push rdi
	lea rdi, [rsp+8] ; Parameters for the ISR

	push rsi
	push rax
	push rcx
	push rdx
	push r8
	push r9
	push r10
	push r11

	cld
	extern isr_handler
	call isr_handler

	pop r11
	pop r10
	pop r9
	pop r8
	pop rdx
	pop rcx
	pop rax
	pop rsi
	pop rdi

	add rsp, 16

	iretq

NUM_ISR_VECTORS equ 256
%assign i 0
%rep    NUM_ISR_VECTORS
global isr_num %+ i
isr_num %+ i:
	push qword i
	jmp isr_wrapper
%assign i i+1
%endrep

global isr_register_all:
isr_register_all:
%assign i 0
%rep    NUM_ISR_VECTORS
	mov rdi, i
	lea rsi, [isr_num %+ i]
	mov rdx, 0x8E
	extern isr_register
	call isr_register
%assign i i+1
%endrep
	ret
