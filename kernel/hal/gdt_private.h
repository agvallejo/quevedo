#ifndef QUEVEDO_GDT_PRIVATE_H
#define QUEVEDO_GDT_PRIVATE_H

#include <stdint.h>
#include <stdatomic.h>

#include "gdt.h"

enum {
	HAL_GDT_IDX_NULL,
	HAL_GDT_IDX_KERNEL_CODE,
	HAL_GDT_IDX_KERNEL_DATA,
	HAL_GDT_IDX_USER_CODE,
	HAL_GDT_IDX_USER_DATA,
	HAL_GDT_IDX_TSS_LO,
	HAL_GDT_IDX_TSS_HI,
};

struct gdt_ptr {
	uint16_t limit;
	uint64_t base;
} __attribute__((packed));

struct gdt_data {
	#define TSS_NUM_ENTRIES 26
	uint32_t tss[TSS_NUM_ENTRIES];
	size_t tss_pos;
	/* Global Descriptor Table
	 *
	 * -Null descriptor
	 * -Kernel code
	 * -Kernel data
	 * -User   code
	 * -User   data
	 * -TSS (lo) [per-core]
	 * -TSS (hi) [per-core]
	 */
	#define GDT_NUM_ENTRIES 32
	uint64_t gdt[GDT_NUM_ENTRIES];
	struct gdt_ptr gdt_ptr;
};

/**
 * Load the GDTR with a GDT pointer
 *
 * Written in assembly. Also reloads the data
 * segments with the null descriptor
 *
 * @param gdt_ptr Pointer to load
 */
extern void lgdt(void* gdt_ptr, size_t selector);

#endif /* QUEVEDO_GDT_PRIVATE_H */
