#ifndef QUEVEDO_GDT_H
#define QUEVEDO_GDT_H

#include <stdint.h>

/** Populate and register a GDT */
void gdt_init(void);

/** Populate and register a GDT */
void gdt_ap_init(void);

/**
 * Update the RSP in the TSS
 *
 * Allows the kernel stack to be chosen
 * when returning to kernel mode from userspace
 *
 * @param rsp Stack pointer of the kernel stack
 */
void gdt_tss_update_rsp(uintptr_t rsp);

#endif /* QUEVEDO_GDT_HA */
