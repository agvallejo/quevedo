#ifndef QUEVEDO_APIC_PRIVATE_H
#define QUEVEDO_APIC_PRIVATE_H

#include <stdint.h>
#include "apic.h"

struct apic_data {
	uintptr_t lapic_base;
	struct {
		uint32_t glb_sys_int_base; /**< Global System Interrupt Base */
		uint32_t bitmap;           /**< Bitmap of the IOAPIC entries. 1=in-use, 0=available */
	} ioapic;
};

/** Offsets into the Local APIC space */
#define APIC_LAPIC_REG_LOCAL_ID  (0x20U)
#define APIC_LAPIC_REG_EOI       (0xB0U)
#define APIC_LAPIC_REG_SPURIOUS  (0xF0U)
#define APIC_LAPIC_REG_CMD1      (0x300U)
#define APIC_LAPIC_REG_CMD2      (0x310U)
#define APIC_LAPIC_REG_LVT_TIMER (0x320U)
#define APIC_LAPIC_REG_TIMER_TIC (0x380U)
#define APIC_LAPIC_REG_TIMER_TCC (0x390U)
#define APIC_LAPIC_REG_TIMER_DIV (0x3E0U)

/** Number of available interrupt vector */
#define APIC_IOAPIC_NUM_VECTORS (256U)

/** Number of available interrupt vector */
#define APIC_IOAPIC_VECTOR_DISABLED (1U<<16)

/** Offsets into the IOAPIC space */
#define APIC_IOAPIC_IOREGSEL (0x00U)
#define APIC_IOAPIC_IOWIN    (0x10U)

/** Values for IOREGSEL */
#define APIC_IOAPIC_IOREDTBL(x) (0x10U+2*(x))

/** Bitmask for masking an ISR */
#define APIC_IOAPIC_VECTOR_MASKED (1<<16)

/** Number of external interrupts */
#define APIC_IOAPIC_NUM_EXT_INTS (24U)

#endif /* QUEVEDO_APIC_PRIVATE_H */
