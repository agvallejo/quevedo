#ifndef QUEVEDO_ISR_H
#define QUEVEDO_ISR_H

#include <stdint.h>
#include <stddef.h>
#include <quevedo/hal.h>

/** Argument to isr_handler */
struct isr_arg{
	uint64_t cr2;
	uint64_t isr_num;
	uint32_t error_code;
	uint32_t padding;
	uint64_t tail[];
};

/**
 * Definition of the ISRs dispatched from the main handler
 *
 * @param arg Stack frame pointer
 */
typedef void isr_t(struct isr_arg* arg);

/**
 * Install/uninstall an ISR
 *
 * @param isr_num ISR number
 * @param isr     ISR to install or NULL for uninstalling
 */
void isr_install(uint8_t isr_num, isr_t *isr);

/**
 * "REGISTER" an ISR on the IDT
 *
 * Not to be confused with "INSTALL" an ISR. All of them are registered on the
 * IDT as address isr_numX or irq_numX. Eventually they call isr_handler with a
 * parameter showing the specific ISR raised along with more data. If an ISR is
 * installed isr_handler will forward control to the installed ISR. If an item
 * is NULL, however, the system will panic gracefully (not triggering a triple
 * panic like an imbecile).
 *
 * @param isr_num ISR number
 * @param isr_addr Address of the assembly ISR wrapper
 * @param isr_type Type of the ISR (DPL, gate type, etc)
 */
void isr_register(size_t isr_num, uintptr_t isr_addr, uint8_t isr_type);

/**
 * Register an End-Of-Interrupt handler
 *
 * To be called by whichever interrupt controller is to be used
 *
 * @param eoi handler for sending the EOI
 */
void isr_register_eoi(void (*eoi)(void));

/** Send End-Of-Interrupt to the interrupt controller */
void isr_send_eoi(void);

/**
 * Register an Unmaking handler
 *
 * @param unmask Handler to unmask the vector 'vec_num'
 */
void isr_register_unmask(void (*unmask)(size_t vec_num));

/**
 * Unmask an interrupt vector
 *
 * @param vec_num Vector number to unmask
 */
void isr_unmask(size_t vec_num);

/** ISR masking options */
enum isr_mask {
	ISR_IRQ_MASKED   = 0,
	ISR_IRQ_UNMASKED = 1
};

/** IRQ definitions */
enum isr_irq {
	ISR_IRQ_PIT      = 0,
	ISR_IRQ_KEYBOARD = 1,
	ISR_IRQ_CASCADED = 2,
	ISR_IRQ_COM2     = 3,
	ISR_IRQ_COM1     = 4,
	ISR_IRQ_LPT2     = 5,
	ISR_IRQ_FLOPPY   = 6,
	ISR_IRQ_SPURIOUS = 7,
	ISR_IRQ_CMOS     = 8,
	//  9 - 11 peripherals
	ISR_IRQ_PS2_MICE = 12,
	ISR_IRQ_FPU      = 13,
	ISR_IRQ_PRI_ATA  = 14,
	ISR_IRQ_SEC_ATA  = 15,
};

/**
 * Control the masking status of 'irq'
 *
 * @param irq Interrupt to mask
 * @param isr_mask New masking status
 */
void isr_mask_irq(enum isr_irq irq, enum isr_mask irq_mask);

/** Initialise the interrupts module */
struct hbt;
void isr_init(struct hbt *hbt);
void isr_ap_init(void);

/** Global state of the interrupt enable/disables */
extern size_t interrupts_disable_count;

static inline void isr_enable(void)
{
	if(!--interrupts_disable_count)
		sti();
}

static inline void isr_disable(void)
{
	cli();
	interrupts_disable_count++;
}

#endif // QUEVEDO_ISR_H
