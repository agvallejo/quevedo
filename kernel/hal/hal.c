#include "hal.h"
#include "gdt.h"
#include "isr.h"

void hal_init(struct hbt *hbt)
{
	uint32_t eax, ecx, edx;
	cpuid(1, &eax, &ecx, &edx);
	kassert(edx, &, 1<<4);  /* TSC support */
	kassert(ecx, &, 1<<24); /* TSC deadline support */
	cpuid(0x80000000, &eax, &ecx, &edx);
	kassert(eax, >, 0x80000007); /* Maximum allowed cpuid code */
	cpuid(0x80000007, &eax, &ecx, &edx);
	kassert(edx, &, 1<<8); /* Invariant TSC support */

	gdt_init();
	isr_init(hbt);
}

void hal_ap_init(void)
{
	gdt_ap_init();
	isr_ap_init();
}
