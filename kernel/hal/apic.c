#include <quevedo/hal.h>
#include <quevedo/boot.h>
#include <mm/vmm.h>
#include <time/time.h>

#include <stdint.h>
#include <stddef.h>

#include "apic_private.h"
#include "isr.h"

STATIC struct apic_data apic_data;

STATIC void apic_lapic_setreg(size_t offset, uint32_t val)
{
	volatile uint32_t *reg = (void*)(apic_data.lapic_base+offset);
	*reg = val;
}

STATIC uint32_t apic_lapic_getreg(size_t offset)
{
	volatile uint32_t *reg = (void*)(apic_data.lapic_base+offset);
	return *reg;
}

STATIC void apic_send_eoi(void)
{
	apic_lapic_setreg(APIC_LAPIC_REG_EOI, 0);
}

static void apic_ipi_init(uint8_t apic_id)
{
	uint32_t reg = apic_id<<24;
	apic_lapic_setreg(APIC_LAPIC_REG_CMD2, reg);

	reg = 0;
	reg |= 5<<8;  // INIT IPI
	reg |= 1<<14; // Non Level-Deassert
	apic_lapic_setreg(APIC_LAPIC_REG_CMD1, reg);
}

static void apic_ipi_sipi(uint8_t apic_id, uintptr_t trampoline)
{
	kassert(trampoline, !=, 0);

	uint32_t reg = apic_id<<24;
	apic_lapic_setreg(APIC_LAPIC_REG_CMD2, reg);

	reg = 0xFF & (trampoline>>12); // Start at #page
	reg |= 6<<8;  // INIT SIPI
	reg |= 1<<14; // Non Level-Deassert
	apic_lapic_setreg(APIC_LAPIC_REG_CMD1, reg);
}

void apic_wake(uint8_t apic_id, uintptr_t trampoline)
{
	apic_ipi_init(apic_id);

	time_a target = time_add_mseg(time_gettime(), 200);
	while(time_op(target, <, time_gettime()));

	apic_ipi_sipi(apic_id, trampoline);

	target = time_add_mseg(time_gettime(), 200);
	while(time_op(target, <, time_gettime()));
}

uint8_t apic_getid(void)
{
	return apic_lapic_getreg(APIC_LAPIC_REG_LOCAL_ID) >> 24;
}

void apic_init(struct hbt *hbt)
{
	kassert_bool(vmm_map(VMM_REGION_PRIV_GLOBAL, hbt->apic.lapic_base,
	                     VMM_PAGESIZE, &apic_data.lapic_base),
	                     apic_data.lapic_base);
	vmm_cfg(apic_data.lapic_base, VMM_PAGESIZE, VMM_CFG_RW_CD);

	isr_register_eoi(apic_send_eoi);

	apic_ap_init();
}

void apic_ap_init(void)
{
	/* Enabled and mapped to INT #255 */
	apic_lapic_setreg(APIC_LAPIC_REG_SPURIOUS, 0x1FF);
}
