#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include <quevedo/hal.h>
#include <quevedo/panic.h>

#include "isr_private.h"
#include "apic.h"

/** Global showing the nesting on disable/enable_interrupts */
size_t interrupts_disable_count;

STATIC struct isr_data isr_private_data __attribute__((aligned (8)));

static void isr_handle_page_fault(struct isr_arg *arg)
{
	printf("PAGE FAULT: error_code=0x%X. Paging dump\n", arg->error_code);
	panic("Page fault", arg->cr2);
}

static void isr_handle_gen_prot_fault(struct isr_arg *arg)
{
	panic("General protection fault", arg->error_code);
}

void isr_register(size_t isr_num, uintptr_t isr_addr, uint8_t isr_type)
{
	isr_setidt_field(isr_num, addr1, (uint16_t)isr_addr);
	isr_setidt_field(isr_num, addr2, (uint16_t)(isr_addr>>16));
	isr_setidt_field(isr_num, addr3, (uint32_t)(isr_addr>>32));
	isr_setidt_field(isr_num, sel, 8);
	isr_setidt_field(isr_num, ist, 0);
	isr_setidt_field(isr_num, type, isr_type); // DPL and gate type
}

void isr_init(struct hbt *hbt)
{
	/* Will fill the IDT with all 256 handlers */
	isr_register_all();

	/* Use the default wrappers for these handlers */
	isr_install(0xe, isr_handle_page_fault);
	isr_install(0xd, isr_handle_gen_prot_fault);

	/* Register the IDT in the IDTR */
	struct isr_idtr *idtr = isr_getidtr();
	idtr->size = ISR_NUM_TOTAL_HANDLERS*sizeof(struct isr_idt)-1;
	idtr->offset = (uint64_t)isr_getidt();
	lidt(*idtr);

	apic_init(hbt);

	/* Safe to enable interrupts now */
	sti();
}

void isr_ap_init(void)
{
	struct isr_idtr *idtr = isr_getidtr();
	idtr->size = ISR_NUM_TOTAL_HANDLERS*sizeof(struct isr_idt)-1;
	idtr->offset = (uint64_t)isr_getidt();
	lidt(*idtr);

	apic_ap_init();

	/* Safe to enable interrupts now */
	sti();
}

void isr_mask_irq(enum isr_irq irq, enum isr_mask irq_mask)
{
	kassert(irq, <, 16);

	uint16_t port = irq < 8 ? PICMASTER_DATA : PICSLAVE_DATA;
	uint8_t  val  = inb(port);
	irq %= 8;
	switch(irq_mask) {
	case ISR_IRQ_MASKED:
		val |= 1<<irq;
		break;
	case ISR_IRQ_UNMASKED:
		val &= ~(1<<irq);
		break;
	default:
		panic("Invalid 'irq_mask'", irq_mask);
	}

	outb(port, val);
}

void isr_install(uint8_t isr_num, isr_t *handler)
{
	isr_t **isr = isr_getisr(isr_num);
	*isr = handler;
}

void isr_handler(struct isr_arg *arg)
{
	isr_t **isr = isr_getisr(arg->isr_num);
	kassert_notnull(*isr, arg->isr_num);

	(**isr)(arg);
}

void isr_register_eoi(void (*eoi)(void))
{
	isr_private_data.eoi = eoi;
}

void isr_send_eoi(void)
{
	kassert(isr_private_data.eoi, !=, NULL);
	isr_private_data.eoi();
}

void isr_register_unmask(void (*unmask)(size_t))
{
	isr_private_data.unmask = unmask;
}

void isr_unmask(size_t vec_num)
{
	kassert(isr_private_data.unmask, !=, NULL);
	isr_private_data.unmask(vec_num);
}
