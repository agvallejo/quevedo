#ifndef QUEVEDO_APIC_H
#define QUEVEDO_APIC_H

#include <stdint.h>

struct hbt;
void apic_init(struct hbt *hbt);

void apic_ap_init(void);

/**
 * Wake a specific core
 *
 * @param apic_id ID of the uninit core
 * @param trampoline Page for bootstrap
 */
void apic_wake(uint8_t apic_id, uintptr_t trampoline);

/**
 * Retrieve the local ID of this core
 *
 * @param apic_id ID of the uninit core
 */
uint8_t apic_getid(void);

#endif /* QUEVEDO_APIC_H */
