#ifndef ISR_PRIVATE_H
#define ISR_PRIVATE_H

#include "isr.h"
#include <stdint.h>
#include <stddef.h>

#define SYSCALL_INT (0x40U)

/** Number of ISRs*/
#define ISR_NUM_TOTAL_HANDLERS (256U)

/** PIC ports */
#define PICMASTER_CMD  (0x20U)
#define PICMASTER_DATA (0x21U)
#define PICSLAVE_CMD   (0xA0U)
#define PICSLAVE_DATA  (0xA1U)

/** Interrupt Descriptor Table */
struct isr_idt {
	uint16_t addr1;
	uint16_t sel;
	uint8_t  ist;
	uint8_t  type;
	uint16_t addr2;
	uint32_t addr3;
	uint32_t resvd;
};

/** IDT Register */
struct isr_idtr {
	uint16_t size;
	uint64_t offset;
}__attribute__((packed));

// TODO: 'idtr' is probably useless. Once the IDTR has been loaded the source
//       data shouldn't be required
struct isr_data {
	struct isr_idt idt[ISR_NUM_TOTAL_HANDLERS];
	struct isr_idtr idtr;
	isr_t *isr_handlers[ISR_NUM_TOTAL_HANDLERS];
        void (*eoi)(void);
        void (*unmask)(size_t vec_num);
};

#define isr_getidt() (isr_private_data.idt)
#define isr_getidt_field(idx,field) (&isr_private_data.idt[(idx)].field)
#define isr_getidtr() (&isr_private_data.idtr)
#define isr_getisr(idx) (&isr_private_data.isr_handlers[(idx)])

#define isr_setidt_field(idx,field,val) do{*isr_getidt_field(idx,field) = (val);}while(0)

/**
 * Store into the IDTR register
 *
 * @param idtr Location and size of the IDT
 */
#ifdef UNIT_TESTING
void lidt(struct isr_idtr idtr);
#else
static inline void lidt(struct isr_idtr idtr)
{
	__asm__ ("lidt %0" :: "m"(idtr));
}
#endif //UNIT_TESTING

/**
 * Main interrupt handler
 *
 * All default assembly wrappers call this function
 *
 * @param arg Pointer to the stack frame
 */
void isr_handler(struct isr_arg *arg);

/**
 * Syscall handler.
 *
 * Entry point for syscalls. This is NOT the C entry point, but the assembly
 * handler that passes control later on to isr_syscall and returns with iretq
 */
void isr_syscall_wrapper(void);

/** Register a default interrupt handler for every interrupt vector */
void isr_register_all(void);

#endif // ISR_PRIVATE_H
