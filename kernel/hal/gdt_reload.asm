global lgdt
lgdt:
	lgdt [rdi]

	; Selector at RPL-3
	mov rax, rsi
	shl rax, 3
	or rax, 3
	ltr ax

	; Reload data segments. The null segment is perfectly ok for long mode
	mov ax, 0x10
	mov ss, ax
	mov ds, ax
	mov es, ax
	mov fs, ax

	; Careful here. We want to override everything, but not the base address
	; I'm sure I'm being needlessly paranoid here, but I don't want my percore
	; struct to be lost during initialisation of the APs
	swapgs
	mov gs, ax
	swapgs

	; Reload code segment. We need an iretq or a far return
	pop rax        ; Pop previous RIP
	push qword 0x8 ; Add the CS for the far return
	push rax       ; Recover previous RIP
	o64 retf       ; Far return to reload CS
