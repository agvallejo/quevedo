#ifndef QUEVEDO_HAL_H
#define QUEVEDO_HAL_H

/** Initialise resources not yet initialised by the bootloader */
struct hbt;
void hal_init(struct hbt *hbt);

/** Configure APs with the same resources as the BSP */
void hal_ap_init(void);

#endif /* QUEVEDO_HAL_H */
