#include <quevedo/hal.h>
#include <quevedo/boot.h>
#include <quevedo/panic.h>

#include <mm/hmm.h>
#include <mp/mp.h>

#include "gdt_private.h"
#include "hal_private.h"

#include <stdint.h>
#include <stdatomic.h>

STATIC struct gdt_data gdt_data;

static void gdt_install_tss(void *tss_base)
{
	size_t pos = atomic_fetch_add(&gdt_data.tss_pos, 2);
	uintptr_t tss_limit = -1u+sizeof gdt_data.tss;
	
	gdt_data.gdt[pos]   = 0x0000E90000000000;
	gdt_data.gdt[pos]  |= (0xFFFF & tss_limit);
	gdt_data.gdt[pos]  |= (0xFFFFFF & (uintptr_t)tss_base)<<16;
	gdt_data.gdt[pos]  |= (0xF & (tss_limit>>16))<<48;
	gdt_data.gdt[pos]  |= (0xFF & ((uintptr_t)tss_base>>24))<<56;
	gdt_data.gdt[pos+1] = (uintptr_t)tss_base>>32;

	lgdt(&gdt_data.gdt_ptr, pos);
}

void gdt_init(void)
{
	/* We don't have access to HMM at this stage */
	void *tss_base  = &gdt_data.tss;

	/*
	 * I couldn't be bothered to be create structures
	 * for the GDT. This is as clear as it's going to
	 * get.
	 *
	 * See AMD Architecture Programmer's Manual Vol. 2
	 *     -4.8 Long Mode Segment Registers
	 */
	gdt_data.gdt[HAL_GDT_IDX_NULL]        = 0;
	gdt_data.gdt[HAL_GDT_IDX_KERNEL_CODE] = 0x002F9A000000FFFF;
	gdt_data.gdt[HAL_GDT_IDX_KERNEL_DATA] = 0x002F92000000FFFF;
	gdt_data.gdt[HAL_GDT_IDX_USER_CODE]   = 0x002FFA000000FFFF;
	gdt_data.gdt[HAL_GDT_IDX_USER_DATA]   = 0x002FF2000000FFFF;
	gdt_data.tss_pos = HAL_GDT_IDX_TSS_LO;

	gdt_data.gdt_ptr.limit = sizeof gdt_data.gdt - 1;
	gdt_data.gdt_ptr.base = (uintptr_t)gdt_data.gdt;

	gdt_install_tss(tss_base);
}

void gdt_ap_init(void)
{
	void *tss = hmm_alloc(VMM_REGION_PRIV_GLOBAL, sizeof gdt_data.tss);
	mp_percore(hal)->tss = tss;

	gdt_install_tss(tss);
}

void gdt_tss_update_rsp(uintptr_t rsp)
{
	/* TODO: The BSP should have its TSS in the percore struct */
	uint32_t *tss = mp_percore(id)?mp_percore(hal)->tss:&gdt_data.tss;
	tss[1] = (uint32_t)rsp;
	tss[2] = rsp >> 32;

	mp_percore(self)->rsp = (void*)rsp;
}
