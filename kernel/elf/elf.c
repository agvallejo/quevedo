#include <quevedo/panic.h>
#include <quevedo/hal.h>
#include <quevedo/cfg.h>

#include <mm/vmm.h>

#include <string.h>

#include "elf_private.h"

extern void elf_jmp2user(uintptr_t addr);

static void elf_validate(struct elf_ehdr *ehdr)
{
	kassert(ehdr->e_ident[EI_MAG0],  ==, ELFMAG0);
	kassert(ehdr->e_ident[EI_MAG1],  ==, ELFMAG1);
	kassert(ehdr->e_ident[EI_MAG2],  ==, ELFMAG2);
	kassert(ehdr->e_ident[EI_MAG3],  ==, ELFMAG3);
	kassert(ehdr->e_ident[EI_CLASS], ==, ELFCLASS64);
	kassert(ehdr->e_ident[EI_DATA],  ==, ELFDATA2LSB);
	kassert(ehdr->e_machine, ==, EM_X86_64);
}

NORETURN void elf_load(void *addr) {
	struct elf_ehdr *ehdr = addr;
	elf_validate(ehdr);

	struct elf_phdr *phdr = (void*)((uintptr_t)addr+ehdr->e_phoffset);
	size_t count = ehdr->e_phnum;
	while(count--) {
		if (phdr->p_type == PT_LOAD) {
			//TODO: Sanity assertions
			uintptr_t page_start = VMM_PAGESIZE*(phdr->p_vaddr/VMM_PAGESIZE);
			uintptr_t page_end   = phdr->p_vaddr + phdr->p_memsz;
			page_end += phdr->p_vaddr - page_start;
			if (page_end%VMM_PAGESIZE)
				page_end = VMM_PAGESIZE*(page_end/VMM_PAGESIZE) + VMM_PAGESIZE;
			vmm_alloc_at(page_start, page_end-page_start);

			memcpy((void*)phdr->p_vaddr, (void*)((uintptr_t)addr+phdr->p_offset), phdr->p_filesz);
			memset((void*)(phdr->p_vaddr+phdr->p_filesz), 0, phdr->p_memsz-phdr->p_filesz);

			enum vmm_cfg cfg = VMM_CFG_INVALID;
			/* Nonreadable segments are illegal */
			kassert(phdr->p_flags, &, PF_R);
			/* Writable regions shouldn't be executable */
			kassert(PF_W|PF_X, !=, phdr->p_flags & (PF_W|PF_X));

			if (phdr->p_flags & PF_W)
				cfg = VMM_CFG_U_RW;
			else if (phdr->p_flags & PF_X)
				cfg = VMM_CFG_U_RX;
			else
				cfg = VMM_CFG_U_RO;
			vmm_cfg(page_start, page_end-page_start, cfg);
		}
		phdr = (void*)((uintptr_t)phdr+ehdr->e_phentsize);
	}

	// FIXME: No magic numbers
	uintptr_t stack_location = 256ull<<39;
	vmm_alloc_at(stack_location-VMM_PAGESIZE-TSCH_STACK_SIZE, TSCH_STACK_SIZE);
	vmm_cfg(stack_location-VMM_PAGESIZE-TSCH_STACK_SIZE, TSCH_STACK_SIZE, VMM_CFG_U_RW);

	elf_jmp2user(ehdr->e_entry);
	kassert_unreachable();
}
