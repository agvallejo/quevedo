global elf_jmp2user
elf_jmp2user:

	mov rax, (256<<39)-0x1000
	push qword 0x23 ; SS
	push rax        ; RSP

        ; RFLAGS
	pushfq
	pop rax
	or rax, 0x200   ; Enable IF
	push rax

	push qword 0x1B ; CS
	push rdi        ; RIP

	mov rdi, rsp
	extern gdt_tss_update_rsp
	call gdt_tss_update_rsp

	iretq
