#ifndef QUEVEDO_HMM_H
#define QUEVEDO_HMM_H

#include <mm/vmm.h>

#include <stddef.h>

/** One-off initialisation of the global heap */
void hmm_global_init(void);

/** Per-process initialisation of the local heap */
void hmm_local_init(void);

/**
 * Allocate 'len' octets from the heap
 *
 * The allocated block is word aligned
 *
 * @param region Which region shouldd be used
 * @param octets Allocation size
 * @return Pointer to the allocated block
 */
void *hmm_alloc(enum vmm_region region, size_t octets);

/**
 * Free an allocaated block into the heap
 *
 * Both start and len are required for
 * sanity checking
 *
 * @param addr The start of the block
 * @param len The end of it
 */
void hmm_free(void *addr, size_t len);

#endif /* QUEVEDO_HMM_H */
