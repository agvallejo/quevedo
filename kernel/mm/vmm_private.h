#ifndef QUEVEDO_VMM_PRIVATE_H
#define QUEVEDO_VMM_PRIVATE_H

#include "vmm.h"

struct vmm_data
{
	struct asmm_pool global_pool;
	struct asmm_pool *pool[VMM_REGION_NUM_TOTAL];
};

#define vmm_getpool(x) (vmm_data.pool[(x)])

#endif /* QUEVEDO_VMM_PRIVATE_H */
