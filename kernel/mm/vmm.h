#ifndef QUEVEDO_VMM_H
#define QUEVEDO_VMM_H

#include <mm/pmm.h>
#include <quevedo/cfg.h>

#include <stdint.h>
#include <stddef.h>

#define VMM_PAGESIZE ((uintptr_t)4096)

/** Configuration flags for a memory region */
enum vmm_cfg{
	VMM_CFG_INVALID  = 0x00,
	VMM_CFG_R        = 0x01,
	VMM_CFG_W        = 0x02,
	VMM_CFG_X        = 0x04,
	VMM_CFG_U        = 0x08,
	VMM_CFG_CD       = 0x10,

	VMM_CFG_RO     = VMM_CFG_R,
	VMM_CFG_RW     = VMM_CFG_R |VMM_CFG_W,
	VMM_CFG_RX     = VMM_CFG_R |VMM_CFG_X,
	VMM_CFG_RW_CD  = VMM_CFG_RW|VMM_CFG_CD,
	VMM_CFG_U_RO   = VMM_CFG_U |VMM_CFG_RO,
	VMM_CFG_U_RW   = VMM_CFG_U |VMM_CFG_RW,
	VMM_CFG_U_RX   = VMM_CFG_U |VMM_CFG_RX,
	VMM_CFG_U_RWX  = VMM_CFG_W |VMM_CFG_U_RX
};

enum vmm_region {
	VMM_REGION_USER,
	VMM_REGION_PRIV_LOCAL,
	VMM_REGION_PTABLES,
	VMM_REGION_PRIV_GLOBAL,
	VMM_REGION_NUM_TOTAL
};

/**
 * Determine the region of a virtual address
 *
 * @param vaddr Address to check
 * @return Region of the address
 */
static inline enum vmm_region vmm_getregion(uintptr_t vaddr)
{
	if (vaddr < VMM_REGION_PRIV_LOCAL_MINADDR)
		return VMM_REGION_USER;
	if (vaddr < VMM_REGION_PTABLES_MINADDR)
		return VMM_REGION_PRIV_LOCAL;
	if (vaddr < VMM_REGION_PRIV_GLOBAL_MINADDR)
		return VMM_REGION_PTABLES;
	return VMM_REGION_PRIV_GLOBAL;
}

/**
 * Initialise the global vmm
 *
 * The initial state is all used
 */
void vmm_global_init(void);

/**
 * Initialise the local vmm
 *
 * The initial state is all used
 */
void vmm_local_init(void);

/**
 * Allocate a virtual memory region
 *
 * Pages will be allocated as needed and mapped
 * as RW
 *
 * @param region Virtual region to use
 * @param len    Size of the memory chunk
 * @return       Allocated page
 */
uintptr_t vmm_alloc(enum vmm_region region, size_t octets);

/**
 * Allocate a specific virtual memory region
 *
 * Pages will be allocated as needed and mapped
 * as RW
 *
 * @param vaddr Start address of the memory chunk
 * @param len   Size of the memory chunk
 */
void vmm_alloc_at(uintptr_t vaddr, size_t octets);

/**
 * Unmap a physical page in the virtual address space
 *
 * It WILL be freed as a normal page
 *
 * @param vaddr Virtual address for the page
 */
void vmm_free(uintptr_t vaddr, size_t octets);

/**
 * Map a physical page in the virtual address space
 *
 * Assumes the all required pages are already allocated
 * and the physical page will be mapped as RW
 *
 * @param vaddr Virtual address for the page
 * @param paddr Physical page to map
 */
void vmm_map_at(uintptr_t vaddr, pmm_addr paddr);

/**
 * Map a physical region somewhere in the virtual address space
 *
 * Will allocate the required tables and map the provided physical page into them
 *
 * @param region The virtual pool to use
 * @param paddr Start of the physical region
 * @param octets Size of the region
 * @param[out] vaddr Start of the virtual map
 * @return True on success
 */
bool vmm_map(enum vmm_region region, pmm_addr paddr, size_t octets, uintptr_t *vaddr);

/**
 * Unmap a physical page in the virtual address space
 *
 * It WON'T be freed as a normal page. The physical
 * page needs to be passed for sanity checks.
 *
 * @param vaddr Virtual address for the page
 * @param paddr Physical address for the page
 */
void vmm_unmap(uintptr_t vaddr, pmm_addr expected_paddr);

/**
 * Reconfigure a virtual memory chunk
 *
 * Assumes the all required pages are already allocated
 *
 * @param vaddr Start address of the memory region
 * @param len   Size of the memory region
 * @param cfg   Configuration of the memory map
 */
void vmm_cfg(uintptr_t vaddr, size_t len, enum vmm_cfg cfg);

/**
 * Create a new address space
 *
 * @return The PML4 physical address
 */
uintptr_t vmm_create_address_space(void);

/**
 * Destroy an address space
 *
 * @param pml4 PML4 of the address space to destroy
 */
void vmm_destroy_address_space(uintptr_t pml4);

#endif /* QUEVEDO_VMM_H */
