#ifndef QUEVEDO_PTMM_H
#define QUEVEDO_PTMM_H

#include "pmm.h"
#include "vmm.h"

#include <stdint.h>

/**
 * Map a physical page into 'vaddr'
 *
 * Assumes the virtual address is available
 *
 * @param vaddr Virtual location of the page
 * @param paddr Physical location of the page
 */
void ptmm_map(uintptr_t vaddr, pmm_addr paddr);

/**
 * Unmap a physical page from 'vaddr'
 *
 * Assumes the virtual address is in use
 *
 * @param vaddr Virtual location of the page
 * @return Physical page present at 'vaddr'
 */
pmm_addr ptmm_unmap(uintptr_t vaddr);

/**
 * Reconfigure the attributes of 'vaddr'
 *
 * Assumes the virtual address is in use
 *
 * @param vaddr Virtual location of the page
 * @param cfg New configuration of the page
 */
void ptmm_cfg(uintptr_t vaddr, enum vmm_cfg cfg);

/**
 * Create a new address space using given PML4
 *
 * Mirrors the kernel data and leaves everything
 * else blank
 *
 * @param pml4 Virtually mapped PML4
 */
void ptmm_create_address_space(uintptr_t virt_pml4, pmm_addr phy_pml4);

#endif /* QUEVEDO_PTMM_H */
