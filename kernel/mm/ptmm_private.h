#ifndef QUEVEDO_PTMM_PRIVATE_H
#define QUEVEDO_PTMM_PRIVATE_H

#include "ptmm.h"

#include <stdint.h>

#define PTMM_NUM_ENTRIES (512u)

#define PTMM_FLAGS_P  (1ull<<0)  /**< Present */
#define PTMM_FLAGS_W  (1ull<<1)  /**< Read/Write */
#define PTMM_FLAGS_U  (1ull<<2)  /**< Available to users */
#define PTMM_FLAGS_WT (1ull<<3)  /**< Write-through (write-back if cleared) */
#define PTMM_FLAGS_CD (1ull<<4)  /**< Disable Cache */
#define PTMM_FLAGS_A  (1ull<<5)  /**< Accessed page */
#define PTMM_FLAGS_D  (1ull<<6)  /**< Dirty page */
#define PTMM_FLAGS_S  (1ull<<7)  /**< Big Page (2MiB) */
#define PTMM_FLAGS_G  (1ull<<8)  /**< Global page */
#define PTMM_FLAGS_NX (1ull<<63) /**< No-eXecute */

/* All flags actively used in configuration */
#define PTMM_FLAGS_CFG_ALL (PTMM_FLAGS_U|PTMM_FLAGS_W|PTMM_FLAGS_P|PTMM_FLAGS_NX|PTMM_FLAGS_CD)

/* All flags. Period. If it doesn't define a physical address, then it's a flag */
#define PTMM_FLAGS_ALL (PTMM_FLAGS_NX|((1<<12)-1))

/* PML4's 511-th entry has the kernel so the paging tables go into the 510-th */
#define PTMM_REC_ENTRY (510U)

/* Given an address, *_STARTING_BIT this is where to start counting for 9 bits */
#define PML4_STARTING_BIT (39U)
#define PDPT_STARTING_BIT (30U)
#define PD_STARTING_BIT   (21U)
#define PT_STARTING_BIT   (12U)

/* Provides sign extension if the recursive entry is higher than 255.
 * Note that an address that involves PML4[256-511] and zeroes on the MSBs are
 * not canonical and cannot be used on amd64. That's why the sign extension.
 */
#define PTMM_SIGN_EXT (~(((uintptr_t)1<<(PML4_STARTING_BIT+9))-1))
#define PTMM_TAB_SIGN_EXT(x) (((x)<256)?0:PTMM_SIGN_EXT)

/* The address at which the recursive mapping starts */
#define PTMM_TAB_START (PTMM_TAB_SIGN_EXT(PTMM_REC_ENTRY) | (((uintptr_t)PTMM_REC_ENTRY)<<PML4_STARTING_BIT))

/* The start at which different types of tables start
 * In order to access one level above we need to keep recursing through the
 * recursive entry.
 * i.e: 1) For the PML4 we need to recurse all the time.
 *      sign|510(9bits)|510(9bits)|510(9bits)|510(9bits)|offset(12bits)
 *      2) For the PDPT we need to recurse all three times.
 *      sign|510(9bits)|510(9bits)|510(9bits)|which_table(9bits)|offset(12bits)
 * The which_table is given by the address we are translating, appropriately
 * shifted and masked
 */
#define PTMM_BASE_PT   (PTMM_TAB_START)
#define PTMM_BASE_PD   (PTMM_BASE_PT   | (((uintptr_t)PTMM_REC_ENTRY)<<PDPT_STARTING_BIT))
#define PTMM_BASE_PDPT (PTMM_BASE_PD   | (((uintptr_t)PTMM_REC_ENTRY)<<PD_STARTING_BIT))
#define PTMM_BASE_PML4 (PTMM_BASE_PDPT | (((uintptr_t)PTMM_REC_ENTRY)<<PT_STARTING_BIT))

/* This looks more frightening than it is.
 * We take the recursive base address and then get the "which_table" part
 * We get it by shifting by 9*N (index bits), removing the sign extension,
 * and masking out the 12 LSB (page offset)
 *
 * Returns pointer to array because SAFETY FIRST
 */
#define ptmm_getpt(x)   ((uint64_t(*)[PTMM_NUM_ENTRIES])(PTMM_BASE_PT   | (((x)>>(1*9)) & 0x7FFFFFF000)))
#define ptmm_getpd(x)   ((uint64_t(*)[PTMM_NUM_ENTRIES])(PTMM_BASE_PD   | (((x)>>(2*9)) &   0x3FFFF000)))
#define ptmm_getpdpt(x) ((uint64_t(*)[PTMM_NUM_ENTRIES])(PTMM_BASE_PDPT | (((x)>>(3*9)) &     0x1FF000)))
#define ptmm_getpml4()  ((uint64_t(*)[PTMM_NUM_ENTRIES]) PTMM_BASE_PML4)

#define ptmm_getidx_pt(x)   (((x)>>PT_STARTING_BIT)   & 0x1FF)
#define ptmm_getidx_pd(x)   (((x)>>PD_STARTING_BIT)   & 0x1FF)
#define ptmm_getidx_pdpt(x) (((x)>>PDPT_STARTING_BIT) & 0x1FF)
#define ptmm_getidx_pml4(x) (((x)>>PML4_STARTING_BIT) & 0x1FF)

void ptmm_invlpg(uintptr_t vaddr);
void ptmm_flushtlb(void);

#endif /* QUEVEDO_PTMM_PRIVATE_H */
