#ifndef QUEVEDO_PMM_H
#define QUEVEDO_PMM_H

#include <stdint.h>
#include <stdbool.h>

/** Size of the physical memory space (it could be bigger than uintptr_t) */
typedef uint64_t pmm_addr;

/**
 * Initialise the pmm
 *
 * @param hbt the Handover Boot Table
 */
struct hbt;
void pmm_init(struct hbt *hbt);

/**
 * Allocate a 4KiB physical page
 *
 * @param[out] On output, allocated page
 * @return True if the allocation was successful
 */
bool pmm_alloc(pmm_addr *paddr_out);

/**
 * Free a 4KiB physical page
 *
 * @param Page to free
 */
void pmm_free(pmm_addr page);

#endif /* QUEVEDO_PMM_H */
