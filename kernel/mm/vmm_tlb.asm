global ptmm_invlpg
ptmm_invlpg:
	invlpg [rdi]
	ret

global ptmm_flushtlb
ptmm_flushtlb:
	mov rax, cr3
	mov cr3, rax
	ret
