#ifndef QUEVEDO_PMM_PRIVATE_H 
#define QUEVEDO_PMM_PRIVATE_H 

#include "pmm.h"
#include "asmm.h"

struct pmm_data {
	struct asmm_pool pool;
};

#define pmm_getpool() (&pmm_data.pool)

#endif /* QUEVEDO_PMM_PRIVATE_H */
