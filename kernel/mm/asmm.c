#include <sys/queue.h>

#include <mp/mp.h>
#include "asmm.h"
#include "hmm.h"

#include <quevedo/cfg.h>
#include <quevedo/panic.h>

#include <stdbool.h>

static bool asmm_is_contained(const struct asmm_entry *entry, uintptr_t vaddr)
{
	/* CAREFUL WITH THE CORNER CASE. start+octets might overflow to zero */
	return entry->start <= vaddr && (vaddr-entry->start)<entry->octets;
}

static bool asmm_are_adyacent(const struct asmm_entry *prev, const struct asmm_entry *next)
{
	kassert(prev->start, !=, next->start);
	if (asmm_is_contained(prev, next->start) || asmm_is_contained(next, prev->start))
	{
		panic("Overlapping blocks", 0);
	}
	return next->start == (prev->start+prev->octets);
}

void asmm_free(struct asmm_pool *pool, uintptr_t start, size_t octets)
{
	mp_lock(&pool->mutex);

	kassert_false(start,  &, VMM_PAGESIZE-1);
	kassert_false(octets, &, VMM_PAGESIZE-1);
	kassert(octets, !=, 0);

	struct asmm_entry input = {.start=start, .octets=octets};
	struct asmm_head *head = &pool->head;

	struct asmm_entry *entry;
	if (LIST_EMPTY(head) || LIST_FIRST(head)->start > start)
	{
		/* First element in the queue */
		entry = hmm_alloc(pool->heap_region, sizeof *entry);
		*entry = input;
		LIST_INSERT_HEAD(head, entry, link);
		mp_unlock(&pool->mutex);
		return;
	}

	LIST_FOREACH(entry, head, link)
	{
		struct asmm_entry *next = LIST_NEXT(entry, link);
		if (asmm_are_adyacent(entry, &input))
		{
			/* input is immediately after entry */
			entry->octets += octets;

			if (next && asmm_are_adyacent(entry, next))
			{
				/* Lucky. Now the next entry is also adyacent */
				entry->octets += next->octets;
				LIST_REMOVE(next, link);
				hmm_free(next, sizeof *next);
			}
			mp_unlock(&pool->mutex);
			return;
		}
		else if (next && asmm_are_adyacent(&input, next))
		{
			/* input is immediately before entry */
			entry->start -= octets;
			entry->octets += octets;

			/* No coalescing can happen or we would have coalesced before */
			mp_unlock(&pool->mutex);
			return;
		}
		else if (!next || (next->start>start))
		{
			/* Non-adyacent and in the middle */
			struct asmm_entry *new_entry = hmm_alloc(pool->heap_region, sizeof *new_entry);
			*new_entry = input;
			LIST_INSERT_AFTER(entry, new_entry, link);
			mp_unlock(&pool->mutex);
			return;
		}
	}
	
	kassert_unreachable();
	mp_unlock(&pool->mutex);
}

bool asmm_alloc(struct asmm_pool *pool, size_t octets, uintptr_t *vaddr_out)
{
	mp_lock(&pool->mutex);

	kassert_false(octets, &, VMM_PAGESIZE-1);
	kassert(octets, !=, 0);

	struct asmm_head *head = &pool->head;
	struct asmm_entry *entry;
	LIST_FOREACH(entry, head, link)
	{
		if (entry->octets >= octets)
		{
			/* This block is big enough */
			*vaddr_out = entry->start;

			entry->start += octets;
			entry->octets -= octets;
			if (!entry->octets)
			{
				/* The block is gone */
				LIST_REMOVE(entry, link);
				hmm_free(entry, sizeof *entry);
			}
			mp_unlock(&pool->mutex);
			return true;
		}
	}
	mp_unlock(&pool->mutex);
	kassert_unreachable();
	return false;
}

void asmm_alloc_at(struct asmm_pool *pool, uintptr_t vaddr, size_t octets)
{
	mp_lock(&pool->mutex);

	kassert_false(vaddr,  &, VMM_PAGESIZE-1);
	kassert_false(octets, &, VMM_PAGESIZE-1);
	kassert(octets, !=, 0);

	struct asmm_head *head = &pool->head;
	struct asmm_entry *entry;
	LIST_FOREACH(entry, head, link)
	{
		/* Is this the block containing the region? */
		if ((entry->start <= vaddr) &&
		    ((entry->start+entry->octets-1) >= (vaddr+octets-1)))
		{
			/* Is the block at the beggining? */
			if (entry->start == vaddr)
			{
				entry->start += octets;
				entry->octets -= octets;
				if (!octets)
				{
					/* The block is gone */
					LIST_REMOVE(entry, link);
					hmm_free(entry, sizeof *entry);
				}
				mp_unlock(&pool->mutex);
				return;
			}

			/* Is the block at the end? Careful with the edge cases */
			if ((entry->start+entry->octets-1) == (vaddr+octets-1))
			{
				entry->octets -= octets;
				if (!octets)
				{
					/* The block is gone */
					LIST_REMOVE(entry, link);
					hmm_free(entry, sizeof *entry);
				}
				mp_unlock(&pool->mutex);
				return;
			}

			/* Is the block in the middle? Careful with the edge cases */
			if ((entry->start < vaddr) &&
			    ((entry->start+entry->octets-1) > (vaddr+octets-1)))
			{
				/* Resized size of the leading chunk */
				size_t first_octets = vaddr-entry->start;

				/* Trailing chunk */
				struct asmm_entry *new_entry = hmm_alloc(pool->heap_region, sizeof *new_entry);
				new_entry->start = entry->start+first_octets+octets;
				new_entry->octets = entry->octets-first_octets-octets;

				entry->octets = first_octets;
				LIST_INSERT_AFTER(entry, new_entry, link);
				mp_unlock(&pool->mutex);
				return;
			}
			panic("Inconsistency error", 0);
		}
	}
	kassert_unreachable();
	mp_unlock(&pool->mutex);
}
