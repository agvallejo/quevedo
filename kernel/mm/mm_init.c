#include "hmm.h"
#include "pmm.h"
#include "vmm.h"
#include "mm_init.h"

void mm_global_init(struct hbt *hbt)
{
	hmm_global_init();
	pmm_init(hbt);
	vmm_global_init();
}

void mm_local_init(void)
{
	hmm_local_init();
	vmm_local_init();
}
