#ifndef QUEVEDO_HMM_PRIVATE_H
#define QUEVEDO_HMM_PRIVATE_H

#include <quevedo/cfg.h>

#include "hmm.h"
#include "vmm.h"

#include <stdint.h>
#include <stdbool.h>
#include <stdatomic.h>

struct hmm_entry {
	size_t octets_in_use; /**< Octets requested by the user*/
};

static inline bool hmm_entry_is_free(struct hmm_entry *entry)
{
	return !entry->octets_in_use;
}

struct hmm_block {
	struct hmm_entry meta;
#define HMM_BLOCKSIZE ((HMM_POOLSIZE/HMM_NUM_ENTRIES)-sizeof(struct hmm_entry))
	uint8_t block[HMM_BLOCKSIZE];
};

struct hmm_data {
	struct hmm_block heap_global[HMM_NUM_ENTRIES];
	struct {
		atomic_flag mutex;
		struct hmm_block *heap;
	} region[VMM_REGION_NUM_TOTAL];
};

#endif /* QUEVEDO_HMM_PRIVATE_H */
