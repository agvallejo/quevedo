#include <sys/queue.h>

#include <mp/mp.h>
#include "hmm_private.h"
#include "vmm.h"
#include "ptmm.h"

#include <quevedo/panic.h>

#include <string.h>
#include <stddef.h>
#include <stdbool.h>

STATIC struct hmm_data hmm_data;

#define HMM_DEBUG /** Enable various checks and tricks */

static void hmm_block_free(struct hmm_entry *entry)
{
#ifdef HMM_DEBUG
	memset(entry+1, 0x66, HMM_BLOCKSIZE);
#endif /* HMM_DEBUG */

	entry->octets_in_use = 0;
}

static void *hmm_block_alloc(struct hmm_entry *entry, size_t size)
{
	entry->octets_in_use = size;

#ifdef HMM_DEBUG
	kassert(size, <=, HMM_BLOCKSIZE);
	memset(entry+1, 0x99, HMM_BLOCKSIZE);
#endif /* HMM_DEBUG */
	return entry+1;
}

static void hmm_reset(enum vmm_region region)
{
	struct hmm_block *block = hmm_data.region[region].heap;
	for (size_t i=0; i<HMM_NUM_ENTRIES; i++)
	{
		hmm_block_free(&block[i].meta);
	}
}

void hmm_global_init(void)
{
	/* Initialise all heap pointers to NULL */
	for (enum vmm_region i=0; i<VMM_REGION_NUM_TOTAL; i++)
	{
		hmm_data.region[i].heap = NULL;
	}

	/* hmm_data will live in VMM_PRIV_GLOBAL, but deducing the region
	 * easier and hmm_data is going to be in VMM_PRIV_GLOBAL anyway
	 */
	enum vmm_region region;
	region = vmm_getregion((uintptr_t)hmm_data.heap_global);
	hmm_data.region[region].heap = hmm_data.heap_global;
	hmm_reset(region);
}

void hmm_local_init(void)
{
	/* First, map the actual heap */
	for (uintptr_t vaddr = HMM_LOCAL_POOL_ADDR;
	     vaddr!=(HMM_LOCAL_POOL_ADDR+HMM_POOLSIZE);
	     vaddr += VMM_PAGESIZE)
	{
		/* We can't use vmm_alloc() because it
		 * relies on asmm and that relies on hmm,
		 * which is not initialised yet. This ugly
		 * chicken-egg scenario is broken by using
		 * ptmm directly and later on initialising
		 * asmm.
		 */
		pmm_addr paddr;
		kassert_bool(pmm_alloc(&paddr), 0);
		ptmm_map(vaddr, paddr);
	}

	enum vmm_region region = VMM_REGION_PRIV_LOCAL;
	hmm_data.region[region].heap = (struct hmm_block*)HMM_LOCAL_POOL_ADDR;
	hmm_reset(region);
}

void hmm_free(void *start, size_t octets)
{
	enum vmm_region region = vmm_getregion((uintptr_t)start);
	mp_lock(&hmm_data.region[region].mutex);

	struct hmm_entry *entry = start;
	entry--;

	/* Ensure the caller knows what they allocated */
	kassert(octets, >, 0);
	kassert(entry->octets_in_use, ==, octets);

#ifdef HMM_DEBUG
	/* Assert this block was allocated */
	struct hmm_block *block = hmm_data.region[region].heap;
	bool found = false;
	for (size_t i=0; i<HMM_NUM_ENTRIES; i++)
	{
		if (entry == &block[i].meta)
		{
			found = true;
			break;
		}
	}
	kassert_bool(found, (uintptr_t)start);
#endif /* HMM_DEBUG */

	hmm_block_free(entry);

	mp_unlock(&hmm_data.region[region].mutex);
}

void *hmm_alloc(enum vmm_region region, size_t octets)
{
	mp_lock(&hmm_data.region[region].mutex);
	kassert(octets, >, 0);
	struct hmm_block *block = hmm_data.region[region].heap;
	kassert(block, !=, NULL);
	for (size_t i=0; i<HMM_NUM_ENTRIES; i++)
		if (hmm_entry_is_free(&block[i].meta))
		{
			mp_unlock(&hmm_data.region[region].mutex);
			return hmm_block_alloc(&block[i].meta, octets);
		}
	mp_unlock(&hmm_data.region[region].mutex);
	kassert_unreachable();
	return NULL;
}
