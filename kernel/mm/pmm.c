#include "pmm_private.h"
#include "vmm.h"

#include <quevedo/panic.h>
#include <quevedo/boot.h>

#include <stdbool.h>
#include <stdio.h>

STATIC struct pmm_data pmm_data;

static void pmm_alloc_at(pmm_addr paddr, size_t octets)
{
	kassert(octets, !=, 0);
	kassert_false(octets, %, VMM_PAGESIZE);
	asmm_alloc_at(pmm_getpool(), paddr, octets);
}

void pmm_init(struct hbt *hbt)
{
	struct asmm_pool *pool = pmm_getpool();
	/* The new list (initially empty) */
	LIST_INIT(&pool->head);

	/* Metadata for physical pages goes in the global scope */
	pool->heap_region = VMM_REGION_PRIV_GLOBAL;

	struct hbt_pmm *pmm = &hbt->pmm;
	for (size_t i=0; i<pmm->map_size; i++)
	{
		uintptr_t start = pmm->map[i].start;
		size_t end = start+pmm->map[i].octets;
		printf("pmm[%u]: start=0x%lX end=0x%lX\n", i, start, end);

		while (start != end)
		{
			pmm_free(start);
			start += VMM_PAGESIZE;
		}
	}
	for (size_t i=0; i<pmm->rsvd_pages_size; i++)
	{
		uintptr_t start = pmm->rsvd_pages[i].start;
		uintptr_t octets = pmm->rsvd_pages[i].octets;
		pmm_alloc_at(start, octets);
		printf("pmm - rsvd[%u]: start=0x%lX octets=0x%lX\n", i, start, octets);
	}
}

bool pmm_alloc(pmm_addr *paddr_out)
{
	return asmm_alloc(pmm_getpool(), VMM_PAGESIZE, paddr_out);
}

void pmm_free(pmm_addr page)
{
	/* Perform cleanup before returning the page */
	page &= ~(VMM_PAGESIZE-1);
	/* Perform the sign extension (NX bit nonsense) */
	page |= (page&(1llu<<62))<<1;

	asmm_free(pmm_getpool(), page, VMM_PAGESIZE);
}
