#ifndef QUEVEDO_MM_INIT_H
#define QUEVEDO_MM_INIT_H

/** Initialise the global memory manager */
struct hbt;
void mm_global_init(struct hbt *hbt);

/**
 * Initialise the per-process memory manager
 *
 * Requires \c mm_global_init() first, obviously
 */
void mm_local_init(void);

#endif /* QUEVEDO_MM_INIT_H */

