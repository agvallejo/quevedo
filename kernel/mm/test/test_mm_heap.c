#include <stdbool.h>
#include <stdio.h>
#include <munit.h>

#include "../mm_heap_private.h"
#include "../mm_heap.h"

extern struct mm_heap_data mm_heap_private_data;

struct {
	size_t heap_size;
	size_t alloc_size;
} fixture;

static void *setup(const MunitParameter params[], void* data)
{
	(void)params;

	memset(&mm_heap_private_data, 0, sizeof mm_heap_private_data);
	memset(&fixture, 0, sizeof fixture);

	/* The whole heap is this size */
	fixture.heap_size  = 0x100000;
	void *heap = aligned_alloc(MM_HEAP_ALIGNMENT_MASK+1,fixture.heap_size);
	memset(heap, 0, fixture.heap_size);
	mm_heap_init(heap, fixture.heap_size);

	/* Each tested allocation where we don't mind we use this value */
	fixture.alloc_size = 256;

	return data;
}
static void teardown(void* data)
{
	(void)data;
	free(mm_heap_block_getfirst());
}

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------
static MunitResult test_mm_heap_advance_jumps_block(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	mm_heap_tag *tag = mm_heap_block_getfirst();
	mm_heap_block_advance(&tag);

	munit_assert_ptr(tag-1, ==, mm_heap_block_getlast());
	return MUNIT_OK;
}

static MunitResult test_mm_heap_block_alloc_divides_block(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	mm_heap_tag *tag = mm_heap_block_getfirst();
	mm_heap_block_alloc(tag, fixture.alloc_size);
	mm_heap_block_advance(&tag);

	munit_assert_true(mm_heap_block_isfree(tag));
	mm_heap_block_advance(&tag);
	munit_assert_ptr(tag-1, ==, mm_heap_block_getlast());
	return MUNIT_OK;
}

static MunitResult test_mm_heap_getnextfree_jumps_nonfree(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	mm_heap_tag *tag1 = mm_heap_block_getfirst();
	mm_heap_tag *tag2 = tag1;

	mm_heap_block_alloc(tag1, fixture.alloc_size);
	mm_heap_block_getnextfree(&tag1);
	mm_heap_block_getnext(&tag2);
	munit_assert_ptr(tag1, ==, tag2);
	return MUNIT_OK;
}

static MunitResult test_mm_heap_afterinit_isconsistent(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	mm_heap_tag *head = mm_heap_block_getfirst();
	mm_heap_tag *tail = mm_heap_block_getlast();
	munit_assert_ulong(*head, ==, *tail);
	munit_assert_true(mm_heap_block_isfree(head));
	munit_assert_ulong((uintptr_t)tail-(uintptr_t)(head+1) , ==, mm_heap_block_getlen(head));
	return MUNIT_OK;
}

static MunitResult test_mm_heap_getlen_ignores_lsb(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	mm_heap_tag tag = 0x4321;
	munit_assert_ulong(0x4320, ==, mm_heap_block_getlen(&tag));
	return MUNIT_OK;
}

static MunitResult test_mm_heap_set_free_and_clrfree_are_consistent(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	mm_heap_tag *head = mm_heap_block_getfirst();
	mm_heap_tag *tail = mm_heap_block_getfirst();

	mm_heap_block_setfree(head);
	munit_assert_true(mm_heap_block_isfree(head));
	munit_assert_true(mm_heap_block_isfree(tail));

	mm_heap_block_clrfree(head);
	munit_assert_false(mm_heap_block_isfree(head));
	munit_assert_false(mm_heap_block_isfree(tail));

	return MUNIT_OK;
}

static MunitResult test_mm_heap_setlen_preserves_freebit(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	mm_heap_tag *head = mm_heap_block_getfirst();

	mm_heap_block_setfree(head);
	mm_heap_block_setlen(head, fixture.alloc_size);
	munit_assert_ulong(fixture.alloc_size, ==, mm_heap_block_getlen(head));
	munit_assert_true(mm_heap_block_isfree(head));

	mm_heap_block_clrfree(head);
	mm_heap_block_setlen(head, fixture.alloc_size);
	munit_assert_ulong(fixture.alloc_size, ==, mm_heap_block_getlen(head));
	munit_assert_false(mm_heap_block_isfree(head));

	return MUNIT_OK;
}

static MunitResult test_mm_heap_simple_alloc(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	mm_heap_tag *tag = mm_heap_block_getfirst();
	void *ptr = mm_heap_alloc(fixture.alloc_size);
	
	/* First block is alloced */
	munit_assert_ptr(ptr, ==, tag+1);
	munit_assert_ulong(fixture.alloc_size, ==, mm_heap_block_getlen(tag));
	munit_assert_false(mm_heap_block_isfree(tag));

	/* The rest of the block is resized */
	mm_heap_block_advance(&tag);
	munit_assert_ulong(fixture.heap_size-fixture.alloc_size,
	                   ==, mm_heap_block_getlen(tag)+4*sizeof(mm_heap_tag));;
	return MUNIT_OK;
}

static MunitResult test_mm_heap_simple_free(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	mm_heap_tag *head = mm_heap_block_getfirst();
	void *ptr = mm_heap_alloc(fixture.alloc_size);

	munit_assert_false(mm_heap_block_isfree(head));
	mm_heap_free(ptr);
	munit_assert_true(mm_heap_block_isfree(head));

	return MUNIT_OK;
}

static MunitResult test_mm_heap_complex_alloc(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	void *ptr2 = mm_heap_alloc(fixture.alloc_size);
	void *ptr3 = mm_heap_alloc(fixture.alloc_size);

	mm_heap_free(ptr2);
	void *ptr4 = mm_heap_alloc(2*fixture.alloc_size);

	/* First and third elements are not free. The second is not big enough */
	munit_assert_ptr(ptr4, ==, (void*)((uintptr_t)ptr3+fixture.alloc_size+2*sizeof(mm_heap_tag)));

	return MUNIT_OK;
}

//------------------------------------------------------------------------------
// Test runner
//------------------------------------------------------------------------------
static MunitTest test_suite_tests[] = {
	// Name, function, setup, teardown, options, parameters
	{"/mm_heap/advance/jumps_block", test_mm_heap_advance_jumps_block, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_heap/block_alloc/divides_block", test_mm_heap_block_alloc_divides_block, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_heap/getnextfree/jumps_nonfree", test_mm_heap_getnextfree_jumps_nonfree, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_heap/init/isconsistent", test_mm_heap_afterinit_isconsistent, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_heap/getlen/ignores_lsb", test_mm_heap_getlen_ignores_lsb, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_heap/set-clr_free/are_consistent", test_mm_heap_set_free_and_clrfree_are_consistent, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_heap/setlen/preserves_freebit", test_mm_heap_setlen_preserves_freebit, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_heap/alloc/simple_alloc", test_mm_heap_simple_alloc, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_heap/alloc/simple_free", test_mm_heap_simple_free, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_heap/alloc/complex_alloc", test_mm_heap_complex_alloc, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},

	// NULL ENTRY @ END
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
	"", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
