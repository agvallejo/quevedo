#include <sys/queue.h>

#include <quevedo/cfg.h>
#include <panic_fake.h>

#include "asmm.h"
#include "hmm_fake.h"

#include <string.h>
#include <stdbool.h>

#include <unity_fixture.h>

/* Static test parameters */
#define DEFAULT_GAPSIZE (4*VMM_PAGESIZE)

/* Dynamic test parameters */
enum vmm_region test_region;
struct asmm_pool *test_pool;
#define test_head (&test_pool->head)

static void assert_empty(struct asmm_pool *pool)
{
	TEST_ASSERT_TRUE(LIST_EMPTY(&pool->head));
}

TEST_GROUP(asmm);

TEST_SETUP(asmm)
{
	UnityMalloc_StartTest();

	hmm_fake_setup();
	panic_fake_setup();

	test_pool = calloc(1, sizeof *test_pool);
	LIST_INIT(&test_pool->head);

	uintptr_t base[VMM_REGION_NUM_TOTAL] = {
		0,
		VMM_REGION_PRIV_LOCAL_MINADDR,
		VMM_REGION_PTABLES_MINADDR,
		VMM_REGION_PRIV_GLOBAL_MINADDR
	};

	/* Insert known regions */
	asmm_free(test_pool, base[test_region], DEFAULT_GAPSIZE);
	asmm_free(test_pool, base[test_region]+VMM_PAGESIZE+DEFAULT_GAPSIZE, DEFAULT_GAPSIZE);
}

TEST_TEAR_DOWN(asmm)
{
	hmm_fake_teardown();
	panic_fake_teardown();

	/* Finish all tests with an empty set */
	struct asmm_entry *entry;
	LIST_FOREACH(entry, test_head, link)
	{
		free(entry);
	}

	free(test_pool);

	UnityMalloc_EndTest();
}

TEST(asmm, hasTwoGapsOnEachRegion)
{
	size_t count = 0;
	struct asmm_entry *entry;
	LIST_FOREACH(entry, test_head, link)
	{
		count++;
	}

	TEST_ASSERT_EQUAL(2, count);
}

TEST(asmm, isEmptyAfterAllocatingAll)
{
	/* In little chunks, for fun */
	uintptr_t vaddr;
	asmm_alloc(test_pool, DEFAULT_GAPSIZE, &vaddr);
	asmm_alloc(test_pool, DEFAULT_GAPSIZE-VMM_PAGESIZE, &vaddr);
	asmm_alloc(test_pool, VMM_PAGESIZE, &vaddr);

	/* Now we should've run out of memory */
	assert_empty(test_pool);
}

TEST(asmm, mixFreeInBetweenAllocations)
{
	uintptr_t full_chunk, partial_chunk;
	asmm_alloc(test_pool, DEFAULT_GAPSIZE, &full_chunk);
	asmm_alloc(test_pool, DEFAULT_GAPSIZE-VMM_PAGESIZE, &partial_chunk);

	/* Return them in another order */
	asmm_free(test_pool, full_chunk, DEFAULT_GAPSIZE);
	asmm_free(test_pool, partial_chunk, DEFAULT_GAPSIZE-VMM_PAGESIZE);

	/* Allocate everything that remains */
	uintptr_t vaddr;
	asmm_alloc(test_pool, DEFAULT_GAPSIZE-VMM_PAGESIZE, &vaddr);
	asmm_alloc(test_pool, DEFAULT_GAPSIZE, &vaddr);
	asmm_alloc(test_pool, VMM_PAGESIZE, &vaddr);

	/* Unless corruption happened this should be empty */
	assert_empty(test_pool);
}

TEST(asmm, freeAnAdyacentBlock)
{
	uintptr_t bigger, smaller;
	asmm_alloc(test_pool, DEFAULT_GAPSIZE-VMM_PAGESIZE, &bigger);
	asmm_alloc(test_pool, VMM_PAGESIZE, &smaller);

	/* Return them in this order to exercise another adyacency */
	asmm_free(test_pool, bigger, DEFAULT_GAPSIZE-VMM_PAGESIZE);
	asmm_free(test_pool, smaller, VMM_PAGESIZE);

	/* Allocate the full memory now */
	uintptr_t vaddr;
	asmm_alloc(test_pool, DEFAULT_GAPSIZE, &vaddr);
	asmm_alloc(test_pool, DEFAULT_GAPSIZE, &vaddr);

	/* Unless corruption happened this should be empty */
	assert_empty(test_pool);
}

TEST(asmm, freeABlockThatMergesTwoBlocks)
{
	uintptr_t first, second;
	asmm_alloc(test_pool, VMM_PAGESIZE, &first);
	asmm_alloc(test_pool, VMM_PAGESIZE, &second);

	/* Return them in this order to exercise another adyacency */
	asmm_free(test_pool, first, VMM_PAGESIZE);
	asmm_free(test_pool, second, VMM_PAGESIZE);

	/* Allocate the full memory now */
	uintptr_t vaddr;
	asmm_alloc(test_pool, DEFAULT_GAPSIZE, &vaddr);
	asmm_alloc(test_pool, DEFAULT_GAPSIZE, &vaddr);

	/* Unless corruption happened this should be empty */
	assert_empty(test_pool);
}

TEST(asmm, freeTheLastBlockInTheList)
{
	uintptr_t first, second_bigger, second_smaller;
	asmm_alloc(test_pool, DEFAULT_GAPSIZE, &first);
	asmm_alloc(test_pool, DEFAULT_GAPSIZE-VMM_PAGESIZE, &second_bigger);
	asmm_alloc(test_pool, VMM_PAGESIZE, &second_smaller);

	/* Return them in this order to exercise another adyacency */
	asmm_free(test_pool, first, DEFAULT_GAPSIZE);
	asmm_free(test_pool, second_bigger, DEFAULT_GAPSIZE-VMM_PAGESIZE);
	asmm_free(test_pool, second_smaller, VMM_PAGESIZE);

	/* Allocate the full memory now */
	uintptr_t vaddr;
	asmm_alloc(test_pool, DEFAULT_GAPSIZE, &vaddr);
	asmm_alloc(test_pool, DEFAULT_GAPSIZE, &vaddr);

	/* Unless corruption happened this should be empty */
	assert_empty(test_pool);
}

TEST_GROUP_RUNNER(asmm)
{
	RUN_TEST_CASE(asmm, hasTwoGapsOnEachRegion);
	RUN_TEST_CASE(asmm, isEmptyAfterAllocatingAll);
	RUN_TEST_CASE(asmm, mixFreeInBetweenAllocations);
	RUN_TEST_CASE(asmm, freeAnAdyacentBlock);
	RUN_TEST_CASE(asmm, freeABlockThatMergesTwoBlocks);
	RUN_TEST_CASE(asmm, freeTheLastBlockInTheList);
}

static void runAllTests(void)
{
	for (size_t i=0; i<VMM_REGION_NUM_TOTAL; i++)
	{
		test_region = i;
		RUN_TEST_GROUP(asmm);
	}
}

int main(int argc, const char* argv[])
{
    return UnityMain(argc, argv, runAllTests);
}

