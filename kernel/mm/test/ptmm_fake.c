#include <sys/mman.h>

#include "ptmm.h"

#include <quevedo/panic.h>

#include <stdint.h>

void ptmm_map(uintptr_t vaddr, pmm_addr paddr)
{
	(void)vaddr;
	(void)paddr;
}

pmm_addr ptmm_unmap(uintptr_t vaddr)
{
	(void)vaddr;
	return -1;
}

void ptmm_cfg(uintptr_t vaddr, enum vmm_cfg cfg)
{
	(void)vaddr;
	(void)cfg;
}
