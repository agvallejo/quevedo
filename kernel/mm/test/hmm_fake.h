#ifndef QUEVEDO_HMM_FAKE_H
#define QUEVEDO_HMM_FAKE_H

#ifndef UNIT_TESTING
#error "Wrong file. This is for unit testing only"
#endif /* UNIT_TESTING */

#include <mm/hmm.h>

static inline void hmm_fake_setup(void) {}
static inline void hmm_fake_teardown(void) {}

#endif /* QUEVEDO_HMM_FAKE_H */
