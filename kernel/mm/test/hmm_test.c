#include "unity_fixture.h"

#include <sys/queue.h>

#include <quevedo/cfg.h>

#include <string.h>
#include <stdbool.h>

#include "hmm_private.h"

extern struct hmm_data hmm_data;

/* Dynamic test parameters */
enum vmm_region test_region;
#define test_size (24u)

TEST_GROUP(hmm);

TEST_SETUP(hmm)
{
	UnityMalloc_StartTest();

	memset(&hmm_data, 0, sizeof hmm_data);

	hmm_global_init();
	test_region = vmm_getregion((uintptr_t)(void*)&hmm_data);
}

TEST_TEAR_DOWN(hmm)
{
	UnityMalloc_EndTest();
}

TEST(hmm, simpleAlloc)
{
	void *p1, *p2;

	/* Allocate and free in a certain order */
	p1 = hmm_alloc(test_region, test_size);
	p2 = hmm_alloc(test_region, 2*test_size);
	TEST_ASSERT_NOT_NULL(p1);
	TEST_ASSERT_NOT_NULL(p2);
	hmm_free(p2, 2*test_size);
	hmm_free(p1, test_size);

	/* Allocate and free in another order */
	p1 = hmm_alloc(test_region, test_size);
	p2 = hmm_alloc(test_region, 2*test_size);
	TEST_ASSERT_NOT_NULL(p1);
	TEST_ASSERT_NOT_NULL(p2);
	hmm_free(p1, test_size);
	hmm_free(p2, 2*test_size);
}

TEST_GROUP_RUNNER(hmm)
{
	RUN_TEST_CASE(hmm, simpleAlloc)
}

static void runAllTests(void)
{
	RUN_TEST_GROUP(hmm);
}

int main(int argc, const char* argv[])
{
    return UnityMain(argc, argv, runAllTests);
}

