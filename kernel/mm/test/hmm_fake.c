#include <unity_fixture.h>

#include "hmm.h"
#include "vmm.h"

#include <string.h>

void hmm_fake_setup(void) {}
void hmm_fake_teardown(void) {}

void hmm_free(void *start, size_t octets)
{
	(void)octets;
	memset(start, 0x99, octets);
	free(start);
}

void *hmm_alloc(enum vmm_region region, size_t octets)
{
	(void)region; /* Not very useful here */
	void *ret = malloc(octets);
	memset(ret, 0x66, octets);
	return ret;
}
