TESTNAME:=test_mm_phy
ENV:=test
TEST:=$(TESTSDIR)$(ENV)/$(TESTNAME)
TESTS+=$(TEST)

ASMS:=
SRCS:=\
    $(DIR)../mm_phy.c \
    $(DIR)test_mm_phy.c \
    $(SRCS<test>)
$(eval $(call getobjs,$(TEST)))

CC:=$(CC<$(ENV)>)
CFLAGS:=$(CFLAGS<default>)
CPPFLAGS:=-I$(INCLUDE<test>) -I$(INCLUDE<kernel>) -DSTATIC= -DUNIT_TESTING
LDFLAGS:=
LDLIBS:=
$(eval $(call spawn_c_rule,$(TEST)))
$(eval $(call spawn_link_rule,$(TEST)))
