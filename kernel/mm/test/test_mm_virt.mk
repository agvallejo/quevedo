TESTNAME:=test_mm_virt
ENV:=test
TEST:=$(TESTSDIR)$(ENV)/$(TESTNAME)
TESTS+=$(TEST)

ASMS:=
SRCS:=\
    $(DIR)../mm_virt.c \
    $(DIR)../mm_virt_init.c \
    $(DIR)test_mm_virt.c \
    $(SRCS<test>)
$(eval $(call getobjs,$(TEST)))

CC:=$(CC<$(ENV)>)
CFLAGS:=$(CFLAGS<default>)
CPPFLAGS:=-I$(INCLUDE<test>) -I$(INCLUDE<kernel>) -DSTATIC= -DUNIT_TESTING
LDFLAGS:=
LDLIBS:=
$(eval $(call spawn_c_rule,$(TEST)))
$(eval $(call spawn_link_rule,$(TEST)))
