#include <cpuinfo/cpuinfo.h>
#include <quevedo/mm.h>
#include <quevedo/ld.h>

#include <munit.h>

#include "../mm_virt_private.h"

//------------------------------------------------------------------------------
// Test fixture
//------------------------------------------------------------------------------
extern struct mm_virt_data mm_virt_private_data;
struct cpuinfo_data cpuinfo_private_data;
static struct {
	uint64_t ptables[MM_VIRT_PTABLES_DEPTH][512];
	uint8_t mapped_page[4096];
} fixture __attribute__((aligned(4096)));

// TODO: I don't like having these here. mm_virt needs refactoring
extern void mm_virt_init_ptables(uintptr_t recursive_entry);
struct ld_symbols ld_fake_symbols;

void mm_phy_free(mm_phy_addr_t paddr){(void)paddr;}
void set_cr3(uintptr_t cr3){(void)cr3;}
uintptr_t get_cr3(void){return 0;}

//------------------------------------------------------------------------------
// Fake functions
//------------------------------------------------------------------------------
uint64_t *mm_virt_getpt(  uintptr_t addr) {(void)addr; return &fixture.ptables[3][0];}
uint64_t *mm_virt_getpd(  uintptr_t addr) {(void)addr; return &fixture.ptables[2][0];}
uint64_t *mm_virt_getpdpt(uintptr_t addr) {(void)addr; return &fixture.ptables[1][0];}
uint64_t *mm_virt_getpml4(void)           {            return &fixture.ptables[0][0];}

size_t mm_virt_getidx_pt(  uintptr_t addr) {(void)addr; return 0;}
size_t mm_virt_getidx_pd(  uintptr_t addr) {(void)addr; return 0;}
size_t mm_virt_getidx_pdpt(uintptr_t addr) {(void)addr; return 0;}
size_t mm_virt_getidx_pml4(uintptr_t addr) {(void)addr; return 0;}

uintptr_t mm_phy_alloc(void)
{
	return 0;
}

int test_getprot(const MunitParameter params[])
{
	const char *prot = munit_parameters_get(params, (char*)"prot");
	if (!strcmp(prot,"x"))
		return MM_VIRT_PROT_X;
	else if (!strcmp(prot,"u_x"))
		return MM_VIRT_PROT_U_X;
	else if (!strcmp(prot,"w"))
		return MM_VIRT_PROT_W;
	else if (!strcmp(prot,"u_w"))
		return MM_VIRT_PROT_U_W;
	munit_assert(false);
}



//------------------------------------------------------------------------------
// Setup & Teardown
//------------------------------------------------------------------------------
static void *setup(const MunitParameter params[], void* data)
{
	(void) params;

	memset(&mm_virt_private_data, 0, sizeof mm_virt_private_data);
	memset(&cpuinfo_private_data, 0, sizeof cpuinfo_private_data);
	memset(&fixture, 0, sizeof fixture);

	mm_virt_init_ptables(510);
	return data;
}

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------
static char *alloc_params_str[] = {"x", "u_x", "w", "u_w", NULL};
static MunitParameterEnum alloc_params[] = {{"prot",alloc_params_str},{NULL,NULL}};
static MunitResult test_mm_virt_alloc(const MunitParameter params[], void* data)
{
	(void) data;
	int prot = test_getprot(params);
	mm_virt_alloc((uintptr_t)fixture.mapped_page, 4096, MM_VIRT_BSIZE_4KiB, prot);

	struct mm_virt_ptables *ptables = mm_virt_getptables();
#define test_getflags(x) ((x) & MM_VIRT_AMD64_FLAGS_MASK)
	munit_assert_uint(test_getflags(fixture.ptables[3][0]),
	                  ==, ptables->lut_prot2flags[prot]);

	int flags = MM_VIRT_AMD64_FLAGS_P  |
	            MM_VIRT_AMD64_FLAGS_RW |
	            MM_VIRT_AMD64_FLAGS_US;
	munit_assert_uint(test_getflags(fixture.ptables[2][0]), ==, flags);
	munit_assert_uint(test_getflags(fixture.ptables[1][0]), ==, flags);
	munit_assert_uint(test_getflags(fixture.ptables[0][0]), ==, flags);
	return MUNIT_OK;
}

static MunitResult test_mm_virt_conf(const MunitParameter params[], void* data)
{
	(void) data;
	int prot = test_getprot(params);
	fixture.ptables[0][0] = (uintptr_t)&fixture.ptables[1][0];
	fixture.ptables[1][0] = (uintptr_t)&fixture.ptables[2][0];
	fixture.ptables[2][0] = (uintptr_t)&fixture.ptables[3][0];
	fixture.ptables[3][0] = (uintptr_t)fixture.mapped_page;

	struct mm_virt_ptables *ptables = mm_virt_getptables();
	fixture.ptables[0][0] |= ptables->lut_prot2flags[MM_VIRT_PROT_U_W];
	fixture.ptables[1][0] |= ptables->lut_prot2flags[MM_VIRT_PROT_U_W];
	fixture.ptables[2][0] |= ptables->lut_prot2flags[MM_VIRT_PROT_U_W];

	// Ensure the table doesnt already have the flags we want to check
	if (prot == MM_VIRT_PROT_X)
		fixture.ptables[3][0] |= ptables->lut_prot2flags[MM_VIRT_PROT_U_W];
	else
		fixture.ptables[3][0] |= ptables->lut_prot2flags[MM_VIRT_PROT_X];

	mm_virt_conf((uintptr_t)fixture.mapped_page, 4096, MM_VIRT_BSIZE_4KiB, prot);
	munit_assert_uint(test_getflags(fixture.ptables[3][0]),
                          ==, ptables->lut_prot2flags[prot]);

	return MUNIT_OK;
}

//------------------------------------------------------------------------------
// Test runner
//------------------------------------------------------------------------------
static MunitTest test_suite_tests[] = {
	// Name, function, setup, teardown, options, parameters
	{"/mm_virt/alloc", test_mm_virt_alloc, setup, NULL, MUNIT_TEST_OPTION_NONE, alloc_params},
	{"/mm_virt/conf", test_mm_virt_conf,   setup, NULL, MUNIT_TEST_OPTION_NONE, alloc_params},
	// NULL ENTRY @ END
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
	"", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
