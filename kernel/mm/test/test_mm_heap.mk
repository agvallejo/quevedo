TESTNAME:=test_mm_heap
ENV:=test_mm_heap
TEST:=$(TESTSDIR)$(ENV)/$(TESTNAME)
TESTS+=$(TEST)

ASMS:=
SRCS:=\
    $(DIR)../mm_heap.c \
    $(DIR)test_mm_heap.c \
    $(SRCS<test>)
$(eval $(call getobjs,$(TEST)))


CC:=$(CC<test>)
CFLAGS:=$(CFLAGS<test>)
CPPFLAGS:=$(CPPFLAGS<test>) -I$(INCLUDE<test>) -I$(INCLUDE<kernel>) -DSTATIC= -DUNIT_TESTING
LDFLAGS:=
LDLIBS:=
$(eval $(call spawn_c_rule,$(TEST)))
$(eval $(call spawn_link_rule,$(TEST)))
