#include <quevedo/mm.h>
#include <stdbool.h>
#include <stdio.h>
#include <munit.h>

#include "../mm_phy_private.h"

extern struct mm_phy_data mm_phy_private_data;

void mm_virt_alloc(uintptr_t vaddr, size_t octets, enum mm_virt_bsize bsize,
                   enum mm_virt_prot prot)
{
	struct mm_phy_mgr *mgr = mm_phy_getmgr(MM_VIRT_BSIZE_4KiB);

	size_t alloc_size = 512;
	// Ensure the requested region makes sense
	munit_assert_uint(vaddr, ==, (uintptr_t)(mgr->base-alloc_size));
	munit_assert_uint(octets, ==, alloc_size* sizeof mgr->base[0]);
	munit_assert_uint(bsize, ==, MM_VIRT_BSIZE_4KiB);
	munit_assert_uint(prot, ==, MM_VIRT_PROT_W);

	mgr->base = realloc(mgr->base, octets + (mgr->size * sizeof mgr->base[0]));
	memmove(mgr->base+alloc_size, mgr->base, mgr->size * sizeof mgr->base[0]);
	memset(mgr->base, 0, alloc_size);
	mgr->base += alloc_size;
}

static void *setup(const MunitParameter params[], void* data)
{
	(void)params;

	memset(&mm_phy_private_data, 0, sizeof mm_phy_private_data);
	mm_phy_init();

	return data;
}

static void teardown(void* data)
{
	(void)data;
	struct mm_phy_mgr *mgr = mm_phy_getmgr(MM_VIRT_BSIZE_4KiB);

	free(mgr->base);
}

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------
static MunitResult test_mm_phy_three_allocs(const MunitParameter params[], void* data)
{
	(void)params; (void)data;

	uintptr_t test_addr[] = {0xA000, 0x1000, 0x5000};
	size_t idx;
	for (idx = 0; idx<(sizeof test_addr/sizeof *test_addr); idx++)
		mm_phy_free(test_addr[idx]);

	while(idx--)
		munit_assert_uint(test_addr[idx], ==, mm_phy_alloc());

	return MUNIT_OK;
}

static MunitResult test_mm_phy_stress_pushpop(const MunitParameter params[], void* data)
{
	(void)params; (void)data;

	size_t num_tries = 4096;
	uintptr_t test_addr = 0xABC000;

	for (size_t i = 0; i<num_tries; i++) {
		mm_phy_free(test_addr);
		munit_assert_uint(test_addr, ==, mm_phy_alloc());
	}

	return MUNIT_OK;
}

static MunitResult test_mm_phy_grows_when_overflown(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	struct mm_phy_mgr *mgr = mm_phy_getmgr(MM_VIRT_BSIZE_4KiB);

	// Do it more than once just to make sure it works fine
	for (int i = 0; i<512; i++) {
		while(mgr->next)
			mm_phy_free(i*0x1000);
		mm_phy_free(i*0x1000);

		munit_assert_uint(mgr->next, !=, 0);
	}

	return MUNIT_OK;
}

static MunitResult test_mm_phy_alloc_untils_grows_then_free_everything(const MunitParameter params[], void* data)
{
	(void)params; (void)data;

	size_t num_tries = 4096;
	uintptr_t i;
	for (i = 0; i<num_tries; i++)
		mm_phy_free(i*0x1000);
	while(i--){
		munit_assert_uint(i*0x1000, ==, mm_phy_alloc());
	}

	return MUNIT_OK;
}

static MunitResult test_mm_phy_free_lots_and_check_usage(const MunitParameter params[], void* data)
{
	(void)params; (void)data;
	for (size_t i=0; i<54321; i++)
		mm_phy_free(i*0x1000);

	munit_assert_uint(54321, ==, mm_phy_getusage());
	return MUNIT_OK;
}

//------------------------------------------------------------------------------
// Test runner
//------------------------------------------------------------------------------
static MunitTest test_suite_tests[] = {
	// Name, function, setup, teardown, options, parameters
	{"/mm_phy/alloc_and_free", test_mm_phy_three_allocs, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_phy/stress_pushpop", test_mm_phy_stress_pushpop, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_phy/grow", test_mm_phy_grows_when_overflown, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_phy/alloc_and_free_huge", test_mm_phy_alloc_untils_grows_then_free_everything, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_phy/usage", test_mm_phy_free_lots_and_check_usage, setup, teardown, MUNIT_TEST_OPTION_NONE, NULL},
	// NULL ENTRY @ END
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
	"", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
