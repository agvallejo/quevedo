#include <cpuinfo/cpuinfo.h>
#include <quevedo/ld.h>
#include <quevedo/mm.h>
#include <stdio.h>
#include <assert.h>

#include <munit.h>
#include <fff.h>

#include "../mm_virt_private.h"

extern void mm_virt_init_ptables(uintptr_t recursive_entry);
extern void mm_virt_init_kernel(void);
struct mm_virt_data mm_virt_private_data;
struct cpuinfo_data cpuinfo_private_data;
struct ld_symbols ld_fake_symbols;

DEFINE_FFF_GLOBALS

FAKE_VALUE_FUNC0(mm_phy_addr_t, mm_phy_alloc)
FAKE_VOID_FUNC(mm_virt_conf, uintptr_t, size_t, enum mm_virt_bsize, enum mm_virt_prot)
FAKE_VALUE_FUNC0(uintptr_t*, mm_virt_getpml4)
void set_cr3(uintptr_t cr3){(void)cr3;}
uintptr_t get_cr3(void){return 0;}

static void *setup(const MunitParameter params[], void* data)
{
	(void) params;

	FFF_RESET_HISTORY();
	RESET_FAKE(mm_virt_getpml4);

	memset(&mm_virt_private_data, 0, sizeof mm_virt_private_data);
	memset(&cpuinfo_private_data, 0, sizeof cpuinfo_private_data);
	memset(&ld_fake_symbols, 0, sizeof ld_fake_symbols);

	// Usual multiboot stuff for a 32-bit kernel
	ld_fake_symbols.kernel_lma_start    = 0x00100000;
	ld_fake_symbols.kernel_vma          = 0xC0000000;
	ld_fake_symbols.kernel_text_start   = 0xC0000000;
	ld_fake_symbols.kernel_text_end     = 0xC0002000;
	ld_fake_symbols.kernel_rodata_start = 0xC0002000;
	ld_fake_symbols.kernel_rodata_start = 0xC0004000;
	ld_fake_symbols.kernel_rodata_end   = 0xC0004000;
	ld_fake_symbols.kernel_data_start   = 0xC0006000;
	ld_fake_symbols.kernel_data_end     = 0xC0006000;
	ld_fake_symbols.kernel_bss_start    = 0xC0008000;
	ld_fake_symbols.kernel_bss_end      = 0xC0008000;
	return data;
}

//------------------------------------------------------------------------------
// Tests
//------------------------------------------------------------------------------
static MunitResult test_mm_init_defines_ptables_region(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	mm_virt_init_ptables(MM_RECURSIVE_ENTRY);

	struct mm_virt_ptables *ptables = mm_virt_getptables();
	munit_assert_ptr((void*)ptables->region.start, ==, (void*)MM_DEFAULT_PTABLES_START);
	munit_assert_ulong(ptables->region.octets, ==, ((size_t)512)<<30); // 512GiB

	return MUNIT_OK;
}

static MunitResult test_mm_init_defines_kernel_regions(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	mm_virt_init_kernel();
	struct mm_virt_bin *kernel = mm_virt_getkernel();
	struct mm_virt_region *region;

	region = &kernel->text;
	munit_assert_ptr((void*)region->start, ==, (void*)ld_get(kernel_text_start));
	munit_assert_ulong(region->octets, ==, ld_get(kernel_text_end)-ld_get(kernel_text_start));
	munit_assert_ulong(region->flags, ==, MM_VIRT_PROT_X);

	region = &kernel->rodata;
	munit_assert_ptr((void*)region->start, ==, (void*)ld_get(kernel_rodata_start));
	munit_assert_ulong(region->octets, ==, ld_get(kernel_rodata_end)-ld_get(kernel_rodata_start));
	munit_assert_ulong(region->flags, ==, MM_VIRT_PROT_RO);

	region = &kernel->data;
	munit_assert_ptr((void*)region->start, ==, (void*)ld_get(kernel_data_start));
	munit_assert_ulong(region->octets, ==, ld_get(kernel_data_end)-ld_get(kernel_data_start));
	munit_assert_ulong(region->flags, ==, MM_VIRT_PROT_W);

	region = &kernel->bss;
	munit_assert_ptr((void*)region->start, ==, (void*)ld_get(kernel_bss_start));
	munit_assert_ulong(region->octets, ==, ld_get(kernel_bss_end)-ld_get(kernel_bss_start));
	munit_assert_ulong(region->flags, ==, MM_VIRT_PROT_W);

	return MUNIT_OK;
}

static_assert(MM_VIRT_BSIZE_4KiB<MM_VIRT_BSIZE_2MiB,
              "4KiB should have a lower index than 2MiB");
static_assert(MM_VIRT_BSIZE_2MiB<MM_VIRT_BSIZE_1GiB,
              "2MiB should have a lower index than 1GiB");
static_assert(MM_VIRT_BSIZE_1GiB<MM_VIRT_BSIZE_512GiB,
              "1GiB should have a lower index than 512GiB");
              
static MunitResult test_mm_init_populates_ptables_luts_with_nx(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	cpuinfo_setsupport_nx(true);
	uintptr_t pml4[512] = {0};
	mm_virt_getpml4_fake.return_val = pml4;
	mm_virt_init_ptables(0);
	struct mm_virt_ptables *ptables = mm_virt_getptables();

	munit_assert(pml4[MM_RECURSIVE_ENTRY] & MM_VIRT_AMD64_FLAGS_NX);

	munit_assert_ulong(ptables->lut_lvl2size[MM_VIRT_BSIZE_4KiB],   ==, 1LU<<PT_STARTING_BIT);
	munit_assert_ulong(ptables->lut_lvl2size[MM_VIRT_BSIZE_2MiB],   ==, 1LU<<PD_STARTING_BIT);
	munit_assert_ulong(ptables->lut_lvl2size[MM_VIRT_BSIZE_1GiB],   ==, 1LU<<PDPT_STARTING_BIT);
	munit_assert_ulong(ptables->lut_lvl2size[MM_VIRT_BSIZE_512GiB], ==, 1LU<<PML4_STARTING_BIT);

	munit_assert_ulong(ptables->lut_prot2flags[MM_VIRT_PROT_X],    ==,
		MM_VIRT_AMD64_FLAGS_P);
	munit_assert_ulong(ptables->lut_prot2flags[MM_VIRT_PROT_U_X],  ==,
		MM_VIRT_AMD64_FLAGS_P | MM_VIRT_AMD64_FLAGS_US);
	munit_assert_ulong(ptables->lut_prot2flags[MM_VIRT_PROT_RO],   ==,
		MM_VIRT_AMD64_FLAGS_P | MM_VIRT_AMD64_FLAGS_NX);
	munit_assert_ulong(ptables->lut_prot2flags[MM_VIRT_PROT_U_RO], ==,
		MM_VIRT_AMD64_FLAGS_P | MM_VIRT_AMD64_FLAGS_NX | MM_VIRT_AMD64_FLAGS_US);
	munit_assert_ulong(ptables->lut_prot2flags[MM_VIRT_PROT_W],    ==,
		MM_VIRT_AMD64_FLAGS_P | MM_VIRT_AMD64_FLAGS_NX | MM_VIRT_AMD64_FLAGS_RW);
	munit_assert_ulong(ptables->lut_prot2flags[MM_VIRT_PROT_U_W],  ==,
		MM_VIRT_AMD64_FLAGS_P | MM_VIRT_AMD64_FLAGS_NX | MM_VIRT_AMD64_FLAGS_RW | MM_VIRT_AMD64_FLAGS_US);
	return MUNIT_OK;
}

static MunitResult test_mm_init_populates_ptables_luts_without_nx(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	cpuinfo_setsupport_nx(false);
	uintptr_t pml4[512] = {0};
	mm_virt_getpml4_fake.return_val = pml4;
	mm_virt_init_ptables(0);
	struct mm_virt_ptables *ptables = mm_virt_getptables();

	munit_assert(!(pml4[MM_RECURSIVE_ENTRY] & MM_VIRT_AMD64_FLAGS_NX));

	munit_assert_ulong(ptables->lut_lvl2size[MM_VIRT_BSIZE_4KiB],   ==, 1LU<<PT_STARTING_BIT);
	munit_assert_ulong(ptables->lut_lvl2size[MM_VIRT_BSIZE_2MiB],   ==, 1LU<<PD_STARTING_BIT);
	munit_assert_ulong(ptables->lut_lvl2size[MM_VIRT_BSIZE_1GiB],   ==, 1LU<<PDPT_STARTING_BIT);
	munit_assert_ulong(ptables->lut_lvl2size[MM_VIRT_BSIZE_512GiB], ==, 1LU<<PML4_STARTING_BIT);

	munit_assert_ulong(ptables->lut_prot2flags[MM_VIRT_PROT_X],    ==,
		MM_VIRT_AMD64_FLAGS_P);
	munit_assert_ulong(ptables->lut_prot2flags[MM_VIRT_PROT_RO],   ==,
		MM_VIRT_AMD64_FLAGS_P);
	munit_assert_ulong(ptables->lut_prot2flags[MM_VIRT_PROT_W],    ==,
		MM_VIRT_AMD64_FLAGS_P | MM_VIRT_AMD64_FLAGS_RW);
	munit_assert_ulong(ptables->lut_prot2flags[MM_VIRT_PROT_U_X],  ==,
		MM_VIRT_AMD64_FLAGS_P | MM_VIRT_AMD64_FLAGS_US);
	munit_assert_ulong(ptables->lut_prot2flags[MM_VIRT_PROT_U_RO], ==,
		MM_VIRT_AMD64_FLAGS_P | MM_VIRT_AMD64_FLAGS_US);
	munit_assert_ulong(ptables->lut_prot2flags[MM_VIRT_PROT_U_W],  ==,
		MM_VIRT_AMD64_FLAGS_P | MM_VIRT_AMD64_FLAGS_RW | MM_VIRT_AMD64_FLAGS_US);
	return MUNIT_OK;
}

//------------------------------------------------------------------------------
// Test runner
//------------------------------------------------------------------------------
static MunitTest test_suite_tests[] = {
	// Name, function, setup, teardown, options, parameters
	{"/mm_virt/init/ptables/layout",         test_mm_init_defines_ptables_region,            setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_virt/init/ptables/lut/with_nx",    test_mm_init_populates_ptables_luts_with_nx,    setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_virt/init/ptables/lut/without_nx", test_mm_init_populates_ptables_luts_without_nx, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/mm_virt/init/kernel/layout",          test_mm_init_defines_kernel_regions,            setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	// NULL ENTRY @ END
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
	"", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
