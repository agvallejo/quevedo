#ifndef QUEVEDO_ASMM_H
#define QUEVEDO_ASMM_H

#include <sys/queue.h>

#include <mm/vmm.h>

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdatomic.h>

struct asmm_entry
{
	LIST_ENTRY(asmm_entry) link;
	uintptr_t start;
	size_t octets;
};

LIST_HEAD(asmm_head, asmm_entry);

struct asmm_pool
{
	atomic_flag mutex;
	struct asmm_head head; /**< Pool data */
	enum vmm_region heap_region; /**< Which heap to use */
};

/**
 * Find a free gap in the address space
 *
 * @param head Pool to use
 * @param octets Size of the gap
 * @param[out] On output, allocated region
 * @return True if the allocation was successful
 */
bool asmm_alloc(struct asmm_pool *head, size_t octets, uintptr_t *vaddr_out);

/**
 * Reserve a memory gap for use
 *
 * Ensures it's not in use and won't allocate it
 *
 * @param head Pool to use
 * @param start  Address of the gap
 * @param octets Size of the gap
 */
void asmm_alloc_at(struct asmm_pool *head, uintptr_t start, size_t octets);

/**
 * Return a memory gap to the allocation pool
 *
 * @param head Pool to use
 * @param start  Address of the gap
 * @param octets Size of the gap
 */
void asmm_free(struct asmm_pool *head, uintptr_t start, size_t octets);

#endif /* QUEVEDO_ASMM_H */
