#include "ptmm_private.h"
#include "pmm.h"
#include "vmm.h"

#include <quevedo/panic.h>

#include <stdint.h>
#include <string.h>

static const struct
{
	pmm_addr set;
	pmm_addr clear;
} ptmm_lut[] =
{
	[VMM_CFG_RO] =
	{
		.set   = PTMM_FLAGS_NX|PTMM_FLAGS_P,
		.clear = PTMM_FLAGS_W|PTMM_FLAGS_U|PTMM_FLAGS_CD
	},
	[VMM_CFG_RW] =
	{
		.set   = PTMM_FLAGS_NX|PTMM_FLAGS_W|PTMM_FLAGS_P,
		.clear = PTMM_FLAGS_U|PTMM_FLAGS_CD
	},
	[VMM_CFG_RX] =
	{
		.set   = PTMM_FLAGS_P,
		.clear = PTMM_FLAGS_NX|PTMM_FLAGS_W|PTMM_FLAGS_U|PTMM_FLAGS_CD
	},
	[VMM_CFG_RW_CD] =
	{
		.set   = PTMM_FLAGS_CD|PTMM_FLAGS_NX|PTMM_FLAGS_W|PTMM_FLAGS_P,
		.clear = PTMM_FLAGS_U
	},
	[VMM_CFG_U_RO] =
	{
		.set   = PTMM_FLAGS_U|PTMM_FLAGS_NX|PTMM_FLAGS_P,
		.clear = PTMM_FLAGS_W|PTMM_FLAGS_CD
	},
	[VMM_CFG_U_RW] =
	{
		.set   = PTMM_FLAGS_U|PTMM_FLAGS_NX|PTMM_FLAGS_W|PTMM_FLAGS_P,
		.clear = PTMM_FLAGS_CD
	},
	[VMM_CFG_U_RX] =
	{
		.set   = PTMM_FLAGS_U|PTMM_FLAGS_P,
		.clear = PTMM_FLAGS_NX|PTMM_FLAGS_W|PTMM_FLAGS_CD
	},
	[VMM_CFG_U_RWX] =
	{
		/* To be used on inner pages only of the paging tables! */
		.set   = PTMM_FLAGS_U|PTMM_FLAGS_W|PTMM_FLAGS_P,
		.clear = PTMM_FLAGS_NX|PTMM_FLAGS_CD
	},
};

static void ptmm_fill(pmm_addr *paddr, enum vmm_cfg cfg)
{
	pmm_addr new_page = *paddr;

	kassert(PTMM_FLAGS_CFG_ALL, ==, ptmm_lut[cfg].clear ^ ptmm_lut[cfg].set);

	new_page &= ~ptmm_lut[cfg].clear;
	new_page |=  ptmm_lut[cfg].set;

	*paddr = new_page;
}

static void* ptmm_gettab(size_t lvl, uintptr_t vaddr)
{
	uintptr_t (*pt)[PTMM_NUM_ENTRIES]; /**< Generic page table */
	switch(lvl)
	{
	case 3:
		pt  = ptmm_getpml4();
		break;
	case 2:
		pt  = ptmm_getpdpt(vaddr);
		break;
	case 1:
		pt  = ptmm_getpd(vaddr);
		break;
	case 0:
		pt  = ptmm_getpt(vaddr);
		break;
	default:
		panic("Paging overflow", lvl);
	}
	return pt;
}

static pmm_addr *ptmm_get_pageline(size_t lvl, uintptr_t vaddr)
{
	uintptr_t (*pt)[PTMM_NUM_ENTRIES]; /**< Generic page table */
	size_t idx; /**< The index into the page table */
	switch(lvl)
	{
	case 3:
		pt  = ptmm_getpml4();
		idx = ptmm_getidx_pml4(vaddr);
		break;
	case 2:
		pt  = ptmm_getpdpt(vaddr);
		idx = ptmm_getidx_pdpt(vaddr);
		break;
	case 1:
		pt  = ptmm_getpd(vaddr);
		idx = ptmm_getidx_pd(vaddr);
		break;
	case 0:
		pt  = ptmm_getpt(vaddr);
		idx = ptmm_getidx_pt(vaddr);
		break;
	default:
		panic("Paging overflow", lvl);
	}
	return &((*pt)[idx]);
}

void ptmm_map(uintptr_t vaddr, pmm_addr paddr)
{
	pmm_addr *tabpage; /**< The table entry to modify */
	pmm_addr newpage; /**< Output parameter of pmm_alloc() */

	/* pt = PML4 */
	tabpage = ptmm_get_pageline(3, vaddr);
	if (!(PTMM_FLAGS_P & *tabpage))
	{
		kassert_bool(pmm_alloc(&newpage), vaddr);
		ptmm_fill(&newpage, VMM_CFG_U_RWX);
		*tabpage = newpage;
		ptmm_flushtlb();
		memset(ptmm_gettab(2, vaddr), 0, VMM_PAGESIZE);
		ptmm_flushtlb();
	}

	/* pt = PDPT */
	tabpage = ptmm_get_pageline(2, vaddr);
	if (!(PTMM_FLAGS_P & *tabpage))
	{
		kassert_bool(pmm_alloc(&newpage), vaddr);
		ptmm_fill(&newpage, VMM_CFG_U_RWX);
		*tabpage = newpage;
		ptmm_flushtlb();
		memset(ptmm_gettab(1, vaddr), 0, VMM_PAGESIZE);
		ptmm_flushtlb();
	}

	/* pt = PD */
	tabpage = ptmm_get_pageline(1, vaddr);
	if (!(PTMM_FLAGS_P & *tabpage))
	{
		kassert_bool(pmm_alloc(&newpage), vaddr);
		ptmm_fill(&newpage, VMM_CFG_U_RWX);
		*tabpage = newpage;
		ptmm_flushtlb();
		memset(ptmm_gettab(0, vaddr), 0, VMM_PAGESIZE);
		ptmm_flushtlb();
	}

	/* pt = (actual) PT */
	tabpage = ptmm_get_pageline(0, vaddr);

	/* No page must be mapped */
	kassert_false(*tabpage, &, 1);

	ptmm_fill(&paddr, VMM_CFG_RW);
	kassert_bool(pmm_alloc(&newpage), vaddr);
	*tabpage = paddr;
	ptmm_flushtlb();
}

pmm_addr ptmm_unmap(uintptr_t vaddr)
{
	pmm_addr *tabpage; /**< The table entry to access */

	/* pt = PML4 */
	tabpage = ptmm_get_pageline(3, vaddr);
	kassert(PTMM_FLAGS_P, &, *tabpage);

	/* pt = PDPT */
	tabpage = ptmm_get_pageline(2, vaddr);
	kassert(PTMM_FLAGS_P, &, *tabpage);

	/* pt = PD */
	tabpage = ptmm_get_pageline(1, vaddr);
	kassert(PTMM_FLAGS_P, &, *tabpage);

	/* pt = (actual) PT */
	tabpage = ptmm_get_pageline(0, vaddr);
	kassert(PTMM_FLAGS_P, &, *tabpage);

	pmm_addr ret = *tabpage;
	*tabpage = 0;
	ptmm_flushtlb();

	/* Get rid of all those annoying flags */
	ret &= ~PTMM_FLAGS_ALL;

	/* If the bit next to NX is set, sign extend NX */
	ret |= (ret&(PTMM_FLAGS_NX>>1))?PTMM_FLAGS_NX:0;

	return ret;
}

void ptmm_cfg(uintptr_t vaddr, enum vmm_cfg cfg)
{
	pmm_addr *tabpage; /**< The table entry to access */

	/* pt = PML4 */
	tabpage = ptmm_get_pageline(3, vaddr);
	kassert(PTMM_FLAGS_P, &, *tabpage);

	/* pt = PDPT */
	tabpage = ptmm_get_pageline(2, vaddr);
	kassert(PTMM_FLAGS_P, &, *tabpage);

	/* pt = PD */
	tabpage = ptmm_get_pageline(1, vaddr);
	kassert(PTMM_FLAGS_P, &, *tabpage);

	/* pt = (actual) PT */
	tabpage = ptmm_get_pageline(0, vaddr);
	kassert(PTMM_FLAGS_P, &, *tabpage);

	ptmm_fill(tabpage, cfg);
	ptmm_flushtlb();
}

void ptmm_create_address_space(uintptr_t virt_pml4, pmm_addr phy_pml4)
{
	/* Paranoid check. The global region must start in 511 */
	kassert(ptmm_getidx_pml4(VMM_REGION_PRIV_GLOBAL_MINADDR), ==, 511);

	uintptr_t (*current_pml4)[PTMM_NUM_ENTRIES] = ptmm_getpml4();
	uintptr_t (*new_pml4)[PTMM_NUM_ENTRIES] = (void*)virt_pml4;
	memset(new_pml4, 0, VMM_PAGESIZE);
	(*new_pml4)[511] = (*current_pml4)[511];
	(*new_pml4)[510] = phy_pml4;
	ptmm_fill(&(*new_pml4)[510], VMM_CFG_RW);
}
