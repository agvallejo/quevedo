#include "asmm.h"
#include "ptmm.h"
#include "pmm.h"
#include "vmm_private.h"

#include <quevedo/panic.h>
#include <quevedo/log.h>
#include <quevedo/ld.h>
#include <quevedo/hal.h>

#include <stdint.h>
#include <stdbool.h>

STATIC struct vmm_data vmm_data;

static struct asmm_pool *vmm_vaddr2pool(uintptr_t vaddr)
{
	struct asmm_pool *pool = vmm_getpool(vmm_getregion(vaddr));
	kassert_notnull(pool, vaddr);
	return pool;
}

void vmm_global_init(void)
{
	struct asmm_pool **pool = &vmm_getpool(VMM_REGION_PRIV_GLOBAL);
	*pool = &vmm_data.global_pool;
	LIST_INIT(&(*pool)->head);
	(*pool)->heap_region = VMM_REGION_PRIV_GLOBAL;

	asmm_free(*pool, VMM_REGION_PRIV_GLOBAL_MINADDR, VMM_REGION_PRIV_GLOBAL_SIZE);

	uintptr_t start;
	size_t octets;

	/* Remove the kernel chunks from asmm and configure permissions*/

	octets = ld_get(kernel_text_end)-ld_get(kernel_text_start);
	if (octets)
	{
		start = ld_get(kernel_text_start);
		asmm_alloc_at(*pool, start, octets);
		vmm_cfg(start, octets, VMM_CFG_RX);
	}

	octets = ld_get(kernel_data_end)-ld_get(kernel_data_start);
	if (octets)
	{
		start = ld_get(kernel_data_start);
		asmm_alloc_at(*pool, start, octets);
		vmm_cfg(start, octets, VMM_CFG_RW);
	}

	octets = ld_get(kernel_rodata_end)-ld_get(kernel_rodata_start);
	if (octets)
	{
		start = ld_get(kernel_rodata_start);
		asmm_alloc_at(*pool, start, octets);
		vmm_cfg(start, octets, VMM_CFG_RO);
	}

	octets = ld_get(kernel_bss_end)-ld_get(kernel_bss_start);
	if (octets)
	{
		start = ld_get(kernel_bss_start);
		asmm_alloc_at(*pool, start, octets);
		vmm_cfg(start, octets, VMM_CFG_RW);
	}
}

void vmm_local_init(void)
{
	pmm_addr paddr;
	kassert_bool(pmm_alloc(&paddr), 0);
	ptmm_map(VMM_REGION_PRIV_LOCAL_MINADDR, paddr);

	struct asmm_pool *pool;

	pool = (void*)VMM_REGION_PRIV_LOCAL_MINADDR;
	vmm_getpool(VMM_REGION_USER) = pool;
	LIST_INIT(&pool->head);
	pool->heap_region = VMM_REGION_PRIV_LOCAL;

	/* Free all memory of the privileged local area */
	asmm_free(pool, VMM_PAGESIZE, VMM_REGION_PRIV_LOCAL_MINADDR-VMM_PAGESIZE);

	pool++;
	vmm_getpool(VMM_REGION_PRIV_LOCAL) = pool;
	LIST_INIT(&pool->head);
	pool->heap_region = VMM_REGION_PRIV_LOCAL;

	/* Free all memory of the privileged local area */
	asmm_free(pool, VMM_REGION_PRIV_LOCAL_MINADDR, VMM_REGION_PRIV_LOCAL_SIZE);

	/* Allocate the currently used parts */
	asmm_alloc_at(pool, VMM_REGION_PRIV_LOCAL_MINADDR, VMM_PAGESIZE);
	asmm_alloc_at(pool, HMM_LOCAL_POOL_ADDR, HMM_POOLSIZE);


}

uintptr_t vmm_alloc(enum vmm_region region, size_t octets)
{
	kassert_false(octets, &, 0xFFF);
	kassert(octets, >, 0);

	uintptr_t vaddr;
	kassert_bool(asmm_alloc(vmm_getpool(region), octets, &vaddr), region);
	for (size_t offset = 0; offset!=octets; offset += VMM_PAGESIZE)
	{
		pmm_addr paddr;
		kassert_bool(pmm_alloc(&paddr), 0);
		ptmm_map(vaddr+offset, paddr);
	}
	return vaddr;
}

void vmm_alloc_at(uintptr_t vaddr, size_t octets)
{
	kassert_false(vaddr, &, 0xFFF);
	kassert_false(octets, &, 0xFFF);
	kassert(octets, >, 0);

	asmm_alloc_at(vmm_vaddr2pool(vaddr), vaddr, octets);
	for (size_t offset = 0; offset!=octets; offset += VMM_PAGESIZE)
	{
		pmm_addr paddr;
		kassert_bool(pmm_alloc(&paddr), 0);
		ptmm_map(vaddr+offset, paddr);
	}
}

void vmm_free(uintptr_t vaddr, size_t octets)
{
	kassert_false(octets, &, 0xFFF);
	kassert(octets, >, 0);
	for (size_t offset = 0; offset!=octets; offset += VMM_PAGESIZE)
	{
		pmm_addr paddr = ptmm_unmap(vaddr+offset);
		pmm_free(paddr);
	}
	asmm_free(vmm_vaddr2pool(vaddr), vaddr, octets);
}

bool vmm_map(enum vmm_region region, pmm_addr paddr, size_t octets, uintptr_t *vaddr)
{
	kassert_false(octets, %, VMM_PAGESIZE);
	kassert(octets, !=, 0);
	if (!asmm_alloc(vmm_getpool(region), octets, vaddr))
	{
		panic("Map failed", paddr);
		return false;
	}
	for (size_t offset=0; offset != octets; offset+=VMM_PAGESIZE)
	{
		ptmm_map(offset+*vaddr, offset+paddr);
	}
	return true;
}

void vmm_map_at(uintptr_t vaddr, pmm_addr paddr)
{
	asmm_alloc_at(vmm_vaddr2pool(vaddr), vaddr, VMM_PAGESIZE);
	ptmm_map(vaddr, paddr);
}

void vmm_unmap(uintptr_t vaddr, pmm_addr expected_paddr)
{
	pmm_addr actual_paddr = ptmm_unmap(vaddr);
	kassert(actual_paddr, ==, expected_paddr);
	asmm_free(vmm_vaddr2pool(vaddr), vaddr, VMM_PAGESIZE);
}

void vmm_cfg(uintptr_t vaddr, size_t octets, enum vmm_cfg cfg)
{
	for (size_t offset = 0; offset!=octets; offset += VMM_PAGESIZE)
	{
		ptmm_cfg(vaddr+offset, cfg);
	}
}

pmm_addr vmm_create_address_space(void)
{
	pmm_addr paddr_pml4;
	kassert_bool(pmm_alloc(&paddr_pml4), 0);

	uintptr_t vaddr_pml4;
	vmm_map(VMM_REGION_PRIV_GLOBAL, paddr_pml4, VMM_PAGESIZE, &vaddr_pml4);

	ptmm_create_address_space(vaddr_pml4, paddr_pml4);
	vmm_unmap(vaddr_pml4, paddr_pml4);

	return paddr_pml4;
}

void vmm_destroy_address_space(pmm_addr pml4)
{
	(void)pml4;
	/* Unimplemented */
	kassert_unreachable();
}
