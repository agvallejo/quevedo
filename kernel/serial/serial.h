#ifndef QUEVEDO_SERIAL_H
#define QUEVEDO_SERIAL_H

#include <stddef.h>
#include <stdint.h>

enum serial_com {
	SERIAL_COM_MIN = 0, // Only used for bounds check
	SERIAL_COM1    = 1,
	SERIAL_COM2    = 2,
	SERIAL_COM3    = 3,
	SERIAL_COM4    = 4,
	SERIAL_COM_MAX = 5  // Only used for bounds check
};

//------------------------------------------------------------------------------
// Parity
//------------------------------------------------------------------------------
enum serial_parity {
	SERIAL_PARITY_MIN  = 0, // Only used for bounds check
	SERIAL_PARITY_NONE = 1,
	SERIAL_PARITY_EVEN = 2,
	SERIAL_PARITY_ODD  = 3,
	SERIAL_PARITY_MAX  = 4  // Only used for bounds check
};

/**
 * Configure 'com' with a specific parity
 *
 * @param com    Port to configure
 * @param parity Desired parity
 */
void serial_setparity(enum serial_com com, enum serial_parity parity);

//------------------------------------------------------------------------------
// Baud rate
//------------------------------------------------------------------------------
#define SERIAL_RATE_MAX  (115200U)

/**
 * Configure 'com' for a specific baud rate
 *
 * @param com       Port to configure
 * @param baud_rate Desired rate in bps
 */
void serial_setrate(enum serial_com com, uint32_t rate);

//------------------------------------------------------------------------------
// Word Length
//------------------------------------------------------------------------------
/**
 * Configure 'com' for a specific word length
 *
 * @param com    Port to configure
 * @param size_t Bits/word
 */
void serial_setwordlen(enum serial_com com, size_t wordlen);

//------------------------------------------------------------------------------
// Stop Bits
//------------------------------------------------------------------------------
enum serial_stopbit{
	SERIAL_STOPBIT_MIN     = 0, // Only used for bounds check
	SERIAL_STOPBIT_1BIT    = 1, // Match 1 stopbit  with '1'
	SERIAL_STOPBIT_2BITS   = 2, // Match 2 stopbits with '2'
	SERIAL_STOPBIT_1_5BITS = 3,
	SERIAL_STOPBIT_MAX     = 4  // Only used for bounds check
};
/**
 * Configure 'com' for a specific number of stop bits
 *
 * @param com    Port to configure
 * @param size_t Number of stopbits
 */
void serial_setstopbits(enum serial_com com, enum serial_stopbit stopbit);

//------------------------------------------------------------------------------
// Initialisation
//------------------------------------------------------------------------------
/**
 * Initialise the 'com' port
 *
 * Configures buffers, flow control, etc
 *
 * @param com Port to initialise
 */
void serial_init(
	enum serial_com com,
	uint32_t rate,
	size_t wordlen,
	enum serial_parity parity,
	enum serial_stopbit stopbit);

//------------------------------------------------------------------------------
// Char I/O
//------------------------------------------------------------------------------
/**
 * Send a character via serial port
 *
 * @param com COM port to use
 * @param c   Character to send
 */
void serial_putc(enum serial_com com, char c);

/**
 * Receive a character via serial port
 *
 * @param com COM port to use
 * @return    Received character
 */
char serial_getc(enum serial_com com);

/**
 * Deactivate 'com' and return it to a known state
 *
 * Disable interrupts and reconfigure buffers
 *
 * @parm com COM port to use
 */
void serial_reset(enum serial_com com);

#endif /* QUEVEDO_SERIAL_H */
