#include <quevedo/panic.h>

#include <isr/isr.h>

#include <stddef.h>

#include "serial_private.h"
#include "serial.h"

void serial_putc(enum serial_com com, char c)
{
	serial_set_reg(com, SERIAL_THR, (uint8_t)c);
}

char serial_getc(enum serial_com com)
{
	return serial_get_reg(com, SERIAL_RBR);
}

void serial_pc_enable_interrupt(enum serial_com com, enum serial_ier_bits bit)
{
	isr_mask_irq(serial_getirq(com), ISR_IRQ_UNMASKED);
	uint8_t ier = serial_get_reg(com, SERIAL_IER);
	serial_set_reg(com, SERIAL_IER, ier | (1<<bit));
}

void serial_pc_disable_interrupt(enum serial_com com, enum serial_ier_bits bit)
{
	uint8_t ier = serial_get_reg(com, SERIAL_IER);
	serial_set_reg(com, SERIAL_IER, ier & ~(1<<bit));
}

void serial_setrate(enum serial_com com, uint32_t baud_rate)
{
	kassert(SERIAL_RATE_MAX, >=, baud_rate);

	uint16_t div = SERIAL_RATE_MAX/baud_rate;
	kassert(SERIAL_RATE_MAX/div, ==, baud_rate);

	uint8_t lcr = serial_get_reg(com, SERIAL_LCR);

	serial_set_reg(com, SERIAL_LCR, lcr | (1<<SERIAL_LCR_DLAB));
	serial_set_reg(com, SERIAL_DLL, div&0xFF);
	serial_set_reg(com, SERIAL_DLM, div>>8);
	serial_set_reg(com, SERIAL_LCR, lcr);
}

void serial_setwordlen(enum serial_com com, size_t len)
{
	/* Unsupported wordlen? */
	kassert_true((len>=5) && (len<=8), len);
	uint8_t lcr = serial_get_reg(com, SERIAL_LCR);
	lcr &= ~(SERIAL_LCR_LEN_MASK<<SERIAL_LCR_LEN_LSB);
	serial_set_reg(com, SERIAL_LCR, lcr | ((len-5)<<SERIAL_LCR_LEN_LSB));
}

void serial_setparity(enum serial_com com, enum serial_parity parity)
{
	uint8_t lcr = serial_get_reg(com, SERIAL_LCR);
	lcr &= ~(SERIAL_LCR_PARITY_MASK<<SERIAL_LCR_PARITY_LSB);
	switch(parity) {
	case SERIAL_PARITY_NONE:
		lcr |= SERIAL_LCR_PARITY_NONE<<SERIAL_LCR_PARITY_LSB;
		break;
	case SERIAL_PARITY_EVEN:
		lcr |= SERIAL_LCR_PARITY_EVEN<<SERIAL_LCR_PARITY_LSB;
		break;
	case SERIAL_PARITY_ODD:
		lcr |= SERIAL_LCR_PARITY_ODD<<SERIAL_LCR_PARITY_LSB;
		break;
	default:
		panic("Nonsensical parity value", parity);
		return;
	};
	serial_set_reg(com, SERIAL_LCR, lcr);
}

void serial_setstopbit(enum serial_com com, enum serial_stopbit stopbit)
{
	/* Invalid stopbit cfg? */
	kassert_true((stopbit>SERIAL_STOPBIT_MIN) && (stopbit<SERIAL_STOPBIT_MAX),
		stopbit);

	uint8_t lcr = serial_get_reg(com, SERIAL_LCR);
	switch(stopbit) {
	case SERIAL_STOPBIT_1BIT:
		lcr &= ~(1<<SERIAL_LCR_2STOPBITS);
		break;
	case SERIAL_STOPBIT_1_5BITS:
	case SERIAL_STOPBIT_2BITS:
		lcr |= 1<<SERIAL_LCR_2STOPBITS;
		break;
	default:
		panic("Unsupported wordlen", stopbit);
		return;
	}
	serial_set_reg(com, SERIAL_LCR, lcr);
}

void serial_init(
	enum serial_com com,
	uint32_t rate,
	size_t wordlen,
	enum serial_parity parity,
	enum serial_stopbit stopbit)
{
	serial_set_reg(com, SERIAL_IER, 0); // Disable all interrupts
	serial_setrate(com, rate);
	serial_setwordlen(com, wordlen);
	serial_setparity(com, parity);
	serial_setstopbit(com, stopbit);
}
