#ifndef SERIAL_PRIVATE_H
#define SERIAL_PRIVATE_H

#include "serial.h"

#include <quevedo/hal.h>

#include <isr/isr.h>

/** Addressed with 'enum serial_com' */
static const struct {
	uint8_t  irq;
	uint16_t port;
} serial_params[] = {
	{.irq = ISR_IRQ_COM1, .port=0x3F8},
	{.irq = ISR_IRQ_COM2, .port=0x2F8},
	{.irq = ISR_IRQ_COM1, .port=0x3E8},
	{.irq = ISR_IRQ_COM2, .port=0x2E8}
};
#define serial_getirq(x) (serial_params[(x)].irq)
#define serial_getport(x) (serial_params[(x)].port)

/** Registers as offsets of the IO port */
enum serial_reg {
	SERIAL_RBR = 0, // Receive Buffer           (RO,  DLAB=0)
	SERIAL_THR = 0, // Transmitter Holding      (WO,  DLAB=0)
	SERIAL_DLL = 0, // Divisor Latch (LSB)      (R/W, DLAB=1)
	SERIAL_DLM = 1, // Divisor Latch (MSB)      (R/W, DLAB=1)
	SERIAL_IER = 1, // Interrupt Enable         (R/W, DLAB=X)
	SERIAL_IIR = 2, // Interrupt Identification (RO,  DLAB=X)
	SERIAL_FCR = 2, // FIFO  Control            (WO,  DLAB=X)
	SERIAL_LCR = 3, // Line  Control            (R/W, DLAB=X)
	SERIAL_MCR = 4, // Modem Control            (R/W, DLAB=X)
	SERIAL_LSR = 5, // Line  Status             (RO,  DLAB=X)
	SERIAL_MSR = 6, // Modem Status             (RO,  DLAB=X)
};

/** Bitfield of INT_EN */
enum serial_ier_bits {
	SERIAL_IER_RX_AVAIL       = 0,
	SERIAL_IER_TX_UNDERFLOW   = 1,
	SERIAL_IER_RX_LINE_CHANGE = 2,
	SERIAL_IER_MODEM          = 3,
	SERIAL_IER_SLEEP          = 4,
	SERIAL_IER_LOW_POWER      = 5,
	SERIAL_IER_RESERVED6      = 6,
	SERIAL_IER_RESERVED7      = 7
};

/** Bitfield of INT_ID */
enum serial_iir_bits {
	SERIAL_IIR_INT_PENDING = 0,
	SERIAL_IIR_REASON_LSB  = 1,
	SERIAL_IIR_RESERVED4   = 4,
	SERIAL_IIR_64BYTE_FIFO = 5,
	SERIAL_IIR_FIFO_ID_LSB = 6,
};

enum serial_iir_reason {
	SERIAL_IIR_REASON_MODEM          = 0,
	SERIAL_IIR_REASON_TX_UNDERFLOW   = 1,
	SERIAL_IIR_REASON_RX_AVAIL       = 2,
	SERIAL_IIR_REASON_RX_LINE_CHANGE = 3,
	SERIAL_IIR_REASON_RESERVED4      = 4,
	SERIAL_IIR_REASON_RESERVED5      = 5,
	SERIAL_IIR_REASON_TIMEOUT        = 6,
	SERIAL_IIR_REASON_RESERVED7      = 7
};

enum serial_iir_fifo {
	SERIAL_IIR_FIFO_NO_FIFO  = 0,
	SERIAL_IIR_FIFO_RESERVED = 1,
	SERIAL_IIR_FIFO_BROKEN   = 2,
	SERIAL_IIR_FIFO_WORKING  = 3
};

enum serial_lcr {
	SERIAL_LCR_LEN_LSB     = 0,
	SERIAL_LCR_1_5STOPBITS = 2, //If 5bit words
	SERIAL_LCR_2STOPBITS   = 2, //If 6-8bit words
	SERIAL_LCR_PARITY_LSB  = 3,
	SERIAL_LCR_BREAK       = 6,
	SERIAL_LCR_DLAB        = 7
};

enum serial_lcr_len {
	SERIAL_LCR_LEN_5BITS = 0,
	SERIAL_LCR_LEN_6BITS = 1,
	SERIAL_LCR_LEN_7BITS = 2,
	SERIAL_LCR_LEN_8BITS = 3,
	SERIAL_LCR_LEN_MASK  = 3,
	SERIAL_LCR_LEN_MAX
};


enum serial_lcr_parity {
	SERIAL_LCR_PARITY_NONE  = 0, // Actually any value %2
	SERIAL_LCR_PARITY_ODD   = 1,
	SERIAL_LCR_PARITY_EVEN  = 3,
	SERIAL_LCR_PARITY_HIGH  = 5,
	SERIAL_LCR_PARITY_LOW   = 7,
	SERIAL_LCR_PARITY_MASK  = 7
};

enum serial_fcr {
	SERIAL_FCR_ENABLE   = 0,
	SERIAL_FCR_CLEAR_RX = 1,
	SERIAL_FCR_CLEAR_TX = 2,
	SERIAL_FCR_DMA      = 3,
	SERIAL_FCR_RESERVED = 4,
	SERIAL_FCR_64BIT    = 5,
	SERIAL_FCR_TRIG_LSB = 6
};

enum serial_fcr_thr {
	SERIAL_FCR_THR_1BYTE   = 0,
	SERIAL_FCR_THR_4BYTES  = 1,
	SERIAL_FCR_THR_8BYTES  = 2,
	SERIAL_FCR_THR_14BYTES = 3
};

// Register accesors
#ifdef UNIT_TESTING
uint8_t serial_get_reg(enum serial_com com, enum serial_reg reg);
void serial_set_reg(enum serial_com com, enum serial_reg reg, uint8_t val);
#else
static inline uint8_t serial_get_reg(enum serial_com com, enum serial_reg reg)
{
	return inb(serial_getport(com-1)+reg);
}

static inline void serial_set_reg(enum serial_com com, enum serial_reg reg, uint8_t val)
{
	outb(serial_getport(com-1)+reg, val);
}
#endif // UNIT_TESTING

#endif // SERIAL_PRIVATE_H
