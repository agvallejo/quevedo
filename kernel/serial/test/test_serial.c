#include <fff.h>
#include <munit.h>
#include <stdio.h>

#include <serial/serial.h>
#include <isr/isr.h>

#include "../serial_private.h"

DEFINE_FFF_GLOBALS

FAKE_VOID_FUNC(isr_mask_irq, enum isr_irq, enum isr_mask)

struct {
	uint8_t reg_array[SERIAL_MSR+1];
	uint8_t dll;
	uint8_t dlm;
} test_port[4];

static uint8_t *serial_get_reg_ptr(enum serial_com com, enum serial_reg reg)
{
	bool dlab = test_port[com].reg_array[SERIAL_LCR] & (1<<SERIAL_LCR_DLAB);
	uint8_t *reg_ptr = &test_port[com].reg_array[reg];
	if (!dlab)
		return reg_ptr;
	if (reg == SERIAL_DLL)
		return &test_port[com].dll;
	if (reg == SERIAL_DLL)
		return &test_port[com].dlm;
	return reg_ptr;
}

uint8_t serial_get_reg(enum serial_com com, enum serial_reg reg)
{
	return *serial_get_reg_ptr(com, reg);
}

void serial_set_reg(enum serial_com com, enum serial_reg reg, uint8_t val)
{
	uint8_t *reg_ptr = serial_get_reg_ptr(com, reg);
	*reg_ptr = val;
}

//------------------------------------------------------------------------------
// Setup/Teardown
//------------------------------------------------------------------------------
static void *setup(const MunitParameter params[], void* data)
{
	(void) params;

	FFF_RESET_HISTORY();
	RESET_FAKE(isr_mask_irq);
	memset(test_port, 0, sizeof test_port);

	return data;
}


//------------------------------------------------------------------------------
// Tests for the fake register API
//------------------------------------------------------------------------------
static MunitResult test_double_sets_reg(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	enum serial_com test_com = SERIAL_COM1;
	uint8_t magic = 0x34U;

	// Set magic value
	serial_set_reg(test_com, SERIAL_DLL, magic);
	munit_assert_uint(test_port[test_com].reg_array[SERIAL_DLL], ==, magic);
	munit_assert_uint(serial_get_reg(test_com, SERIAL_DLL), ==, magic);

	// No DLAB, so the shadow register is still zero
	munit_assert_uint(test_port[test_com].dll, ==, 0);

	return MUNIT_OK;
}

static MunitResult test_double_sets_dll(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	enum serial_com test_com = SERIAL_COM1;
	uint8_t dlab     = 1<<SERIAL_LCR_DLAB;
	uint8_t magic    = 0x34U;

	// Enable access to the DLL register
	serial_set_reg(test_com, SERIAL_LCR, dlab);

	// Set it to the magic value
	serial_set_reg(test_com, SERIAL_DLL, magic);

	// Check the register was set and it's shadow counterpart wasn't
	munit_assert_uint(test_port[test_com].dll, ==, magic);
	munit_assert_uint(serial_get_reg(test_com, SERIAL_DLL), ==, magic);
	munit_assert_uint(test_port[test_com].reg_array[SERIAL_DLL], ==, 0);

	// After getting rid of the DLAB the DLL register gets shadowed
	serial_set_reg(test_com, SERIAL_LCR, 0);
	munit_assert_uint(serial_get_reg(test_com, SERIAL_DLL), ==, 0);

	return MUNIT_OK;
}

//------------------------------------------------------------------------------
// Tests for the serial module itself
//------------------------------------------------------------------------------
static MunitResult test_init_sets_baud_rate(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	uint16_t test_div = 0x21;
	size_t test_com = SERIAL_COM1;
	serial_init(test_com, 115200/test_div, 8, SERIAL_PARITY_NONE, 1);

	munit_assert_uint(test_port[test_com].dll, ==, test_div & 0xFF);
	munit_assert_uint(test_port[test_com].dlm, ==, test_div >>8);

	return MUNIT_OK;
}

static void assert_field(enum serial_com com, enum serial_reg reg, uint8_t offset, uint8_t mask, uint8_t val)
{
	uint8_t reg_val = test_port[com].reg_array[reg];
	reg_val = (reg_val>>offset)&mask;
	munit_assert_uint(reg_val, ==, val);
}

static MunitResult test_init_sets_parity(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	size_t test_com = SERIAL_COM1;
	serial_init(test_com, 115200, 8, SERIAL_PARITY_EVEN, 1);

	assert_field(test_com, SERIAL_LCR, SERIAL_LCR_PARITY_LSB,
			SERIAL_LCR_PARITY_MASK, SERIAL_LCR_PARITY_EVEN);

	return MUNIT_OK;
}

static MunitResult test_init_sets_wordlen(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	size_t test_com = SERIAL_COM1;
	int test_wordlen = 8;
	serial_init(test_com, 115200, test_wordlen, SERIAL_PARITY_NONE, 1);

	assert_field(test_com, SERIAL_LCR, SERIAL_LCR_LEN_LSB,
			SERIAL_LCR_LEN_MASK, test_wordlen-5);
	
	return MUNIT_OK;
}

static MunitResult test_init_sets_stopbit(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	size_t test_com = SERIAL_COM1;
	int test_stopbit = 2;
	serial_init(test_com, 115200, 8, SERIAL_PARITY_NONE, test_stopbit);

	assert_field(test_com, SERIAL_LCR, SERIAL_LCR_2STOPBITS, 1, 1);
	
	return MUNIT_OK;
}

static MunitResult test_setwordlen_can_be_called_twice(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	size_t test_com     = SERIAL_COM1;
	size_t test_wordlen = 7;
	serial_setwordlen(test_com, test_wordlen);
	assert_field(test_com, SERIAL_LCR, SERIAL_LCR_LEN_LSB,
			SERIAL_LCR_LEN_MASK, test_wordlen-5);

	test_wordlen = 5;
	serial_setwordlen(test_com, test_wordlen);
	assert_field(test_com, SERIAL_LCR, SERIAL_LCR_LEN_LSB,
			SERIAL_LCR_LEN_MASK, test_wordlen-5);
	
	return MUNIT_OK;
}

static MunitResult test_setparity_can_be_called_twice(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	size_t test_com     = SERIAL_COM1;
	serial_setparity(test_com, SERIAL_PARITY_EVEN);
	assert_field(test_com, SERIAL_LCR, SERIAL_LCR_PARITY_LSB,
			SERIAL_LCR_PARITY_MASK, SERIAL_LCR_PARITY_EVEN);

	serial_setparity(test_com, SERIAL_PARITY_ODD);
	assert_field(test_com, SERIAL_LCR, SERIAL_LCR_PARITY_LSB,
			SERIAL_LCR_PARITY_MASK, SERIAL_LCR_PARITY_ODD);
	
	return MUNIT_OK;
}

static MunitResult test_io(const MunitParameter params[], void* data)
{
	(void) params; (void) data;

	int test_com = SERIAL_COM1;
	char test_char = 'a';
	serial_putc(test_com, test_char);

	// RBR and THR share address, they are different on real HW
	// but this dumb double model just does a loopback
	munit_assert_uint(test_port[test_com].reg_array[SERIAL_THR], ==, test_char);
	munit_assert_char(serial_getc(test_com), ==, test_char);

	return MUNIT_OK;
}

//------------------------------------------------------------------------------
// Test runner
//------------------------------------------------------------------------------
static MunitTest test_suite_tests[] = {
	// Name, function, setup, teardown, options, parameters
	{"/serial/double/sets_reg", test_double_sets_reg, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/serial/double/sets_dll", test_double_sets_dll, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/serial/init/sets_baud_rate", test_init_sets_baud_rate, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/serial/init/sets_parity", test_init_sets_parity, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/serial/init/sets_wordlen", test_init_sets_wordlen, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/serial/init/sets_stopbit", test_init_sets_stopbit, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/serial/wordlen/can_be_called_twice", test_setwordlen_can_be_called_twice, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/serial/parity/can_be_called_twice", test_setparity_can_be_called_twice, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/serial/io/putc_and_getc_operate_fine", test_io, setup, NULL, MUNIT_TEST_OPTION_NONE, NULL},

	// NULL ENTRY @ END
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

static const MunitSuite test_suite = {
	"", test_suite_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE
};

int main(int argc, char* argv[MUNIT_ARRAY_PARAM(argc + 1)])
{
	return munit_suite_main(&test_suite, (void*) "µnit", argc, argv);
}
