#ifndef QUEVEDO_FS_H
#define QUEVEDO_FS_H

struct hbt;
void fs_init(struct hbt *hbt);

/**
 * Lay the image of 'filename' in memory
 *
 * Can only be used on empty address spaces
 *
 * @param filename Name of the image
 */
struct fs_file;
struct fs_file *fs_find(char *filename);

/**
 * Load 'file' into memory
 *
 * @param file Pointer to the file to load
 */
struct fs_file;
void fs_exec(struct fs_file *file);

#endif /* QUEVEDO_FS_H */
