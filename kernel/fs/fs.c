#include "fs_private.h"

#include <quevedo/boot.h>
#include <quevedo/elf.h>
#include <quevedo/panic.h>

#include <mm/hmm.h>
#include <mm/pmm.h>
#include <mp/mp.h>

#include <stddef.h>
#include <string.h>

struct fs_data fs_data;

static void fs_file_install(char *filename, pmm_addr start, size_t octets)
{
	struct fs_file *file = hmm_alloc(VMM_REGION_PRIV_GLOBAL, sizeof *file);
	char *allocated_filename = hmm_alloc(VMM_REGION_PRIV_GLOBAL, strlen(filename)+1);
	kassert_bool(vmm_map(VMM_REGION_PRIV_GLOBAL, start, octets, &file->start), 0);
	strcpy(allocated_filename, filename);
	file->name = allocated_filename;
	file->octets = octets;

	mp_lock(&fs_data.mutex);
	LIST_INSERT_HEAD(&fs_data.head, file, link);
	mp_unlock(&fs_data.mutex);
}

void fs_init(struct hbt *hbt)
{
	LIST_INIT(&fs_data.head);

	pmm_addr start = hbt->initrd.init.start;
	size_t octets = hbt->initrd.init.octets;
	fs_file_install("init", start, octets);
}

struct fs_file *fs_find(char *filename)
{
	struct fs_file *entry;
	mp_lock(&fs_data.mutex);
	LIST_FOREACH(entry, &fs_data.head, link)
	{
		if (!strcmp(filename, entry->name))
		{
			mp_unlock(&fs_data.mutex);
			return entry;
		}
	}
	kassert_unreachable();
	mp_unlock(&fs_data.mutex);
}

void fs_exec(struct fs_file *file)
{
	elf_load((void*)file->start);

	kassert_unreachable();
}
