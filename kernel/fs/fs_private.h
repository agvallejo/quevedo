#ifndef QUEVEDO_FS_PRIVATE_H
#define QUEVEDO_FS_PRIVATE_H

#include "fs.h"

#include <sys/queue.h>

#include <stdatomic.h>
#include <stdint.h>
#include <stddef.h>

struct fs_file
{
	LIST_ENTRY(fs_file) link;
	char *name;
	uintptr_t start;
	size_t octets;
};

LIST_HEAD(fs_head, fs_file);

struct fs_data
{
	atomic_flag mutex;
	struct fs_head head;
};

#endif /* QUEVEDO_FS_PRIVATE_H */
