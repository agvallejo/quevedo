#ifndef QUEVEDO_KMAIN_H
#define QUEVEDO_KMAIN_H

NORETURN void kmain(struct hbt *hbt);
NORETURN void kmain_ap(void);

#endif /* QUEVEDO_KMAIN_H */
