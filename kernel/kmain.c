#include <quevedo/log.h>

#include <quevedo/boot.h>
#include <quevedo/cfg.h>
#include <quevedo/hal.h>
#include <quevedo/panic.h>

#include <fb/fb.h>
#include <fs/fs.h>
#include <hal/hal.h>
#include <mm/mm_init.h>
#include <mp/mp.h>
#include <time/time.h>
#include <tsch/tsch.h>

#include "kmain.h"

/* Stack used by the BSP during boot */
const uint8_t kmain_stack[TSCH_STACK_SIZE] __attribute__((aligned(16)));

NORETURN void kmain(struct hbt *hbt)
{
	/* The HBT provides a preliminary framebuffer mapped in lower memory
	 * for use during the early stages of kernel bootstrap. It's not
	 * supposed to live long, but it eases debugging of early memory
	 * management issues.
	 *
	 * No debug message as there's no fb yet, Duh!
	 */
	fb_pre_init(hbt);

	/* Initialises all memory managers in the right order. After this point
	 * the global heap and virtual managers (hmm and vmm) are fully usable
	 */
	log_preboot("mm_global_init");
	mm_global_init(hbt);

	/* Initialise the platform specific shenanigans (GDT and IDT) */
	log_preboot("hal_init");
	hal_init(hbt);

	/* With memory management in place we can now map the framebuffer in
	 * higher memory. At the end of bootstrap nothing can remain in lower
	 * memory
	 */
	log_preboot("fb_init");
	fb_init(hbt);

	/* Initialise the time manager and HW timers (HPET and LAPIC timer) */
	log_preboot("time_imit");
	time_init(hbt);

	/* Start the in-kernel filesystem */
	log_preboot("fs_init");
	fs_init(hbt);

	/* Wake the other cores */
	log_preboot("mp_init");
	mp_init(hbt);

	/* Fire up the scheduler for the BSP */
	log_debug("tsch_init");
	tsch_init();

	/* This point is unreachable and the core should be in the idle thread */
	kassert_unreachable();
}

NORETURN void kmain_ap(void)
{
	/* Let the BSP know we're alive and kicking */
	log_preboot("mp_ap_init");
	mp_ap_init();

	/* Initialise the platform specific shenanigans (GDT and IDT) */
	log_preboot("hal_ap_init");
	hal_ap_init();

	/* Initialise the LAPIC timer */
	log_debug("time_ap_init");
	time_ap_init();

	/* Fire up the scheduler for the AP */
	log_debug("tsch_init");
	tsch_init();

	/* This point is unreachable and the core should be in the idle thread */
	kassert_unreachable();
}
