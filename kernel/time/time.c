#include <sys/queue.h>

#include <quevedo/panic.h>
#include <mm/hmm.h>

#include "time_private.h"
#include "hpet.h"

STATIC struct time_data time_data;

void time_init(struct hbt *hbt)
{
	/* Initialise the HPET (if present) */
	hpet_init(hbt);

	time_data.ref_clk = hpet_getclk();
	kassert(time_data.ref_clk, !=, NULL);

	/* TODO: Add support for the LAPIC timer */
}

void time_ap_init(void)
{
	/* TODO: Add support for the LAPIC timer */
}

time_a time_gettime(void)
{
	kassert_bool(time_data.ref_clk, 0);
	return time_data.ref_clk->gettime();
}

void time_sleep_until(time_a target)
{
	struct time_dline *dline = hmm_alloc(VMM_REGION_PRIV_GLOBAL, sizeof *dline);
	dline->cb     = time_sleep_cb;
	dline->target = target;

	time_dline_add(time_data.ref_clk, dline);
}

/*******************************************************************************
 * Interface to the clocks (pit, hpet, apic...)
 ******************************************************************************/
void time_tick(struct time_clk *clk)
{
	struct dline_head *head = &clk->head;
	struct time_dline *dline_tmp;
	struct time_dline *dline = LIST_FIRST(head);
	kassert_notnull(dline, 0);
	kassert(dline->target, < ,time_gettime()); /* At least a dline should be removed */
	LIST_FOREACH_SAFE(dline, head, link, dline_tmp)
	{
		if (time_op(time_gettime(), >, dline->target))
			break;

		dline->cb(dline->arg);
		LIST_REMOVE(dline, link);
		hmm_free(dline, sizeof *dline);
	}

	dline = LIST_FIRST(head);
	if (dline)
		time_data.ref_clk->cfg_oneshot(dline->target);
}

STATIC void time_dline_add(struct time_clk *clk, struct time_dline *new_dline)
{
	struct dline_head *head = &clk->head;
	if (LIST_EMPTY(head) || dline_op(LIST_FIRST(head), >=, new_dline))
	{
		LIST_INSERT_HEAD(head, new_dline, link);
		clk->cfg_oneshot(new_dline->target);
	}

	struct time_dline *dline;
	LIST_FOREACH(dline, head, link)
	{
		if (dline_op(dline, >=, new_dline))
		{
			LIST_INSERT_BEFORE(dline, new_dline, link);
			break;
		}
	}
}

STATIC void time_sleep_cb(void *arg)
{
	panic("Unimplemented", (uintptr_t)arg);
	//proc_unblock((pid_t)arg);
}
