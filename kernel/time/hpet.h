#ifndef QUEVEDO_HPET_H
#define QUEVEDO_HPET_H

#include "time/clock.h"

struct hbt;
void hpet_init(struct hbt *hbt);

struct time_clk *hpet_getclk(void);

#endif /* QUEVEDO_HPET_H */
