#ifndef QUEVEDO_HPET_PRIVATE_H
#define QUEVEDO_HPET_PRIVATE_H

#include "hpet.h"
#include "clock.h"

struct hpet_data{
	struct time_clk clk;
	uintptr_t regs;
	uint8_t ioapic_entry;
};

/* Timer we always use */
#define HPET_DEFAULT_TIMER 1u

#define hpet_get_gen_caps_id()     (*(volatile uint64_t*)(hpet_data.regs+0x00u))
#ifdef UNIT_TESTING
#define hpet_set_gen_caps_id(val) do {(*(volatile uint64_t*)(hpet_data.regs+0x00u)) = (val);}while(0)
#endif /* UNIT_TESTING */

#define hpet_get_gen_cfg()        (*(volatile uint64_t*)(hpet_data.regs+0x10u))
#define hpet_set_gen_cfg(val) do {(*(volatile uint64_t*)(hpet_data.regs+0x10u)) = (val);}while(0)

/** Main counter */
#define hpet_get_gen_int_status()        (*(volatile uint64_t*)(hpet_data.regs+0x20u))
#define hpet_set_gen_int_status(val) do{(*(volatile uint64_t*)(hpet_data.regs+0x20u)) = (val);}while(0)

/** Main counter */
#define hpet_get_main_cnt()        (*(volatile uint64_t*)(hpet_data.regs+0xF0u))
#define hpet_set_main_cnt(val) do {(*(volatile uint64_t*)(hpet_data.regs+0xF0u)) = (val);}while(0)

/** Configure each individual timer */
#define hpet_get_tmrN_cfg_cap(N)          (*(volatile uint64_t*)(hpet_data.regs+0x100u+0x20u*(N)))
#define hpet_set_tmrN_cfg_cap(N, val) do {(*(volatile uint64_t*)(hpet_data.regs+0x100u+0x20u*(N))) = (val);}while(0)

/** Configurethe comparator for each individual timer */
#define hpet_get_tmrN_cmp(N)          (*(volatile uint64_t*)(hpet_data.regs+0x108u+0x20u*(N)))
#define hpet_set_tmrN_cmp(N, val) do {(*(volatile uint64_t*)(hpet_data.regs+0x108u+0x20u*(N))) = (val);}while(0)

/** Configure the FSB interrupt routing for each individual timer */
#define hpet_get_tmrN_fsb_int_route(N)          (*(volatile uint64_t*)(hpet_data.regs+0x110u+0x20u*(N)))
#define hpet_set_tmrN_fsb_int_route(N, val) do {(*(volatile uint64_t*)(hpet_data.regs+0x110u+0x20u*(N))) = (val);}while(0)

/** In femtoseconds. Less than 100ns */
#define hpet_get_clk_period() ((hpet_get_gen_caps_id()>>32)&0xFFFFFFFFu)

/** Vendor ID */
#define hpet_get_vendor_id() ((hpet_get_gen_caps_id()>>16)&0xFFFFu)

/** 64-bit capable */
#define hpet_get_count_size_cap() ((hpet_get_gen_caps_id()>>13)&1ull)

/** Number of available timers */
#define hpet_get_num_tim_cap() (1+((hpet_get_gen_caps_id()>>8)&0x1Fu))

/** Enable counter */
#define hpet_enable()   hpet_set_gen_cfg(hpet_get_gen_cfg()|3ull);
#define hpet_disable()  hpet_set_gen_cfg(hpet_get_gen_cfg()&~3ull);

/** Control the IOAPIC entry */
#define hpet_get_tmrN_cfg_int_route(N) ((hpet_get_tmrN_cfg_cap(N)>>9)&0x1F)
#define hpet_set_tmrN_cfg_int_route(N, val) do {uint8_t _N =(N); hpet_set_tmrN_cfg_cap(_N, hpet_get_tmrN_cfg_cap(_N)|(((val)&0x1F)<<9));}while(0)

/** Control trigger*/
#define hpet_set_tmrN_trigger_level(N) do {uint8_t _N =(N); hpet_set_tmrN_cfg_cap(_N, hpet_get_tmrN_cfg_cap(_N)|2ull);}while(0)
#define hpet_set_tmrN_trigger_edge(N)  do {uint8_t _N =(N); hpet_set_tmrN_cfg_cap(_N, hpet_get_tmrN_cfg_cap(_N)&~2ull);}while(0)

/** Control trigger*/
#define hpet_set_tmrN_int_enable(N)  do {uint8_t _N =(N); hpet_set_tmrN_cfg_cap(_N, hpet_get_tmrN_cfg_cap(_N)|4ull);}while(0)
#define hpet_set_tmrN_int_disable(N) do {uint8_t _N =(N); hpet_set_tmrN_cfg_cap(_N, hpet_get_tmrN_cfg_cap(_N)&~4ull);}while(0)
#define hpet_set_tmrN_int_clear(N)   do {hpet_set_gen_int_status(1<<(N));}while(0)
#define hpet_get_tmrN_int_status(N)  (hpet_get_gen_int_status()&(1<<(N)))

STATIC uint64_t hpet_gettime(void);

#endif /* QUEVEDO_HPET_PRIVATE_H */
