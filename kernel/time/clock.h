#ifndef QUEVEDO_CLOCK_H
#define QUEVEDO_CLOCK_H

#include <sys/queue.h>

#include "time.h"

LIST_HEAD(dline_head, time_dline);

/** Timer deadlines */
struct time_dline {
	LIST_ENTRY(time_dline) link;
	time_cb cb;
	void *arg;
	time_a target;
};

struct time_clk {
	struct dline_head head;
	time_a (*gettime)(void);
	void   (*cfg_oneshot)(time_a target);
};

#endif /* QUEVEDO_CLOCK_H */
