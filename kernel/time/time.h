#ifndef QUEVEDO_TIME_H
#define QUEVEDO_TIME_H

#include <stdbool.h>
#include <stdint.h>

/**
 * Time representation
 * @{
 */
/** Unsigned time representation in ns*/
typedef uint64_t time_ns;
/** (A)bsolute time in ns */
typedef time_ns time_a;
/** (R)elative time in ns. Negative values are in the past */
typedef int64_t time_r;
/** 1 (positive) second in time_r units */
#define TIME_SECONDS      (1000000000ULL)
#define TIME_MILLISECONDS (TIME_SECONDS/1000)
#define TIME_MICROSECONDS (TIME_MILLISECONDS/1000)
#define TIME_NANOSECONDS  (TIME_MICROSECONDS/1000)
/** @}*/

typedef void (*time_cb)(void *arg);

/** Initialise the time subsystem */
struct hbt;
void time_init(struct hbt *hbt);

/** Initialise the time sybsystem for APs */
void time_ap_init(void);

/**
 * Get current absolute time
 *
 * @return Current absolute time
 */
time_a time_gettime(void);

/**
 * Sleep until a time target is reached
 *
 * @param target Sleep until 'target'
 */
void time_sleep_until(time_a target);

struct time_clk;
void time_tick(struct time_clk *clk);

/** To be used when 'arg' is not required */
#define TIME_ARG_NONE (-1U)

/**
 * Perform arithmetic operations with time
 *
 * Assumes two's complement, but I think that's a fair assumption
 *
 * @param x Some time_a 
 * @param op ==, !=, <, >, <= or >=
 * @param y Some time_a 
 * @return evaluation of the operation
 */
#define time_op(x, op, y) (((int64_t)((x)-(y))) op 0)

#define time_add_seg(time,  delta) ((time)+(time_a)(TIME_SECONDS*(delta)))
#define time_add_mseg(time, delta) ((time)+(time_a)(TIME_MILLISECONDS*(delta)))
#define time_add_useg(time, delta) ((time)+(time_a)(TIME_MICROSECONDS*(delta)))
#define time_add_nseg(time, delta) ((time)+(time_a)(TIME_NANOSECONDS*(delta)))

#endif /* QUEVEDO_TIME_H */
