#ifndef QUEVEDO_TIME_PRIVATE_H
#define QUEVEDO_TIME_PRIVATE_H

#include "time.h"
#include "clock.h"

struct time_data {
	struct time_clk *ref_clk;
};

#define dline_op(dline1, op, dline2) \
	time_op((dline1)->target, op, (dline2)->target)

/**
 * Add a deadline to a clock
 *
 * @param clk Clock that will track the deadline
 * @param dline Deadline to add
 */
STATIC void time_dline_add(struct time_clk *clk, struct time_dline *dline);

/**
 * Callback function for the sleep mechanism
 *
 * @param pid PID in u64 mode, casted from pid_t
 */
STATIC void time_sleep_cb(void *pid);

#endif /* QUEVEDO_TIME_PRIVATE_H */
