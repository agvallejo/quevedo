#include <stdint.h>

#include <quevedo/panic.h>
#include <quevedo/boot.h>

#include <mm/vmm.h>

#include "clock.h"
#include "hpet_private.h"

STATIC struct hpet_data hpet_data;

void hpet_init(struct hbt *hbt)
{
	kassert_bool(hbt->hpet.present, 0);
	kassert_bool(vmm_map(VMM_REGION_PRIV_GLOBAL, hbt->hpet.base, VMM_PAGESIZE, &hpet_data.regs), 0);
	vmm_cfg(hpet_data.regs, VMM_PAGESIZE, VMM_CFG_RW_CD);

	/* Return counter to a known position */
	hpet_disable();
	hpet_set_main_cnt(0);

	/* Start the counter */
	hpet_enable();

	/* The HPET needs to be 64-bits wide */
	kassert(1, ==, hpet_get_count_size_cap());
}

struct time_clk *hpet_getclk(void)
{
	hpet_data.clk.gettime     = hpet_gettime;
	hpet_data.clk.cfg_oneshot = NULL;

	return &hpet_data.clk;
}

STATIC uint64_t hpet_gettime(void)
{
	uint64_t ticks       = hpet_get_main_cnt();
	uint32_t tick2femto = hpet_get_clk_period();
	uint32_t femto2nano = 1000000;

	uint64_t femtos = ticks*tick2femto;
	if (femtos/tick2femto == ticks) {
		return femtos/femto2nano;
	}
	else {
		/* Overflow
		 *
		 * We must compute the following: a*b/c
		 *
		 * Where 'b' and 'c' are tiny and 'a' can be any 64-bits
		 * number. Big idea is to calculate quotient and remainder,
		 * then multiply remainder (smaller than 'c') by 'b' and divide
		 * by c. Sum everything in the end. That should work.
		 *
		 * a*b/c -> (q_a+r_a/c)*b -> (q_a*b)+((r_a*b)/c)
		 *
		 * The result might indeed overflow because the nanosecond counter
		 * overflows. That's perfectly normal. What needs to be done is
		 * to make sure the multiplication carry is not ignored in the division.
		 *
		 * This is integer arithmetic. ORDER IS ESSENTIAL.
		 */
		uint64_t full_nanos = (ticks/femto2nano)*tick2femto;
		uint32_t frac_nanos = ((ticks%femto2nano)*tick2femto)/femto2nano;
		return full_nanos+frac_nanos;
	}
}
