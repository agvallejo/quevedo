#ifndef _QUEVEDO_LD_H
#define _QUEVEDO_LD_H

/**
 * Get the address of the symbol 'x'
 *
 * @param x	name of the symbol minus the linker script prefix
 * @return	VMA of the linker script symbol '__ld_x'
 */
#ifndef UNIT_TESTING
#define ld_get(x) ((uintptr_t)& __ld_ ## x)
#define _QUEVEDO_SPAWN_SYMBOLS(x) extern void *__ld_ ## x ## _start; \
                                  extern void *__ld_ ## x ## _end
extern void *__ld_kernel_lma_start;
extern void *__ld_kernel_vma;

_QUEVEDO_SPAWN_SYMBOLS(kernel_text);
_QUEVEDO_SPAWN_SYMBOLS(kernel_rodata);
_QUEVEDO_SPAWN_SYMBOLS(kernel_data);
_QUEVEDO_SPAWN_SYMBOLS(kernel_bss);

#else
#define _QUEVEDO_SPAWN_SYMBOLS(x) uintptr_t x ## _start; \
                                  uintptr_t x ## _end
#include <stdint.h>
extern struct ld_symbols{
	uintptr_t kernel_lma_start;
	uintptr_t kernel_vma;

	_QUEVEDO_SPAWN_SYMBOLS(kernel_text);
	_QUEVEDO_SPAWN_SYMBOLS(kernel_rodata);
	_QUEVEDO_SPAWN_SYMBOLS(kernel_data);
	_QUEVEDO_SPAWN_SYMBOLS(kernel_bss);
} ld_fake_symbols;
#define ld_get(x) (ld_fake_symbols.x)
#endif

/**
 * Get the address of the symbol 'x' as loaded at bootstrap
 *
 * @param x	name of the symbol minus the linker script prefix
 * @return	LMA of the linker script symbol '__ld_x'
 */
#define ld_get_lma(x) (ld_get(x)+ld_get(kernel_lma_start)-ld_get(kernel_vma))

#endif // _QUEVEDO_LD_H
