#ifndef QUEVEDO_ELF_H
#define QUEVEDO_ELF_H

/**
 * Load an ELF image located at 'addr'
 *
 * @param addr Location of the ELF image
 */
NORETURN void elf_load(void *addr);

#endif /* QUEVEDO_ELF_H */
