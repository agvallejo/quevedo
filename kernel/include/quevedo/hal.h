#ifndef _QUEVEDO_HAL_H
#define _QUEVEDO_HAL_H

#include <stdint.h>
#include <stddef.h>
#include <quevedo/panic.h>

/** GPR sized unsigned integer */
typedef uintptr_t reg_t;

/**
 * Execute the 'cpuid' instruction
 *
 * @param code Input code
 * @param a Pointer to the variable to hold the contents of eax
 * @param c Pointer to the variable to hold the contents of ecx
 * @param d Pointer to the variable to hold the contents of edx
 */
#ifndef UNIT_TESTING
static inline void cpuid(int code, uint32_t *a, uint32_t *c, uint32_t *d)
{
	__asm__ volatile ("cpuid":"=a"(*a),"=c"(*c),"=d"(*d):"a"(code):"ebx");
}
#else
void cpuid(int code, uint32_t *a, uint32_t *d);
#endif // UNIT_TESTING

/**
 * Write to a Model-Specific-Register
 *
 * @param msr Register to write
 * @param val Value to write
 */
#ifndef UNIT_TESTING
static inline void wrmsr(uint64_t msr, uint64_t val)
{
	__asm__ volatile ("wrmsr" : : "c"(msr), "a"(val & 0xFFFFFFFF), "d"(val>>32));
}
#else
void wrmsr(uint64_t msr, uint64_t val);
#endif // UNIT_TESTING

/**
 * Read a Model-Specific-Register
 *
 * @param msr Register to write
 * @param val Value to write
 */
#ifndef UNIT_TESTING
static inline uint64_t rdmsr(uint64_t msr)
{
	uint32_t lo, hi;
	__asm__ volatile ("rdmsr" : "=a"(lo), "=d"(hi) : "c"(msr));
	return ((uint64_t)hi<<32) | lo;
}
#else
uint64_t rdmsr(uint64_t msr);
#endif // UNIT_TESTING

/**
 * Tx an octet through PIO
 *
 * @param port PIO port to use
 * @param val  Transmitted octet
 */
#ifndef UNIT_TESTING
static inline void outb(uint16_t port, uint8_t val)
{
	__asm__ volatile ("outb %0, %1" : : "a"(val), "Nd"(port));
}
#else
void outb(uint16_t port, uint8_t val);
#endif

/**
 * Rx an octet through PIO
 *
 * @param port PIO port to use
 * @return Received octet
 */
#ifndef UNIT_TESTING
static inline uint8_t inb(uint16_t port)
{
	uint8_t ret;
	__asm__ volatile ("inb %1, %0" : "=a"(ret) : "Nd"(port));
	return ret;
}
#else
uint8_t inb(uint16_t port);
#endif

#ifndef UNIT_TESTING
/** Halt execution on the machine */
static inline void halt(void)
{
	__asm__ volatile ("hlt");
}
#else
void halt(void);
#endif // UNIT_TESTING

/** Disable all interrupts */
#ifndef UNIT_TESTING
static inline void cli(void)
{
	__asm__ volatile ("cli");
}
#else
void cli(void);
#endif // UNIT_TESTING

static inline NORETURN void halt_forever(void)
{
	while(1) {
		cli();
		halt();
	}
}

/** Enable all interrupts */
#ifndef UNIT_TESTING
static inline void sti(void)
{
	__asm__ volatile ("sti");
}
#else
void sti(void);
#endif // UNIT_TESTING

#ifndef UNIT_TESTING
static inline uint64_t rdtsc(void)
{
	uint32_t low, high;
	__asm__ volatile("rdtsc":"=a"(low),"=d"(high));
	return ((uint64_t)high << 32) | low;
}
#else
uint64_t rdtsc(void);
#endif // UNIT_TESTING

#ifndef UNIT_TESTING
static inline void swapgs(void)
{
    __asm__ volatile ("swapgs");
}
#else
void swapgs(void);
#endif // UNIT_TESTING

#ifndef UNIT_TESTING
static inline uint64_t hal_getflags(void)
{
	uint64_t flags;
	__asm__ volatile("pushfq\n\tpopq %0":"=rm"(flags));
	return flags;
}
#else
uint64_t hal_getflags(void);
#endif // UNIT_TESTING

#ifndef UNIT_TESTING
static inline uintptr_t hal_getcr3(void)
{
    uintptr_t cr3;
    __asm__ volatile ( "mov %%cr3, %0" : "=r"(cr3) );
    return cr3;
}
#else
uintptr_t hal_getcr3(void);
#endif // UNIT_TESTING

#ifndef UNIT_TESTING
static inline void hal_setcr3(uintptr_t cr3)
{
    __asm__ volatile ( "mov %0, %%cr3" : : "r"(cr3) );
}
#else
void hal_setcr3(uintptr_t cr3);
#endif // UNIT_TESTING

#endif // _QUEVEDO_HAL_H
