#ifndef QUEVEDO_LOG_H
#define QUEVEDO_LOG_H

#include <quevedo/panic.h>
#include <quevedo/cfg.h>

#include <mp/mp.h>

#include <stdio.h>

#define _STR(x) #x
#define _VAL(x) _STR(x)

#define log_op_nopanic(arg1, op, arg2) printf("[cpu:%u|file:%s|line:%s] Assertion failed (%s%s%s|0x%llX%s0x%llX)\n", \
                                              mp_percore(id), __FILE__, _VAL(__LINE__), #arg1, #op, #arg2, arg1, #op, arg2)
#define log_op_panic(arg1, op, arg2) do{log_op_nopanic(arg1, op, arg2); panic("", 0);}while(0)

#define log_assert_nopanic(arg1, op, arg2) do{if(!((arg1) op (arg2))){log_op_nopanic(arg1, op, arg2);}}while(0)
#define log_assert_panic(arg1, op, arg2) do{if(!((arg1) op (arg2))){log_op_panic(arg1, op, arg2);}}while(0)

#if defined(LOG_PANIC_WARN)
#define log_warn_assert(arg1, op, arg2) log_assert_panic(arg1, op, arg2)
#define log_error_assert(arg1, op, arg2) log_assert_panic(arg1, op, arg2)
#define log_fatal_assert(arg1, op, arg2) log_assert_panic(arg1, op, arg2)
#elif defined(LOG_PANIC_ERROR)
#define log_warn_assert(arg1, op, arg2) log_assert_nopanic(arg1, op, arg2)
#define log_error_assert(arg1, op, arg2) log_assert_panic(arg1, op, arg2)
#define log_fatal_assert(arg1, op, arg2) log_assert_panic(arg1, op, arg2)
#elif defined(LOG_PANIC_FATAL)
#define log_warn_assert(arg1, op, arg2) log_assert_nopanic(arg1, op, arg2)
#define log_error_assert(arg1, op, arg2) log_assert_nopanic(arg1, op, arg2)
#define log_fatal_assert(arg1, op, arg2) log_assert_panic(arg1, op, arg2)
#error "Panic level incorrect or not chosen"
#endif

#define log(msg) printf("[cpu:%u|file:%s|line:%s] %s\n", mp_percore(id), __FILE__, _VAL(__LINE__), msg)
#define log_preboot(msg) printf("[cpu:X|file:%s|line:%s] %s\n", __FILE__, _VAL(__LINE__), msg)

#if defined(LOG_DEBUG)
#define log_debug(msg) log(msg)
#define log_info(msg) log(msg)
#define log_warn(msg) log(msg)
#define log_error(msg) log(msg)
#define log_fatal(msg) log(msg)
#elif defined(LOG_INFO)
#undef log_debug
#define log_debug(msg) ((void)0)
#undef log_debug_assert
#define log_debug_assert(arg1, op, arg2) ((void)0)
#define log_info(msg) log(msg)
#define log_warn(msg) log(msg)
#define log_error(msg) log(msg)
#define log_fatal(msg) log(msg)
#elif defined(LOG_WARN)
#undef log_debug
#define log_debug(msg) ((void)0)
#undef log_debug_assert
#define log_debug_assert(arg1, op, arg2) ((void)0)
#undef log_info
#define log_info(msg) ((void)0)
#undef log_info_assert
#define log_info_assert(arg1, op, arg2) ((void)0)
#define log_warn(msg) log(msg)
#define log_error(msg) log(msg)
#define log_fatal(msg) log(msg)
#elif defined(LOG_ERROR)
#undef log_debug
#define log_debug(msg) ((void)0)
#undef log_debug_assert
#define log_debug_assert(arg1, op, arg2) ((void)0)
#undef log_info
#define log_info(msg) ((void)0)
#undef log_info_assert
#define log_info_assert(arg1, op, arg2) ((void)0)
#undef log_warn
#define log_warn(msg) ((void)0)
#undef log_warn_assert
#define log_warn_assert(arg1, op, arg2) ((void)0)
#define log_error(msg) log(msg)
#define log_fatal(msg) log(msg)
#elif defined(LOG_FATAL)
#undef log_debug
#define log_debug(msg) ((void)0)
#undef log_debug_assert
#define log_debug_assert(arg1, op, arg2) ((void)0)
#undef log_info
#define log_info(msg) ((void)0)
#undef log_info_assert
#define log_info_assert(arg1, op, arg2) ((void)0)
#undef log_warn
#define log_warn(msg) ((void)0)
#undef log_warn_assert
#define log_warn_assert(arg1, op, arg2) ((void)0)
#undef log_error
#define log_error(msg) ((void)0)
#undef log_error_assert
#define log_error_assert(arg1, op, arg2) ((void)0)
#define log_fatal(msg) log(msg)
#else
#error "Log level incorrect or not chosen"
#endif

#endif /* QUEVEDO_LOG_H */
