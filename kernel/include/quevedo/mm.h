#ifndef _QUEVEDO_MM_H
#define _QUEVEDO_MM_H

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include "mm/mm_phy.h"

//------------------------------------------------------------------------------
// Virtual memory manager
//------------------------------------------------------------------------------
/** Page size (bsize=block-size) */
enum mm_virt_bsize {
	MM_VIRT_BSIZE_4KiB   = 0,
	MM_VIRT_BSIZE_2MiB   = 1,
	MM_VIRT_BSIZE_1GiB   = 2,
	MM_VIRT_BSIZE_TOTAL  = 3,
	MM_VIRT_BSIZE_512GiB = 3 // Not a valid block size
};

/** Architecture dependent paging flags */
typedef uint64_t mm_virt_pflags_t;

/** Protection of a memory block */
enum mm_virt_prot{
	MM_VIRT_PROT_INVALID = -1,
	MM_VIRT_PROT_X       = 0,
	MM_VIRT_PROT_U_X     = 1,
	MM_VIRT_PROT_W       = 2,
	MM_VIRT_PROT_U_W     = 3,
	MM_VIRT_PROT_RO      = 4,
	MM_VIRT_PROT_U_RO    = 5,
	MM_VIRT_PROT_U_W_X   = 6,
	MM_VIRT_PROT_TOTAL
};

/** Initialise the memory manager module */
void mm_virt_init(void);

/** Unmap the bootstrap code/data */
void mm_virt_init_unmap_lower_mem(void);

/**
 * Allocate a region of virtual memory
 *
 * Assumes the region is currently unmapped
 *
 * @param vaddr  Start of the virtual memory chunk
 * @param octets Amount of memory to allocate
 * @param bsize  The chunk uses blocks/pages of 'bsize'
 * @param prot   Protection of the region
 */
void mm_virt_alloc(uintptr_t vaddr,
                   size_t    octets,
                   enum mm_virt_bsize bsize,
                   enum mm_virt_prot  prot);

/**
 * Reconfigure a region of virtual memory
 *
 * Assumes the region is already mapped
 *
 * @param vaddr  Start of the virtual memory chunk
 * @param octets Amount of memory to reconfigure
 * @param bsize  The chunk uses blocks/pages of 'bsize'
 * @param prot   Protection of the region
 */
void mm_virt_conf(uintptr_t vaddr,
                  size_t    octets,
                  enum mm_virt_bsize bsize,
                  enum mm_virt_prot  prot);

/**
 * Map the contiguous physical chunk 'paddr' into 'vaddr'
 *
 * @param paddr  Start of the physical memory chunk
 * @param vaddr  Start of the virtual memory chunk
 * @param octets Amount of memory to reconfigure
 * @param bsize  The chunk uses blocks/pages of 'bsize'
 * @param prot   Protection of the region
 */
void mm_virt_map(phyaddr   paddr,
                 uintptr_t vaddr,
                 size_t    octets,
                 enum mm_virt_bsize bsize,
                 enum mm_virt_prot  prot);

/**
 * Free a region of virtual memory
 *
 * Physical pages go to mm_phy
 *
 * @param vaddr  Start of the virtual memory chunk
 * @param octets Amount of memory to reconfigure
 * @param bsize  The chunk uses block/pages of 'bsize'
 */
void mm_virt_free(uintptr_t vaddr, size_t octets,
                  enum mm_virt_bsize bsize);

/**
 * Check if 'addr' is mapped
 *
 * @param addr Virtual address to check
 */
bool mm_virt_ismapped(uintptr_t addr);

/**
 * Get the bsize 'addr' is mapped with
 *
 * @param addr Virtual address to check
 */
enum mm_virt_bsize mm_virt_getbsize(uintptr_t addr);


/**
 * Clears the TLB from 'addr'
 *
 * Needs to be called each time a page entry is modified
 *
 * @param addr Address which has changed flags
 */
static inline void mm_virt_invlpg(uintptr_t addr)
{
#ifdef UNIT_TESTING
	(void)addr;
#else
	__asm__ volatile("invlpg (%0)" ::"r" (addr) : "memory");
#endif
}

/**
 * Print the paging structures of 'addr'
 *
 * @param addr Address to consider while doing the page walk
 */
void mm_virt_print_pwalk(uintptr_t addr);

#endif // _QUEVEDO_MM_H
