#ifndef _QUEVEDO_PANIC_H
#define _QUEVEDO_PANIC_H

#include <stdint.h>

NORETURN void panic_simple(char *file, char *line, char *cond, uint64_t arg);
NORETURN void panic_full(char *file, char *line, char* sarg1, char *op, char *sarg2, uint64_t arg1,  uint64_t arg2);

#define _STR(x) #x
#define _VAL(x) _STR(x)

#define kassert_true(arg1, op, arg2) \
       do{ \
               uint64_t _arg1 = (uint64_t)(arg1); \
               uint64_t _arg2 = (uint64_t)(arg2); \
               if (!((_arg1) op (_arg2))) { \
                       panic_full(__FILE__, _VAL(__LINE__), #arg1, #op, #arg2, _arg1, _arg2); \
               } \
       } while(0)

#define kassert_false(arg1, op, arg2) \
       do{ \
               uint64_t _arg1 = (uint64_t)(arg1); \
               uint64_t _arg2 = (uint64_t)(arg2); \
               if ((_arg1) op (_arg2)) { \
                       panic_full(__FILE__, _VAL(__LINE__), #arg1, #op, #arg2, _arg1, _arg2); \
               } \
       } while(0)

#define kassert_with_arg(cond, arg) ((cond)?(void)0:panic_simple(__FILE__, _VAL(__LINE__), #cond, (uint64_t)(arg)))

/* Convenience macros */
#define kassert(...) kassert_true(__VA_ARGS__)
#define kassert_notnull(ptr, arg) kassert_with_arg(ptr, arg)
#define kassert_bool(cond, arg) kassert_with_arg(cond, arg)
#define kassert_unreachable() panic("Unreachable", 0)

#define panic(msg, arg) panic_simple(__FILE__, _VAL(__LINE__), msg, (uint64_t)(arg))

#endif // _QUEVEDO_PANIC_H
