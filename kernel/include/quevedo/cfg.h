#ifndef QUEVEDO_CFG_H
#define QUEVEDO_CFG_H

/******************************************************************************
* Interrupt configuration
******************************************************************************/
#define ISR_IRQ_BASE    (0x20U)  /** Base address for the IRQ remaps */
#define ISR_INTNUM_HPET (1u)

/******************************************************************************
* Logging configuration
*
* Levels: DEBUG, INFO, WARN, ERROR and FATAL
******************************************************************************/
#define LOG_DEBUG /**< LOG_X=Log up to X*/
#define LOG_PANIC_WARN /**< LOG_PANIC_X=Panic on any X event or higher */

/******************************************************************************
* Address space layout configuration
******************************************************************************/
/** Convenience macro for sign extensions  */
#define SIGN_EXT (~((512ull<<39)-1))

/** Start of the recursive mapping of the page tables */
#define VMM_REGION_PRIV_GLOBAL_MINADDR (SIGN_EXT|(511ull<<39))

/** Start of global privileged area. Includes paging tables */
#define VMM_REGION_PTABLES_MINADDR (SIGN_EXT|(510ull<<39))

/** Start of global privileged area. Includes paging tables */
#define VMM_REGION_PRIV_LOCAL_MINADDR (SIGN_EXT|(509ull<<39))

/******************************************************************************
* Size configuration
******************************************************************************/
/** Convenience macro for sizes */
#define KiB (1024u)

/**
 * Default size of a thread stack
 *
 * IMPORTANT: Also modify kmain.asm with the new stack size or things WILL break
 */
#define TSCH_STACK_SIZE (16*KiB)

/** Default size (in messages) of the per-thread mailboxes */
#define TIPC_MBOX_SIZE (32)

/** Default fixed-size (in octets) of each passed message */
#define TIPC_MSG_SIZE (8)

/* The heap is a pool a fixed-size blocks. The actual size of the pool
 * and the number of entries can be configured, while the blocksize is
 * deduced from the previous parameters and the metadata size:
 *
 * Usable blocksize = (HMM_POOLSIZE/HMM_NUM_ENTRIES)-sizeof(struct hmm_entry)
 *
 * In general, Quevedo assumes all allocations will fit in a block or
 * it will otherwise panic. The block size should therefore be at least
 * as big as the biggest allocation.
 */
#define HMM_POOLSIZE (32*KiB) /** Default size of the heap pool. Real size */
#define HMM_NUM_ENTRIES (128u) /** Number of entries in the heap pool */

/******************************************************************************
* Deduced parameters
******************************************************************************/
/** Size of the local privileged side of the address space */
#define VMM_REGION_PRIV_LOCAL_SIZE (VMM_REGION_PTABLES_MINADDR-\
                                    VMM_REGION_PRIV_LOCAL_MINADDR)
_Static_assert(VMM_REGION_PRIV_LOCAL_SIZE == 512llu<<30, "");

/** Maximum address of the local privileged side of the address space */
#define VMM_REGION_PRIV_LOCAL_MAXADDR (VMM_REGION_PRIV_LOCAL_MINADDR+\
                                       VMM_REGION_PRIV_LOCAL_SIZE-1)

/** Size of the recursive map region of the paging tables */
#define VMM_REGION_PTABLES_SIZE (VMM_REGION_PRIV_GLOBAL_MINADDR-\
                                 VMM_REGION_PTABLES_MINADDR)
_Static_assert(VMM_REGION_PTABLES_SIZE == 512llu<<30, "");

/** Maximum address of the recursive map region of the paging tables */
#define VMM_REGION_PTABLES_MAXADDR (VMM_REGION_PTABLES_MINADDR+\
                                       VMM_REGION_PTABLES_SIZE-1)

/** Size of the global region on the privileged side of the address space */
#define VMM_REGION_PRIV_GLOBAL_SIZE (-VMM_REGION_PRIV_GLOBAL_MINADDR)
_Static_assert(VMM_REGION_PRIV_GLOBAL_SIZE == 512llu<<30, "");

/** Size of the global region on the privileged side of the address space */
#define VMM_REGION_PRIV_GLOBAL_MAXADDR (VMM_REGION_PRIV_GLOBAL_MINADDR+\
                                       VMM_REGION_PRIV_GLOBAL_SIZE-1)

/** Location of the local heap */
#define HMM_LOCAL_POOL_ADDR (VMM_REGION_PRIV_LOCAL_MAXADDR+1-\
			     HMM_POOLSIZE)

#endif /* QUEVEDO_CFG_H */
