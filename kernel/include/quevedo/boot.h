#ifndef QUEVEDO_BOOT_H
#define QUEVEDO_BOOT_H

#include <stdint.h>

#define QUEVEDO_MAGIC 0x19920413 /**< Passed as a parameter to the entry point of the kernel */

/**
 * Graphics related information
 *
 * Provides all the framebuffer details
 */
struct hbt_gfx {
	uint64_t virt_buf; /**< Address of the start of the framebuffer */
	uint64_t phy_buf; /**< Address of the start of the framebuffer */
	uint64_t resx; /**< Horizontal resolution */
	uint64_t resy; /**< Vertical resolution */
	uint64_t fmt;  /**< Pixel format*/
};

struct hbt_mm_entry
{
	uint64_t start; /**< Start of the region */
	uint64_t octets; /**< Size of the region */
};

/** Physical Memory Management related information */
struct hbt_pmm {
	uint64_t map_size;
#define HBT_PMM_NUM_REGION_ENTRIES (64)
	struct hbt_mm_entry map[HBT_PMM_NUM_REGION_ENTRIES];
	uint64_t rsvd_pages_size;
#define HBT_PMM_NUM_RSVD_ENTRIES (32)
	struct hbt_mm_entry rsvd_pages[HBT_PMM_NUM_RSVD_ENTRIES]; /**< Entries used by the bootloader in paging */
};

struct hbt_initrd
{
	struct hbt_mm_entry init; /**< Initial userland app */
};

struct hbt_hpet
{
	uint8_t present; /**< 1 if present, otherwise 0 */
	uint64_t base; /**< if present, base address of the HPET */
};

struct hbt_apic
{
	uint8_t present; /**< 1 if present, otherwise 0 */
	uint64_t lapic_base; /**< if present, base address of the Local APIC */
};

struct hbt_mp
{
	uint16_t cpumap_size;
	struct {
		uint8_t apic_id;
#define HBT_MP_NUM_ENTRIES (16)
	} cpumap[HBT_MP_NUM_ENTRIES];
};

struct hbt_ap_params
{
	uint64_t entry; /**< Entry point into the kernel proper */
	uint64_t stack; /**< Position of the AP stack */
	uint32_t trampoline; /**< Address of the trampoline itself. Needed for relocations */
	uint32_t cr3; /**< Address of the PML4 */
};

/**
 * HBT (Handover-Boot-Table)
 *
 * All bootloaders must implement code to populate this table before
 * handing control over to the main kernel. This provides an isolation
 * layer with the bootloader so that BIOS and UEFI can be constrained
 * to specific code to boot.
 */
struct hbt {
#define QUEVEDO_HBT_ID  "QUEVEDO!" /**< Identification tag for the HBT */
	char id[8];           /**< Easy identifier to ensure validity. Also 64-bit aligned :) */
#define QUEVEDO_HBT_VERSION "01122019" /**< Lazily set to a date. EFI loader and kernel must agree */
	char version[8];      /**< Version of this table. To be updated when the table changes */
	uint64_t checksum;    /**< Checksum to ensure the table is not corrupted */
	struct hbt_gfx gfx;   /**< Graphics sub-table */
	struct hbt_pmm pmm;   /**< Non-overlapping areas of available physical memory */
	struct hbt_hpet hpet; /**< Information about the HPET */
	struct hbt_apic apic; /**< Information about the APIC */
	struct hbt_initrd initrd; /**< Information about the 'initrd' */
	struct hbt_mp mp;     /**< Information about multicore */
	struct hbt_ap_params ap_params; /**< Parameters for the to-be-woken Application Proccessors */

};

#endif /* QUEVEDO_BOOT_H */
