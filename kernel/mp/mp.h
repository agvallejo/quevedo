#ifndef QUEVEDO_MP_H
#define QUEVEDO_MP_H

#include <quevedo/hal.h>

#include <stdatomic.h>
#include <stddef.h>

/**
 * Initialise the MultiProcessor module
 *
 * Wakes up the other CPUs in the system
 *
 * @param hbt Pointer to the HBT
 */
struct hbt;
void mp_init(struct hbt *hbt);

/** Initialise the MultiProcessor module for a new AP */
void mp_ap_init(void);

/**
 * Locks a mutex with a spinlock
 *
 * @param mutex The mutex to lock
 */
static inline void mp_lock(atomic_flag *mutex)
{
	while(atomic_flag_test_and_set_explicit(mutex, memory_order_acquire));
}

/**
 * Unlocks a mutex
 *
 * @param mutex The mutex to unlock
 */
static inline void mp_unlock(atomic_flag *mutex)
{
	atomic_flag_clear_explicit(mutex, memory_order_release);
}

typedef uint64_t coreid_t;
struct tsch_data_percore *tsch;
struct mp_percore
{
	/* ALL ELEMENTS MUST BE UINTPTR_T SIZED */
	void *rsp; /**< Stack to use on syscalls */
	struct mp_percore *self;
	struct tsch_data_percore *tsch;
	struct hal_data_percore *hal;
	coreid_t id;
};

static inline void mp_percore_set(struct mp_percore *data)
{
#define KernelGSBase (0xC0000102)
	wrmsr(KernelGSBase, (uintptr_t)data);
	swapgs();
}

static inline uintptr_t mp_percore_get(size_t offset)
{
	uintptr_t out;
	__asm__ volatile ("movq %%gs:(%1), %0" : "=g"(out) : "r"(offset));
	return out;
}

/** Gets the offset of a field in the mp_percore struct */
#define mp_offsetof(field) offsetof(struct mp_percore, field)
/** Gets the type of a field in the mp_percore struct */
#define mp_typeof(field) __typeof__(((struct mp_percore*)0)->field)
/** Retrieve a per-cpu pointer */
#define mp_percore(field) ((mp_typeof(field))mp_percore_get(mp_offsetof(field)))

#endif /* QUEVEDO_MP_H */
