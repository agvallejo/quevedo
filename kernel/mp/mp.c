#include <quevedo/boot.h>
#include <quevedo/hal.h>

#include <hal/apic.h>
#include <mm/vmm.h>
#include <mm/hmm.h>

#include "mp_private.h"

#include <hal/hal_percore.h>
#include <tsch/tsch_percore.h>

#include <stddef.h>
#include <stdatomic.h>
#include <stdio.h>

static struct mp_data mp_data;

extern NORETURN void kmain_ap(void);

static void mp_wake(uint8_t apic_id, struct hbt *hbt)
{
	hbt->ap_params.cr3   = hal_getcr3();
	hbt->ap_params.stack = vmm_alloc(VMM_REGION_PRIV_GLOBAL, TSCH_STACK_SIZE)+TSCH_STACK_SIZE;
	hbt->ap_params.trampoline = ~0xFFFllu & (uintptr_t)hbt;
	hbt->ap_params.entry = (uintptr_t)kmain_ap;

	apic_wake(apic_id, hbt->ap_params.trampoline);
}

static void mp_create_percore(coreid_t id)
{
	size_t len = sizeof(struct mp_percore) + sizeof(struct mp_percore *);
	struct mp_percore *percore = hmm_alloc(VMM_REGION_PRIV_GLOBAL, len);
	percore->self = percore;
	percore->id = id;
	percore->tsch = hmm_alloc(VMM_REGION_PRIV_GLOBAL, sizeof(struct tsch_data_percore));
	percore->hal  = hmm_alloc(VMM_REGION_PRIV_GLOBAL, sizeof(struct hal_data_percore));

	mp_percore_set(percore);

	kassert(mp_percore(self), ==, percore);
}

void mp_init(struct hbt *hbt)
{
	uint8_t local_id = apic_getid();
	size_t num_cpus_awake = 1;
	volatile size_t *num_cpus = &mp_data.num_cpus;

	mp_create_percore(0);

	atomic_store(num_cpus, num_cpus_awake);
	for (size_t i=0; i<hbt->mp.cpumap_size; i++)
	{
		uint8_t id = hbt->mp.cpumap[i].apic_id;
		if (local_id != id)
		{
			mp_wake(id, hbt);
			while(*num_cpus == num_cpus_awake);
			num_cpus_awake++;
		}
	}

	printf("Found %u cores\n", num_cpus_awake);
}

void mp_ap_init(void)
{
	coreid_t id = atomic_fetch_add(&mp_data.num_cpus, 1);
	mp_create_percore(id);
}
