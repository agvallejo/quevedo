#ifndef QUEVEDO_MP_PRIVATE_H
#define QUEVEDO_MP_PRIVATE_H

#include "mp.h"

struct mp_data {
	size_t num_cpus; /**< Increased by the CPUs as they wake up */
};

#endif /* QUEVEDO_MP_PRIVATE_H */
